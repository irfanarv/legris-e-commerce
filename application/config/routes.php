<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE; 
$route['contact-us'] = 'contactus';
$route['about-us'] = 'aboutus';
$route['invoice/(:any)'] = 'checkout/getInvoice/$1';
$route['product/(:any)'] = 'product/detail/$1';
$route['categories/(:any)'] = 'product/categoryProduct/$1';
// account
$route['my-account'] = 'auth/accountCustomer';
$route['logout'] = 'auth/logout';
$route['wishlist'] = 'auth/wishlist';
// dashboard
$route['dashboard'] = 'Backend/Dashboard';
$route['dashboard/login'] = 'Backend/Dashboard/login';
$route['dashboard/logout'] = 'Backend/Dashboard/logout';
// dashboard product
$route['dashboard/category-products'] = 'Backend/Products/category';
$route['dashboard/type-products'] = 'Backend/Products/type';
$route['dashboard/products'] = 'Backend/Products/product';
$route['dashboard/add-products'] = 'Backend/Products/product/add';
$route['dashboard/edit-products/(:any)'] = 'Backend/Products/product/edit/$1';
// gallery product
$route['dashboard/products-gallery/(:any)'] = 'Backend/Products/product/list_gallery/$1';
// account
$route['dashboard/account'] = 'Backend/Account';
$route['dashboard/member'] = 'Backend/Account/member';
// sliders
$route['dashboard/slider'] = 'Backend/Sliders/Slider';
// client
$route['dashboard/our-client'] = 'Backend/Client';
// bank account
$route['dashboard/bank-account'] = 'Backend/Bank';
// dashboard blog
$route['dashboard/category-news'] = 'Backend/News/Category';
$route['dashboard/news'] = 'Backend/News/Index';
$route['dashboard/add-news'] = 'Backend/News/Index/add';

// product ajax
// list
$route['dashboard/products_list'] = 'Backend/Products/Product/productList';
$route['dashboard/products_add'] = 'Backend/Products/Product/save';
$route['dashboard/products_upload'] = 'Backend/Products/Product/upload';
$route['dashboard/products_remove'] = 'Backend/Products/Product/removegallery';
$route['dashboard/photo_remove/(:any)'] = 'Backend/Products/Product/photoDelete/$1';
$route['dashboard/products_delete/(:any)'] = 'Backend/Products/Product/productDelete/$1';
$route['dashboard/products_edit'] = 'Backend/Products/Product/update';
// category
$route['dashboard/cat_add'] = 'Backend/Products/Category/categoryAdd';
$route['dashboard/cat_edit'] = 'Backend/Products/Category/categoryEdit';
$route['dashboard/cat_delete/(:any)'] = 'Backend/Products/Category/categoryDelete/$1';
$route['dashboard/cat_list'] = 'Backend/Products/Category/categoryList';
$route['dashboard/edit_cat/(:any)'] = 'Backend/Products/Category/editCategory/$1';
// type 
$route['dashboard/type_add'] = 'Backend/Products/Type/typeAdd';
$route['dashboard/type_edit'] = 'Backend/Products/Type/typeEdit';
$route['dashboard/type_delete/(:any)'] = 'Backend/Products/Type/typeDelete/$1';
$route['dashboard/type_list'] = 'Backend/Products/Type/List';
$route['dashboard/type_ed/(:any)'] = 'Backend/Products/Type/editType/$1';
// our partner
$route['dashboard/partner_upload'] = 'Backend/Client/upload';
$route['dashboard/partner_delete/(:any)'] = 'Backend/Client/photoDelete/$1';
// bank
$route['dashboard/bank_add'] = 'Backend/Bank/bankAdd';
$route['dashboard/bank_edit'] = 'Backend/Bank/editBank';
$route['dashboard/bank_delete/(:any)'] = 'Backend/Bank/bankDelete/$1';
$route['dashboard/bank_list'] = 'Backend/Bank/bankList';
$route['dashboard/edit_bank/(:any)'] = 'Backend/Bank/bankEdit/$1';
// slider
$route['dashboard/slider_add'] = 'Backend/Sliders/Slider/sliderAdd';
$route['dashboard/slider_edit'] = 'Backend/Sliders/Slider/sliderEdit';
$route['dashboard/slider_delete/(:any)'] = 'Backend/Sliders/Slider/sliderDelete/$1';
$route['dashboard/slider_list'] = 'Backend/Sliders/Slider/List';
$route['dashboard/edit_slider/(:any)'] = 'Backend/Sliders/Slider/editSlider/$1';
// account
$route['dashboard/account_list'] = 'Backend/Account/accountList';
$route['dashboard/account_edit/(:any)'] = 'Backend/Account/accountEdit/$1';
$route['dashboard/account_delete/(:any)'] = 'Backend/Account/accountDelete/$1';
$route['dashboard/account_add'] = 'Backend/Account/accountAdd';
$route['dashboard/edit_account'] = 'Backend/Account/editAccount/';
$route['dashboard/member_list'] = 'Backend/Account/memberList';

// page
$route['dashboard/page_list'] = 'Backend/Page/pageList';
$route['dashboard/page_edit/(:any)'] = 'Backend/Page/editPage/$1';
$route['dashboard/page_delete/(:any)'] = 'Backend/Page/pageDelete/$1';
$route['dashboard/page_add'] = 'Backend/Page/pageAdd';
$route['dashboard/edit_page'] = 'Backend/Page/pageEdit/';
$route['dashboard/pages'] = 'Backend/Page';
//
$route['dashboard/link_list'] = 'Backend/Link/linkList';
$route['dashboard/link_edit/(:any)'] = 'Backend/Link/editLink/$1';
$route['dashboard/link_delete/(:any)'] = 'Backend/Link/linkDelete/$1';
$route['dashboard/link_add'] = 'Backend/Link/linkAdd';
$route['dashboard/link_page'] = 'Backend/Link/linkEdit/';
$route['dashboard/link'] = 'Backend/Link';

// seo
$route['dashboard/settings'] = 'Backend/Seo';
$route['dashboard/setting_update'] = 'Backend/Seo/update';

// blog
$route['dashboard/news_add'] = 'Backend/News/Index/save';
$route['dashboard/news_update'] = 'Backend/News/Index/update';
$route['dashboard/edit-news/(:any)'] = 'Backend/News/Index/edit/$1';
$route['dashboard/post_delete/(:any)'] = 'Backend/News/Index/delete/$1';
$route['dashboard/news_list'] = 'Backend/News/Index/List';
// news cat
$route['dashboard/category_news'] = 'Backend/News/Category/categoryList';
$route['dashboard/category_add_news'] = 'Backend/News/Category/categoryAdd';
$route['dashboard/category_edit_news'] = 'Backend/News/Category/categoryEdit';
$route['dashboard/edit_category_news/(:any)'] = 'Backend/News/Category/editCategory/$1';
$route['dashboard/category_delete_news/(:any)'] = 'Backend/News/Category/categoryDelete/$1';
// orders
$route['dashboard/orders'] = 'Backend/Orders';
$route['dashboard/order_list'] = 'Backend/Orders/List';
$route['dashboard/updateorder'] = 'Backend/Orders/Update';
$route['dashboard/get_order/(:any)'] = 'Backend/Orders/getOrder/$1';