<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Slider extends CI_Controller
{
	public $data = array();
    public $userid;
	public $role;
	
	function __construct()
	{ 
		parent::__construct();
		$this->data['isadmin'] = $this->session->userdata('logged_admin');
	}
// view
	function index()
	{
		if ($this->data['isadmin']) {
            $this->data['pagetitle'] = 'Slider | Le Gris Home Furniture';
			$this->backend->display('backend/modules/slider/sliderV', $this->data); 
		} else {
            $this->data['pagetitle'] = 'Login | Le Gris Home Furniture';
            $this->load->view('backend/auth/loginV', $this->data); 
        }
	}

	public function List()
    {
        $list = $this->SliderM->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $rl) {
            if($rl->status == 1){
                $status = "<span class='badge badge-success'>Active</span>";
            }elseif($rl->status == 0) {
                $status = "<span class='badge badge-danger'>Deactive</span>";
            }

            $no++;
            $row = array();
            $row[] = "
            <td class='align-middle'>
            <img src='../assets/images/sliders/$rl->slider_img'  class='rounded mr-3' style='height:60px;width:160px;'/>
            <p class='m-0 d-inline-block align-middle font-16'>
            $rl->slider_title
            </p>
            </td>
            ";
            $row[] = $rl->slider_subtitle;
			$row[] = $rl->slider_btn;
            $row[] = $status;
            $row[] = date('d M Y - H:i', strtotime($rl->updatedAt))." "."WIB";
            
            
            $row[] = '
            <td class="table-action">
                <a href="javascript:void(0)" onclick="sliderEdit('."'".$rl->slider_id."'".')" class="btn btn-icon btn-outline-success"><i class="feather icon-edit"></i></a>
                <a href="javascript:void(0)" onclick="deleteSlider('."'".$rl->slider_id."'".')" class="btn btn-icon btn-outline-danger"><i class="feather icon-trash-2"></i></a>
            </td>
            ';
        
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->SliderM->count_all(),
                        "recordsFiltered" => $this->SliderM->count_filtered(),
                        "data" => $data,
                );
        echo json_encode($output);
    }

	public function sliderAdd() 
	{		
			
        $config['upload_path'] = './assets/images/sliders/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 8000;
        $config['encrypt_name'] = TRUE;
        $config['remove_spaces'] = TRUE; 

        $this->upload->initialize($config);

        if(!empty($_FILES['image']['name'])){
            if ($this->upload->do_upload('image')){
                $img = $this->upload->data();
                $image			= $img['file_name'];
                $title			= $this->input->post('title',true); 
                $sub_title		= $this->input->post('sub_title',true); 
                $btn_title		= $this->input->post('btn_title',true); 
				$btn_url		= $this->input->post('btn_url',true); 
				$status			= $this->input->post('status',true); 
                $btn_1=strip_tags($btn_url);
                $filter=str_replace("?", "", $btn_1);
                $filter2=str_replace("$", "", $filter);
                $pra_slug=$filter2;
                $slug=strtolower(str_replace(" ", "-", $pra_slug));
                
                $data = [
                    'slider_title' => $title,
					'slider_subtitle' => $sub_title,
					'slider_uri' => $slug,
					'slider_btn' => $btn_title,
                    'slider_img' => $image,
                    'status' => $status
                    
                ];
                $result= $this->SliderM->save($data);
                echo json_encode($result);
            }else{
                echo json_encode('Gagal'); 
            }
        }
			
	}

    public function sliderEdit()
    {
        $config['upload_path'] = './assets/images/sliders/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 8000;
        $config['encrypt_name'] = TRUE;

        $this->upload->initialize($config);

        if(!empty($_FILES['image']['name'])){
            if ($this->upload->do_upload('image')){
                $img = $this->upload->data();
                $image			= $img['file_name'];
                $title			= $this->input->post('title',true); 
                $sub_title		= $this->input->post('sub_title',true); 
                $btn_title		= $this->input->post('btn_title',true); 
				$btn_url		= $this->input->post('btn_url',true); 
				$status			= $this->input->post('status',true); 
                $btn_1=strip_tags($btn_url);
                $filter=str_replace("?", "", $btn_1);
                $filter2=str_replace("$", "", $filter);
                $pra_slug=$filter2;
                $slug=strtolower(str_replace(" ", "-", $pra_slug));
                $gambar=$this->input->post('gambar');
                $path='./assets/images/sliders/'.$gambar;
                unlink($path);
                
                $data = [
                    'slider_title' => $title,
					'slider_subtitle' => $sub_title,
					'slider_uri' => $slug,
					'slider_btn' => $btn_title,
                    'slider_img' => $image,
                    'status' => $status
                    
                ];
                
                $this->SliderM->update(array('slider_id' => $this->input->post('id')), $data);
                echo json_encode(array("status" => TRUE));
            }else{
                echo json_encode('Gagal'); 
            }
        }else{
                $title			= $this->input->post('title',true); 
                $sub_title		= $this->input->post('sub_title',true); 
                $btn_title		= $this->input->post('btn_title',true); 
				$btn_url		= $this->input->post('btn_url',true); 
				$status			= $this->input->post('status',true); 
                $btn_1=strip_tags($btn_url);
                $filter=str_replace("?", "", $btn_1);
                $filter2=str_replace("$", "", $filter);
                $pra_slug=$filter2;
                $slug=strtolower(str_replace(" ", "-", $pra_slug));

                $data = [
                    'slider_title' => $title,
					'slider_subtitle' => $sub_title,
					'slider_uri' => $slug,
					'slider_btn' => $btn_title,
                    'status' => $status
                    
                ];
            
            $this->SliderM->update(array('slider_id' => $this->input->post('id')), $data);
            echo json_encode(array("status" => TRUE));
        }

    } 

    public function editSlider($id)
    {
        $data = $this->SliderM->get_by_id($id); 
        echo json_encode($data);
    }

    public function sliderDelete($id)
    {
        $foto = $this->db->get_where('slider', array('slider_id' => $id));
        if ($foto->num_rows() > 0) {
            $hasil = $foto->row();
            $nama_foto = $hasil->slider_img;
            if (file_exists($file = './assets/images/sliders/' . $nama_foto)) {
                unlink($file);
            }
            $this->SliderM->delete_by_id($id);
        }
        echo json_encode(array("status" => TRUE));
    }

   	
}
