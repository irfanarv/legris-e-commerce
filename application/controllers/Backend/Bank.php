<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Bank extends CI_Controller
{
	public $data = array();
    public $userid;
	public $role;
	
	function __construct()
	{ 
		parent::__construct();
		$this->data['isadmin'] = $this->session->userdata('logged_admin');
	}
// view
	function index()
	{
		if ($this->data['isadmin']) {
            $this->data['pagetitle'] = 'Bank Account | Le Gris Home Furniture';
			$this->backend->display('backend/modules/bank/bankV', $this->data); 
		} else {
            $this->data['pagetitle'] = 'Login | Le Gris Home Furniture';
            $this->load->view('backend/auth/loginV', $this->data); 
        }
	}

    public function bankList()
    {
        $list = $this->BankM->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $rl) {
            if($rl->bank_status == 1){
                $status = "<span class='badge badge-success'>Active</span>";
            }elseif($rl->bank_status == 0) {
                $status = "<span class='badge badge-danger'>Deactive</span>";
            }
            $no++;
            $row = array();
            $row[] = "
            <td class='align-middle'>
            <img src='../assets/images/bank/$rl->bank_image'  class='rounded mr-3' style='height:60px;width:60px;'/>
            <p class='m-0 d-inline-block align-middle font-16'>
            $rl->bank_name
            </p>
            </td>
            ";
            $row[] = $rl->bank_no;
            $row[] = $status;
            $row[] = date('d M Y - H:i', strtotime($rl->created))." "."WIB";
            $row[] = '
            <td class="table-action">
                <a href="javascript:void(0)" onclick="bankEdit('."'".$rl->bank_id."'".')" class="btn btn-icon btn-outline-success"><i class="feather icon-edit"></i></a>
                <a href="javascript:void(0)" onclick="deleteBank('."'".$rl->bank_id."'".')" class="btn btn-icon btn-outline-danger"><i class="feather icon-trash-2"></i></a>
            </td>
            ';
        
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->BankM->count_all(),
                        "recordsFiltered" => $this->BankM->count_filtered(),
                        "data" => $data,
                );
        echo json_encode($output);
    }

    public function bankAdd() 
	{		
			
        $config['upload_path'] = './assets/images/bank/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 8000;
        $config['encrypt_name'] = TRUE;
        $config['remove_spaces'] = TRUE; 

        $this->upload->initialize($config);

        if(!empty($_FILES['image']['name'])){
            if ($this->upload->do_upload('image')){
                $img = $this->upload->data();
                $image			    = $img['file_name'];
                $bank_name			= $this->input->post('bank_name'); 
                $bank_no		    = $this->input->post('bank_no'); 
				$status			    = $this->input->post('status',true); 
                $data = [
                    'bank_name' => $bank_name,
					'bank_no' => $bank_no,
					'bank_status' => $status,
                    'bank_image' => $image,
                ];
                $result= $this->BankM->save($data);
                echo json_encode($result);
            }else{
                echo json_encode('Gagal'); 
            }
        }
			
	}

    public function editBank()
    {
        $config['upload_path'] = './assets/images/bank/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 8000;
        $config['encrypt_name'] = TRUE;

        $this->upload->initialize($config);

        if(!empty($_FILES['image']['name'])){
            if ($this->upload->do_upload('image')){
                $img = $this->upload->data();
                $image			= $img['file_name'];
                $bank_name			= $this->input->post('bank_name'); 
                $bank_no		    = $this->input->post('bank_no'); 
				$status			    = $this->input->post('status',true); 
                $gambar=$this->input->post('gambar');
                $path='./assets/images/bank/'.$gambar;
                unlink($path);
                
                $data = [
                    'bank_name' => $bank_name,
					'bank_no' => $bank_no,
					'bank_status' => $status,
                    'bank_image' => $image,
                    
                ];
                
                $this->BankM->update(array('bank_id' => $this->input->post('id')), $data);
                echo json_encode(array("status" => TRUE));
            }else{
                echo json_encode('Gagal'); 
            }
        }else{
            $bank_name			= $this->input->post('bank_name'); 
            $bank_no		    = $this->input->post('bank_no'); 
            $status			    = $this->input->post('status',true); 

            $data = [
                'bank_name' => $bank_name,
                'bank_no' => $bank_no,
                'bank_status' => $status
                
            ];
            
            $this->BankM->update(array('bank_id' => $this->input->post('id')), $data);
            echo json_encode(array("status" => TRUE));
        }

    } 

    public function bankEdit($id)
    {
        $data = $this->BankM->get_by_id($id); 
        echo json_encode($data);
    }

    public function bankDelete($id)
    {
        $foto = $this->db->get_where('bank_account', array('bank_id' => $id));
        if ($foto->num_rows() > 0) {
            $hasil = $foto->row();
            $nama_foto = $hasil->bank_image;
            if (file_exists($file = './assets/images/bank/' . $nama_foto)) {
                unlink($file);
            }
            $this->BankM->delete_by_id($id);
        }
        echo json_encode(array("status" => TRUE));
    }
	
}
