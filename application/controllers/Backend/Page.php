<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Page extends CI_Controller
{
	public $data = array();
    public $userid;
	public $role;
	
	function __construct()
	{ 
		parent::__construct();
		$this->data['isadmin'] = $this->session->userdata('logged_admin');
	}
// view
	function index()
	{
		if ($this->data['isadmin']) {
            $this->data['typeProduct'] = $this->TypeProductM->getData();
            $this->data['pagetitle'] = 'Page Management | Le Gris Home Furniture';
			$this->backend->display('backend/modules/pageV', $this->data); 
		} else {
            $this->data['pagetitle'] = 'Login | Le Gris Home Furniture';
            $this->load->view('backend/auth/loginV', $this->data); 
        }
	}

    public function pageList()
    {
        $list = $this->PageM->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $rl) {

            $no++;
            $row = array();
            $row[] = "
            <td class='align-middle'>
            <img src='../assets/images/page/$rl->image'  class='rounded mr-3' style='height:60px;width:60px;'/>
            <p class='m-0 d-inline-block align-middle font-16'>
            $rl->name_page
            </p>
            </td>
            ";
            $row[] = $rl->title_page;
            $row[] = date('d M Y - H:i', strtotime($rl->updated))." "."WIB";
            
            
            $row[] = '
            <td class="table-action">
                <a href="javascript:void(0)" onclick="pageEdit('."'".$rl->id_page."'".')" class="btn btn-icon btn-outline-success"><i class="feather icon-edit"></i></a>
                
            </td>
            ';
        
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->PageM->count_all(),
                        "recordsFiltered" => $this->PageM->count_filtered(),
                        "data" => $data,
                );
        echo json_encode($output);
    }

    public function editPage($id)
    {
        $data = $this->PageM->get_by_id($id); 
        echo json_encode($data);
    }
// end view

// function
    public function pageAdd() 
	{		
			
        $config['upload_path'] = './assets/images/page/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 8000;
        $config['encrypt_name'] = TRUE;
        $config['remove_spaces'] = TRUE; 

        $this->upload->initialize($config);

        if(!empty($_FILES['image']['name'])){
            if ($this->upload->do_upload('image')){
                $img = $this->upload->data();
                $image			= $img['file_name'];
                $name			= $this->input->post('name',true); 
                $title			= $this->input->post('title',true); 
                $desc			= $this->input->post('desc'); 
                $data = [
                    'name_page' => $name,
                    'image' => $image,
                    'title_page' => $title,
                    'desc_page' => $desc,
                ];
                
                $result= $this->PageM->save($data);
                echo json_encode($result);
            }else{
                echo json_encode('Gagal'); 
            }
        }
			
	}

    public function pageEdit()
    {
        $config['upload_path'] = './assets/images/page';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 8000;
        $config['encrypt_name'] = TRUE;

        $this->upload->initialize($config);

        if(!empty($_FILES['image']['name'])){
            if ($this->upload->do_upload('image')){
                $img = $this->upload->data();
                $image			= $img['file_name'];
                $name			= $this->input->post('name',true); 
                $title			= $this->input->post('title',true); 
                $desc			= $this->input->post('desc'); 
                $gambar=$this->input->post('gambar');
                $path='./assets/images/page/'.$gambar;
                unlink($path);
                
                $data = [
                    'name_page' => $name,
                    'image' => $image,
                    'title_page' => $title,
                    'desc_page' => $desc,
                    
                ];
                
                $this->PageM->update(array('id_page' => $this->input->post('id')), $data);
                echo json_encode(array("status" => TRUE));
            }else{
                echo json_encode('Gagal'); 
            }
        }else{
            $name			= $this->input->post('name',true); 
            $title			= $this->input->post('title',true); 
            $desc			= $this->input->post('desc'); 
            $data = [
                'name_page' => $name,
                'title_page' => $title,
                'desc_page' => $desc,
                
            ];
            
            $this->PageM->update(array('id_page' => $this->input->post('id')), $data);
            echo json_encode(array("status" => TRUE));
        }

    } 
     
    public function pageDelete($id)
    {
        $foto = $this->db->get_where('page', array('id_page' => $id));
        if ($foto->num_rows() > 0) {
            $hasil = $foto->row();
            $nama_foto = $hasil->image;
            if (file_exists($file = './assets/images/page/' . $nama_foto)) {
                unlink($file);
            }
            $this->PageM->delete_by_id($id);
        }
        echo json_encode(array("status" => TRUE));
    }


// end function
	
}
