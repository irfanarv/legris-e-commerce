<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Orders extends CI_Controller
{
    public $data = array();
    public $userid;
    public $role;

    function __construct()
    {
        parent::__construct();
        $this->data['isadmin'] = $this->session->userdata('logged_admin');
        $this->data['aproved'] = $this->session->userdata('fullName');
        $this->load->library('telegram/Telegram_lib','telegram/telegram_lib');
    }

    function index()
    {
        if ($this->data['isadmin']) {
            $this->data['pagetitle'] = 'Orders | Le Gris Home Furniture';
            $this->backend->display('backend/modules/ordersV', $this->data);
        } else {
            $this->data['pagetitle'] = 'Login | Le Gris Home Furniture';
            $this->load->view('backend/auth/loginV', $this->data);
        }
    }

    public function List()
    {
        $list = $this->OrdersM->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $rl) {
            $this->session->set_userdata('validInvoice', $rl->no_invoice);
            if ($rl->status_order == "Menunggu") {
                $status = "<span class='badge badge-danger'>UNPAID</span>";
            } elseif ($rl->status_order == "Selesai") {
                $status = "<span class='badge badge-success'>PAID</span>";
            } elseif ($rl->status_order == "Di Tolak") {
                $status = "<span class='badge badge-danger'>REJECT</span>";
            }
            $no++;
            $row = array();
            $row[] = $rl->no_invoice;
            $row[] = $rl->user_name;
            $row[] = $rl->user_mail;
            $row[] = $rl->user_phone;
            $row[] = $rl->user_city;
            $row[] = $status;
            $row[] = $rl->approved_by; 
            $row[] = date('d M Y - H:i', strtotime($rl->tanggal_transaksi)) . " " . "WIB";


            $row[] = '
            <td class="table-action">
                <a href="javascript:void(0)" onclick="updateStatus(' . "'" . $rl->no_invoice . "'" . ')" class="btn btn-icon btn-outline-success"><i class="feather icon-edit"></i></a>
                <a href="'.base_url('invoice/').$rl->no_invoice.'" target="_new" class="btn btn-icon btn-outline-info"><i class="feather icon-file-text"></i></a>
            </td>
            ';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->OrdersM->count_all(),
            "recordsFiltered" => $this->OrdersM->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    public function getOrder($id)
    {
        $data = $this->OrdersM->get_by_id($id);
        echo json_encode($data);
    }


    public function Update()
    {
        
        $status             = $this->input->post('status', true); 
        $invoice            = $this->input->post('id', true); 
        $this->session->set_userdata('validInvoice', $invoice);
        // 
        $user_mail          = $this->input->post('user_mail', true); 
        $user_name          = $this->input->post('user_name', true); 
        $user_phone         = $this->input->post('user_phone', true); 
        $user_address       = $this->input->post('user_address', true); 
        $user_city          = $this->input->post('user_city', true); 
        $user_state         = $this->input->post('user_state', true); 
        $user_zip           = $this->input->post('user_zip', true); 
        $total              = $this->input->post('total', true); 
        $bank_name          = $this->input->post('bank_name', true); 
        $bank_no            = $this->input->post('bank_no', true); 

        $user_id               = $this->data['aproved'];
        $data = [
            'status_order' => $status,
            'approved_by'  => $user_id
        ]; 
        $data2 = [
            'status' => $status,
            'approved'  => $user_id
        ]; 

        $this->OrdersM->update(array('no_invoice' => $this->input->post('id')), $data);
        $this->OrdersM->updateDetail(array('invoice_no' => $this->input->post('id')), $data2);
        
        $dataemail['email']     = $user_mail;
        $dataemail['name']      = $user_name;
        $dataemail['phone']     = $user_phone;
        $dataemail ['address']  = $user_address;
        $dataemail ['city']     = $user_city;
        $dataemail ['state']    = $user_state;
        $dataemail ['zip']      = $user_zip;

        if($status == "Menunggu") {
            $dataemail['status']  = "<span style='color:red'>UNPAID</span>";
        }elseif($status == "Selesai"){
            $dataemail['status']  = "<span style='color:green'>PAID</span>";
        }else{
            $dataemail['status']  = "<span style='color:red'>REJECT</span>";
        }
        $dataemail ['total']        = $total;
        $dataemail ['bank_name']    = $bank_name;
        $dataemail ['bank_no']      = $bank_no;
        $dataemail ['no_inv']       = $invoice;

        // send mail
        $this->load->library('email');
        $to_cc = array('support@legrishome.com', 'stock@legrishome.com', 'sales@legrishome.com','alfita@legrishome.com');
        $from   = "Legris Home <support@legrishome.com>";
        $_from  = "support@legrishome.com";
        $_me    = "Legris Home Furniture";
        $email_body = $this->load->view('template/email/invoice', $dataemail, true);
        $subject = 'Legris Home Furniture | Your Order';

        $tujuan['email_tujuan'] = $dataemail['email'];
        $config['protocol']    	= 'smtp';
        $config['smtp_host']    = 'mail.legrishome.com';
        $config['smtp_port']    = '587';
        $config['smtp_timeout'] = '7';
        $config['smtp_user']    = 'support@legrishome.com';
        $config['smtp_pass']    = 'EQJvEB1PpnTA';
        $config['charset']    = 'utf-8'; 
        
        $config['mailtype'] = 'html'; 
        $config['validation'] = FALSE;

        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($_from, $_me);
        $this->email->to($tujuan); 
        $this->email->cc($to_cc);
        $this->email->subject($subject);

        $this->email->message($email_body);  
        $send = $this->email->send();
        // telegram
        $this->session->set_userdata('validInvoice', $invoice);
        $viewinv = "https://legrishome.com/invoice/".$invoice;

        
$output = '';
$output = '
Update order status:

Invoice No #'.$invoice.'
Detail : '.$viewinv.'
';
$this->telegram_lib->sendmsg($output);
        
        if($send) {
            echo json_encode(array("status" => TRUE));
        } else {
            $error = $this->email->print_debugger(array('headers'));
            echo json_encode($error);
        }

	

    }



}
