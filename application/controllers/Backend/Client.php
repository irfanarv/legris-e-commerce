<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Client extends CI_Controller
{
	public $data = array();
    public $userid;
	public $role;
	
	function __construct()
	{ 
		parent::__construct();
		$this->data['isadmin'] = $this->session->userdata('logged_admin');
	}
// view
	function index()
	{
		if ($this->data['isadmin']) {
            $this->data['dataclient'] = $this->ClientM->getData(); 
            $this->data['pagetitle'] = 'Our Client | Le Gris Home Furniture';
			$this->backend->display('backend/modules/client/clientV', $this->data); 
		} else {
            $this->data['pagetitle'] = 'Login | Le Gris Home Furniture';
            $this->load->view('backend/auth/loginV', $this->data); 
        }
	} 

    public function upload()
    {
        $config['upload_path'] = './assets/images/client/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 8000;
        $config['encrypt_name'] = TRUE;
        $this->upload->initialize($config);

        if ($this->upload->do_upload('userfile')) {
            $nama = $this->upload->data('file_name');
            $token = $this->input->post('token_foto');
            $data = [
                'image'              => $nama,
                'token'     => $token
            ];

            $result = $this->ClientM->saveImage($data);
            echo json_encode($result);
        } else {
            $error = array('error' => $this->upload->display_errors());
            echo json_encode(['error' => $error]);
        }
    }

    public function photoDelete($id)
    {
        
        $foto = $this->db->get_where('client', array('id' => $id));
        if ($foto->num_rows() > 0) {
            $hasil = $foto->row();
            $nama_foto = $hasil->image;
            if (file_exists($file = './assets/images/client/' . $nama_foto)) {
                unlink($file);
            }
            $this->ClientM->delete_by_id($id);
        }
        echo json_encode(array("status" => TRUE));
    }
	
}
