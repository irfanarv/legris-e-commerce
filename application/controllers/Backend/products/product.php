<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Product extends CI_Controller
{
    public $data = array();
    public $userid;
    public $role;

    function __construct()
    {
        parent::__construct();
        $this->data['isadmin'] = $this->session->userdata('logged_admin');
    }
    // view
    function index()
    {
        if ($this->data['isadmin']) {
            $this->data['pagetitle'] = 'Products | Le Gris Home Furniture';
            $this->backend->display('backend/modules/product/productV', $this->data);
        } else {
            $this->data['pagetitle'] = 'Login | Le Gris Home Furniture';
            $this->load->view('backend/auth/loginV', $this->data);
        }
    }

    public function productList()
    {
        $list = $this->ProductM->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $rl) {
            if ($rl->product_status == 1) {
                $status = "<span class='badge badge-success'>Active</span>";
            } elseif ($rl->product_status == 0) {
                $status = "<span class='badge badge-danger'>Deactive</span>";
            }
            if ($rl->product_home == 1) {
                $ph = "<span class='badge badge-success'>Yes</span>";
            } elseif ($rl->product_home == 0) {
                $ph = "<span class='badge badge-danger'>No</span>";
            }
            $productname = substr($rl->product_name, 0, 25);
            $no++;
            $row = array();
            $row[] = "
            <td>
            <img src='../assets/images/product/$rl->product_image'  class='rounded mr-3' style='height:60px;width:60px;'/>
            <p class='m-0 d-inline-block align-middle font-16'>
            $productname
            
            </p>
            </td>
            ";
            $row[] = $rl->name;
            $row[] = 'Rp. ' . number_format($rl->product_price, 0, '', '.') . ',-';
            $row[] = $rl->product_qty;
            $row[] = $ph;
            $row[] = $status;
            $row[] = $rl->ai_first_name . ' ' . $rl->ai_last_name;
            $row[] = date('d M Y', strtotime($rl->product_date)) . " " . "WIB";

            $row[] = ' 
            <td class="table-action">
                
                <a data-toggle="tooltip" data-placement="top" title="Upload Photo" href="'.base_url('dashboard/products-gallery/').$rl->product_id.'" class="btn btn-icon btn-outline-info"><i class="feather icon-image"></i></a> 
                <a data-toggle="tooltip" data-placement="top" title="Edit" href="'.base_url('dashboard/edit-products/').$rl->product_id.'" class="btn btn-icon btn-outline-success"><i class="feather icon-edit"></i></a>
                <a data-toggle="tooltip" data-placement="top" title="Delete" href="javascript:void(0)" onclick="deleteProduct(' . "'" . $rl->product_id . "'" . ')" class="btn btn-icon btn-outline-danger"><i class="feather icon-trash-2"></i></a>
            </td>
            ';

            $data[] = $row;
        }

        // <a data-toggle="tooltip" data-placement="top" title="Variant Product" href="javascript:void(0)" onclick="categoryEdit(' . "'" . $rl->product_id . "'" . ')" class="btn btn-icon btn-outline-secondary"><i class="feather icon-box"></i></a>
        //         <a data-toggle="tooltip" data-placement="top" title="Detail Pengiriman" href="javascript:void(0)" onclick="categoryEdit(' . "'" . $rl->product_id . "'" . ')" class="btn btn-icon btn-outline-warning"><i class="feather icon-navigation"></i></a>
        //         <a data-toggle="tooltip" data-placement="top" title="Preview" href="javascript:void(0)" onclick="categoryEdit(' . "'" . $rl->product_id . "'" . ')" class="btn btn-icon btn-outline-primary"><i class="feather icon-file-text"></i></a>
        // <a data-toggle="tooltip" data-placement="top" title="Voucher" href="javascript:void(0)" onclick="voucher(' . "'" . $rl->product_id . "'" . ')" class="btn btn-icon btn-outline-danger"><i class="feather icon-award"></i></a>

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->ProductM->count_all(),
            "recordsFiltered" => $this->ProductM->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    // add view
    public function add()
    {
        if ($this->data['isadmin']) {
            $this->data['category'] = $this->CategoryM->getData();
            $this->data['pagetitle'] = 'Add New Product | Le Gris Home Furniture';
            $this->backend->display('backend/modules/product/productAddV', $this->data);
        } else {
            $this->data['pagetitle'] = 'Login | Le Gris Home Furniture';
            $this->load->view('backend/auth/loginV', $this->data);
        }
    }

    // save
    public function save()
    {
        $config['upload_path'] = './assets/images/product/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 8000;
        $config['encrypt_name'] = TRUE;
        $this->upload->initialize($config);

        if (!empty($_FILES['image']['name'])) {
            if ($this->upload->do_upload('image')) {
                $img                        = $this->upload->data();
                $image                      = $img['file_name'];
                $name                       = $this->input->post('name', true);
                $featured                   = $this->input->post('featured', true);
                $status                     = $this->input->post('status', true);
                $slug_1                     = strip_tags($name);
                $filter                     = str_replace("?", "", $slug_1);
                $filter2                    = str_replace("$", "", $filter);
                $pra_slug                   = $filter2;
                $slug                       = strtolower(str_replace(" ", "-", $pra_slug));
                $desc                       = $this->input->post('desc');
                $category                   = $this->input->post('category', true);
                $qty                        = $this->input->post('qty', true);
                $price                      = $this->input->post('price', true);
                $metatitle                  = $this->input->post('metatitle', true);
                $metadesc                   = $this->input->post('metadesc', true);
                $user_id                    = $this->data['isadmin'];
                $data = [
                    'product_name'      => $name,
                    'product_image'     => $image,
                    'product_cat'       => $category,
                    'product_slug'      => $slug,
                    'product_qty'       => $qty,
                    'product_price'     => $price,
                    'product_desc'      => $desc,
                    'product_home'       => $featured,
                    'product_status'    => $status,
                    'seo_title'         => $metatitle,
                    'seo_desc'          => $metadesc,
                    'user_id'           => $user_id

                ];

                $result = $this->ProductM->save($data);
                echo json_encode($result);
            } else {
                $error = array('error' => $this->upload->display_errors());
                echo json_encode(['error' => $error]);
            }
        }
    }

    // edit product
    public function update()
    {
        $config['upload_path'] = './assets/images/product/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 8000;
        $config['encrypt_name'] = TRUE;

        $this->upload->initialize($config);

        if(!empty($_FILES['image']['name'])){
            if ($this->upload->do_upload('image')){
                $img                        = $this->upload->data();
                $image                      = $img['file_name'];
                $name                       = $this->input->post('name', true);
                $featured                   = $this->input->post('featured', true);
                $status                     = $this->input->post('status', true);
                $slug_1                     = strip_tags($name);
                $filter                     = str_replace("?", "", $slug_1);
                $filter2                    = str_replace("$", "", $filter);
                $pra_slug                   = $filter2;
                $slug                       = strtolower(str_replace(" ", "-", $pra_slug));
                $desc                       = $this->input->post('desc');
                $category                   = $this->input->post('category', true);
                $qty                        = $this->input->post('qty', true);
                $price                      = $this->input->post('price', true);
                $metatitle                  = $this->input->post('metatitle', true);
                $metadesc                   = $this->input->post('metadesc', true);
                $user_id                    = $this->data['isadmin'];
                $gambar=$this->input->post('gambar');
                $path='./assets/images/product/'.$gambar;
                unlink($path);
                $data = [
                    'product_name'      => $name,
                    'product_image'     => $image,
                    'product_cat'       => $category,
                    'product_slug'      => $slug,
                    'product_qty'       => $qty,
                    'product_price'     => $price,
                    'product_desc'      => $desc,
                    'product_status'    => $status,
                    'seo_title'         => $metatitle,
                    'seo_desc'          => $metadesc,
                    'product_home'       => $featured,
                    'user_id'           => $user_id

                ];
                
                $this->ProductM->update(array('product_id' => $this->input->post('id')), $data);
                echo $this->session->set_flashdata('msg','sukses');
				redirect('dashboard/products');
            }else{
                echo $this->session->set_flashdata('msg','gagal');
				redirect('dashboard/products');
            }
        }else{
            $name                       = $this->input->post('name', true);
            $featured                   = $this->input->post('featured', true);
            $status                     = $this->input->post('status', true);
            $slug_1                     = strip_tags($name);
            $filter                     = str_replace("?", "", $slug_1);
            $filter2                    = str_replace("$", "", $filter);
            $pra_slug                   = $filter2;
            $slug                       = strtolower(str_replace(" ", "-", $pra_slug));
            $desc                       = $this->input->post('desc');
            $category                   = $this->input->post('category', true);
            $qty                        = $this->input->post('qty', true);
            $price                      = $this->input->post('price', true);
            $metatitle                  = $this->input->post('metatitle', true);
            $metadesc                   = $this->input->post('metadesc', true);
            $user_id                    = $this->data['isadmin'];
            $data = [
                'product_name'      => $name,
                'product_cat'       => $category,
                'product_slug'      => $slug,
                'product_qty'       => $qty,
                'product_price'     => $price,
                'product_desc'      => $desc,
                'product_home'      => $featured,
                'product_status'    => $status,
                'seo_title'         => $metatitle,
                'seo_desc'          => $metadesc,
                'user_id'           => $user_id

            ];
            
            $this->ProductM->update(array('product_id' => $this->input->post('id')), $data);
            echo $this->session->set_flashdata('msg','sukses');
            redirect('dashboard/products');
        }

    }

    // deleteproduct
    public function productDelete($id)
    {
        
        $foto = $this->db->get_where('gallery', array('product_id' => $id));
        $thumb = $this->db->get_where('product', array('product_id' => $id));
        if ($foto->num_rows() > 0) {
            $hasil = $foto->row();
            $nama_foto = $hasil->gallery_name;
            if (file_exists($file = './assets/images/gallery/' . $nama_foto)) {
                unlink($file);
            }
            $this->GalleryM->delete_by_product($id);
        }
        if ($thumb->num_rows() > 0) {
            $hasil = $thumb->row();
            $nama_foto = $hasil->product_image;
            if (file_exists($file = './assets/images/product/' . $nama_foto)) {
                unlink($file);
            }
            $this->ProductM->delete_by_id($id);
        }
        echo json_encode(array("status" => TRUE));
    }

    // edit view 
    public function edit($id)
    {
        if ($this->data['isadmin']) {
            $this->data['category'] = $this->CategoryM->getData();
            $this->data['productdata'] = $this->ProductM->get_by_id($id);
            $this->data['pagetitle'] = 'Edit Product | Le Gris Home Furniture';
            $this->backend->display('backend/modules/product/productEditV', $this->data);
        } else {
            $this->data['pagetitle'] = 'Login | Le Gris Home Furniture';
            $this->load->view('backend/auth/loginV', $this->data);
        }
    }


    // upload photo
    public function list_gallery($id)
    {
        if ($this->data['isadmin']) {
            $this->data['datagallery'] = $this->GalleryM->getDetail($id); 
            $this->data['dataproduct'] = $this->ProductM->getData($id);
            $this->data['pagetitle'] = 'Photo Product | Le Gris Home Furniture';
            $this->backend->display('backend/modules/product/galleryV', $this->data);
        } else {
            $this->data['pagetitle'] = 'Login | Le Gris Home Furniture';
            $this->load->view('backend/auth/loginV', $this->data);
        }
        
    }
    
    public function upload()
    {
        $config['upload_path'] = './assets/images/gallery/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 8000;
        $config['encrypt_name'] = TRUE;
        $this->upload->initialize($config);

        if ($this->upload->do_upload('userfile')) {
            $nama = $this->upload->data('file_name');
            $id = $this->input->post('id');
            $token = $this->input->post('token_foto');
            $data = [
                'gallery_name'      => $nama,
                'product_id'        => $id,
                'gallery_token'     => $token
            ];

            $result = $this->ProductM->saveImage($data);
            echo json_encode($result);
        } else {
            $error = array('error' => $this->upload->display_errors());
            echo json_encode(['error' => $error]);
        }
    }

    function removegallery()
    {
        $token = $this->input->post('token');
        $foto = $this->db->get_where('gallery', array('gallery_token' => $token));
        if ($foto->num_rows() > 0) {
            $hasil = $foto->row();
            $nama_foto = $hasil->gallery_name;
            if (file_exists($file = './assets/images/gallery/' . $nama_foto)) {
                unlink($file);
            }
            $this->db->delete('gallery', array('gallery_token' => $token));
        }
        echo "{}";
    }

    public function photoDelete($id)
    {
        
        $foto = $this->db->get_where('gallery', array('gallery_id' => $id));
        if ($foto->num_rows() > 0) {
            $hasil = $foto->row();
            $nama_foto = $hasil->gallery_name;
            if (file_exists($file = './assets/images/gallery/' . $nama_foto)) {
                unlink($file);
            }
            $this->GalleryM->delete_by_id($id);
        }
        echo json_encode(array("status" => TRUE));
    }
    // end upload photo

}
