<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Category extends CI_Controller
{
	public $data = array();
    public $userid;
	public $role;
	
	function __construct()
	{ 
		parent::__construct();
		$this->data['isadmin'] = $this->session->userdata('logged_admin');
	}
// view
	function index()
	{
		if ($this->data['isadmin']) {
            $this->data['typeProduct'] = $this->TypeProductM->getData();
            $this->data['pagetitle'] = 'Category Products | Le Gris Home Furniture';
			$this->backend->display('backend/modules/product/categoryV', $this->data); 
		} else {
            $this->data['pagetitle'] = 'Login | Le Gris Home Furniture';
            $this->load->view('backend/auth/loginV', $this->data); 
        }
	}

    public function categoryList()
    {
        $list = $this->CategoryM->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $rl) {
            if($rl->status == 1){
                $status = "<span class='badge badge-success'>Active</span>";
            }elseif($rl->status == 0) {
                $status = "<span class='badge badge-danger'>Deactive</span>";
            }

            if($rl->category_home == 1){
                $home = "<span class='badge badge-success'>Yes</span>";
            }elseif($rl->category_home == 0) {
                $home = "<span class='badge badge-danger'>No</span>";
            }

            if($rl->category_featured == 1){
                $featured = "<span class='badge badge-success'>Yes</span>";
            }elseif($rl->category_featured == 0) {
                $featured = "<span class='badge badge-danger'>No</span>";
            }

            $no++;
            $row = array();
            $row[] = "
            <td class='align-middle'>
            <img src='../assets/images/category/$rl->image'  class='rounded mr-3' style='height:60px;width:60px;'/>
            <p class='m-0 d-inline-block align-middle font-16'>
            $rl->name
            </p>
            </td>
            ";
            $row[] = $rl->name_type;
            $row[] = $status;
            $row[] = $home;
            $row[] = $featured;
            $row[] = date('d M Y - H:i', strtotime($rl->updatedAt))." "."WIB";
            
            
            $row[] = '
            <td class="table-action">
                <a href="javascript:void(0)" onclick="categoryEdit('."'".$rl->id."'".')" class="btn btn-icon btn-outline-success"><i class="feather icon-edit"></i></a>
                <a href="javascript:void(0)" onclick="deleteCategory('."'".$rl->id."'".')" class="btn btn-icon btn-outline-danger"><i class="feather icon-trash-2"></i></a>
            </td>
            ';
        
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->CategoryM->count_all(),
                        "recordsFiltered" => $this->CategoryM->count_filtered(),
                        "data" => $data,
                );
        echo json_encode($output);
    }

    public function editCategory($id)
    {
        $data = $this->CategoryM->get_by_id($id); 
        echo json_encode($data);
    }
// end view

// function
    public function categoryAdd() 
	{		
			
        $config['upload_path'] = './assets/images/category/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 8000;
        $config['encrypt_name'] = TRUE;
        $config['remove_spaces'] = TRUE; 

        $this->upload->initialize($config);

        if(!empty($_FILES['image']['name'])){
            if ($this->upload->do_upload('image')){
                $img = $this->upload->data();
                // $config['image_library']='gd2';
                // $config['source_image']='./assets/images/category/'.$img['file_name'];
                // $config['maintain_ratio']= FALSE;
                // $config['quality']= '100%';
                // $config['width']= 201;
                // $config['height']= 288;
                // $config['master_dim'] = 'width';
                // $config['new_image']= './assets/images/category/'.$img['file_name'];
                // $this->load->library('image_lib', $config);
                // $this->image_lib->resize();

                $image			= $img['file_name'];
                $name			= $this->input->post('name',true); 
                $status			= $this->input->post('status',true); 
                $type			= $this->input->post('type',true); 
                $home			= $this->input->post('home_category',true); 
                $feat			= $this->input->post('category_featured',true); 
                $slug_1=strip_tags($name);
                $filter=str_replace("?", "", $slug_1);
                $filter2=str_replace("$", "", $filter);
                $pra_slug=$filter2;
                $slug=strtolower(str_replace(" ", "-", $pra_slug));
                
                $data = [
                    'name' => $name,
                    'image' => $image,
                    'slug' => $slug,
                    'type_id' => $type,
                    'status' => $status,
                    'category_featured' => $feat,
                    'category_home' => $home
                    
                ];
                
                $result= $this->CategoryM->save($data);
                echo json_encode($result);
            }else{
                echo json_encode('Gagal'); 
            }
        }
			
	}

    public function categoryEdit()
    {
        $config['upload_path'] = './assets/images/category';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 8000;
        $config['encrypt_name'] = TRUE;

        $this->upload->initialize($config);

        if(!empty($_FILES['image']['name'])){
            if ($this->upload->do_upload('image')){
                $img = $this->upload->data();
                // $config['image_library']='gd2';
                // $config['source_image']='./assets/images/category/'.$img['file_name'];
                // $config['maintain_ratio']= FALSE;
                // $config['quality']= '100%';
                // $config['width']= 201;
                // $config['height']= 288;
                // $config['master_dim'] = 'width';
                // $config['new_image']= './assets/images/category/'.$img['file_name'];
                // $this->load->library('image_lib', $config);
                // $this->image_lib->resize();
                $image			= $img['file_name'];
                $name			= $this->input->post('name',true); 
                $status			= $this->input->post('status',true); 
                $type			= $this->input->post('type',true); 
                $home			= $this->input->post('home_category',true); 
                $feat			= $this->input->post('category_featured',true); 
                $slug_1=strip_tags($name);
                $filter=str_replace("?", "", $slug_1);
                $filter2=str_replace("$", "", $filter);
                $pra_slug=$filter2;
                $slug=strtolower(str_replace(" ", "-", $pra_slug));
                $gambar=$this->input->post('gambar');
                $path='./assets/images/category/'.$gambar;
                unlink($path);
                
                $data = [
                    'name' => $name,
                    'image' => $image,
                    'slug' => $slug,
                    'type_id' => $type,
                    'status' => $status,
                    'category_featured' => $feat,
                    'category_home' => $home
                    
                ];
                
                $this->CategoryM->update(array('id' => $this->input->post('id')), $data);
                echo json_encode(array("status" => TRUE));
            }else{
                echo json_encode('Gagal'); 
            }
        }else{
            $name			= $this->input->post('name',true); 
            $status			= $this->input->post('status',true); 
            $type			= $this->input->post('type',true); 
            $home			= $this->input->post('home_category',true); 
            $feat			= $this->input->post('category_featured',true); 
            $slug_1=strip_tags($name);
            $filter=str_replace("?", "", $slug_1);
            $filter2=str_replace("$", "", $filter);
            $pra_slug=$filter2;
            $slug=strtolower(str_replace(" ", "-", $pra_slug));

            $data = [
                'name' => $name,
                'slug' => $slug,
                'type_id' => $type,
                'status' => $status,
                'category_featured' => $feat,
                'category_home' => $home
                
            ];
            
            $this->CategoryM->update(array('id' => $this->input->post('id')), $data);
            echo json_encode(array("status" => TRUE));
        }

    } 
     
    public function categoryDelete($id)
    {
        $this->CategoryM->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }


// end function
	
}
