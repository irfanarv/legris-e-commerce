<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Type extends CI_Controller
{
    public $data = array();
    public $userid;
    public $role;

    function __construct()
    {
        parent::__construct();
        $this->data['isadmin'] = $this->session->userdata('logged_admin');
    }

    function index()
    {
        if ($this->data['isadmin']) {
            $this->data['pagetitle'] = 'Type Products | Le Gris Home Furniture';
            $this->backend->display('backend/modules/product/typeV', $this->data);
        } else {
            $this->data['pagetitle'] = 'Login | Le Gris Home Furniture';
            $this->load->view('backend/auth/loginV', $this->data);
        }
    }

    public function List()
    {
        $list = $this->TypeProductM->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $rl) {
            if ($rl->status_type == 1) {
                $status = "<span class='badge badge-success'>Active</span>";
            } elseif ($rl->status_type == 0) {
                $status = "<span class='badge badge-danger'>Deactive</span>";
            }
            $no++;
            $row = array();
            $row[] = $rl->name_type;
            $row[] = $status;
            $row[] = date('d M Y - H:i', strtotime($rl->type_createdAt)) . " " . "WIB";


            $row[] = '
            <td class="table-action">
                <a href="javascript:void(0)" onclick="typeEdit(' . "'" . $rl->id_type . "'" . ')" class="btn btn-icon btn-outline-success"><i class="feather icon-edit"></i></a>
                <a href="javascript:void(0)" onclick="deleteType(' . "'" . $rl->id_type . "'" . ')" class="btn btn-icon btn-outline-danger"><i class="feather icon-trash-2"></i></a>
            </td>
            ';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->TypeProductM->count_all(),
            "recordsFiltered" => $this->TypeProductM->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    public function editType($id)
    {
        $data = $this->TypeProductM->get_by_id($id);
        echo json_encode($data);
    }

    public function typeAdd()
    {
        $name            = $this->input->post('name', true);
        $status          = $this->input->post('status', true);
        $slug_1         = strip_tags($name);
        $filter         = str_replace("?", "", $slug_1);
        $filter2        = str_replace("$", "", $filter);
        $pra_slug       = $filter2;
        $slug           = strtolower(str_replace(" ", "-", $pra_slug));

        $data = [
            'name_type' => $name,
            'slug_type' => $slug,
            'status_type' => $status
        ];
        $result = $this->TypeProductM->save($data);
        echo json_encode($result);
    }

    public function typeEdit()
    {
        $name               = $this->input->post('name', true);
        $status             = $this->input->post('status', true);
        $slug_1 = strip_tags($name);
        $filter = str_replace("?", "", $slug_1);
        $filter2 = str_replace("$", "", $filter);
        $pra_slug = $filter2;
        $slug = strtolower(str_replace(" ", "-", $pra_slug));

        $data = [
            'name_type' => $name,
            'slug_type' => $slug,
            'status_type' => $status
        ];  
        $this->TypeProductM->update(array('id_type' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE));
    }

    public function typeDelete($id)
    {
        $this->TypeProductM->delete_by_id($id);  
        $this->CategoryM->deletebyType($id); 
        $this->ProductM->deletebyType($id); 
        echo json_encode(array("status" => TRUE));
    }
}
