<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Account extends CI_Controller
{
    public $data = array();
    public $userid;
    public $role;

    function __construct()
    {
        parent::__construct();
        $this->data['isadmin'] = $this->session->userdata('logged_admin');
    }


    // view
    function index()
    {
        if ($this->data['isadmin']) {
            $this->data['pagetitle'] = 'Account Management | Le Gris Home Furniture';
            $this->backend->display('backend/modules/accountV', $this->data);
        } else {  
            $this->data['pagetitle'] = 'Login | Le Gris Home Furniture';
            $this->load->view('backend/auth/loginV', $this->data);
        }
    }

    function member()
    {
        if ($this->data['isadmin']) {
            $this->data['pagetitle'] = 'Member Management | Le Gris Home Furniture';
            $this->backend->display('backend/modules/memberV', $this->data);
        } else {
            $this->data['pagetitle'] = 'Login | Le Gris Home Furniture';
            $this->load->view('backend/auth/loginV', $this->data);
        }
    }

    public function accountList()
    {
        $list = $this->Accounts_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $rl) {
            if ($rl->accounts_status == 'yes') {
                $status = "<span class='badge badge-success'>Active</span>";
            } elseif ($rl->accounts_status == 'no') {
                $status = "<span class='badge badge-danger'>Deactive</span>";
            }

            if ($rl->ai_image == NULL) {
                $ava = "<img src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRoyY5b_1yBKpIHqBpE1IOslAL3VOdtNbuCj0PZXGo6-HGUetscjNK6hbbnLaLJw7Y1aVI&usqp=CAU'  class='rounded mr-3' style='height:60px;width:60px;'/>";
            } else {
                $ava = "<img src='../../assets/images/profile/$rl->ai_image'  class='rounded mr-3' style='height:60px;width:60px;'/>";
            }
            $no++;
            $row = array();
            $row[] = "
            <td class='align-middle'>
            $ava
            <p class='m-0 d-inline-block align-middle font-16'>
            $rl->ai_first_name
            </p>
            </td>
            ";
            $row[] = $rl->accounts_email;
            $row[] = $status;
            $row[] = date('d M Y - H:i', strtotime($rl->accounts_updated_at)) . " " . "WIB";
            $row[] = '
            <td class="table-action">
                <a href="javascript:void(0)" onclick="editAccount(' . "'" . $rl->accounts_id . "'" . ')" class="btn btn-icon btn-outline-success"><i class="feather icon-edit"></i></a>
                <a href="javascript:void(0)" onclick="deleteBank(' . "'" . $rl->accounts_id . "'" . ')" class="btn btn-icon btn-outline-danger"><i class="feather icon-trash-2"></i></a>
            </td>
            ';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Accounts_model->count_all(),
            "recordsFiltered" => $this->Accounts_model->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    public function memberList()
    {
        $list = $this->Accounts_model->get_datatables2();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $rl) {
            if ($rl->accounts_status == 'yes') {
                $status = "<span class='badge badge-success'>Active</span>";
            } elseif ($rl->accounts_status == 'no') {
                $status = "<span class='badge badge-danger'>Deactive</span>";
            }

            if ($rl->ai_image == NULL) {
                $ava = "<img src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRoyY5b_1yBKpIHqBpE1IOslAL3VOdtNbuCj0PZXGo6-HGUetscjNK6hbbnLaLJw7Y1aVI&usqp=CAU'  class='rounded mr-3' style='height:60px;width:60px;'/>";
            } else {
                $ava = "<img src='../../assets/images/profile/$rl->ai_image'  class='rounded mr-3' style='height:60px;width:60px;'/>";
            }
            $no++;
            $row = array();
            $row[] = "
            <td class='align-middle'>
            $ava
            <p class='m-0 d-inline-block align-middle font-16'>
            $rl->ai_first_name
            </p>
            </td>
            ";
            $row[] = $rl->accounts_email;
            $row[] = $status;
            $row[] = date('d M Y - H:i', strtotime($rl->accounts_updated_at)) . " " . "WIB";
            
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Accounts_model->count_all2(),
            "recordsFiltered" => $this->Accounts_model->count_filtered2(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    public function accountAdd()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[accounts.accounts_email]', array('required' => 'Email Required', 'is_unique' => 'Oops Email already registered', 'valid_email' => 'Invalid Email'));
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[password]');
        if ($this->form_validation->run() == FALSE) {
            $errors = validation_errors();
            echo json_encode(['error' => $errors]);
        } else {
            $config['upload_path'] = './assets/images/profile/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 8000;
            $config['encrypt_name'] = TRUE;
            $config['remove_spaces'] = TRUE;

            $this->upload->initialize($config);

            if (!empty($_FILES['image']['name'])) {
                if ($this->upload->do_upload('image')) {
                    $img = $this->upload->data();
                    $image                = $img['file_name'];
                    $name                = $this->input->post('name', true);
                    $email              = $this->input->post('email', true);
                    $status                = $this->input->post('status', true);
                    $password            = $this->input->post('password', true);
                    $data = [
                        'ai_first_name'     => $name, 
                        'accounts_email'    => $email,
                        'accounts_status'   => $status,
                        'accounts_password' => sha1($password),
                        'ai_image'          => $image,
                        'accounts_type'     => "webadmin",
                        'accounts_is_admin'    => 1,
                    ];
                    $result = $this->Accounts_model->save($data);
                    echo json_encode($result);
                } else {
                    $error = array('error' => $this->upload->display_errors());
                    echo json_encode(['error' => $error]);
                }
            }
        }
    }

    public function editAccount()
    {
        $config['upload_path'] = './assets/images/profile/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 8000;
        $config['encrypt_name'] = TRUE;

        $this->upload->initialize($config);

        if (!empty($_FILES['image']['name'])) {
            if ($this->upload->do_upload('image')) {
                $img = $this->upload->data();
                $image            = $img['file_name'];
                $name                = $this->input->post('name', true);
                $email              = $this->input->post('email', true);
                $status                = $this->input->post('status', true);
                $password            = $this->input->post('password', true);
                $gambar = $this->input->post('gambar');
                $path = './assets/images/profile/' . $gambar;
                unlink($path);

                $data = [
                    'ai_first_name'     => $name,
                    'accounts_email'    => $email,
                    'accounts_status'   => $status,
                    'accounts_password' => sha1($password),
                    'ai_image'          => $image,

                ];

                $this->Accounts_model->update(array('accounts_id' => $this->input->post('id')), $data);
                echo json_encode(array("status" => TRUE));
            } else {
                echo json_encode('Gagal');
            }
        } else {
            $name                = $this->input->post('name', true);
            $email              = $this->input->post('email', true);
            $status                = $this->input->post('status', true);
            $password            = $this->input->post('password', true);

            $data = [
                'ai_first_name'     => $name,
                'accounts_email'    => $email,
                'accounts_status'   => $status,
                'accounts_password' => sha1($password),

            ];

            $this->Accounts_model->update(array('accounts_id' => $this->input->post('id')), $data);
            echo json_encode(array("status" => TRUE));
        }
    }

    public function accountEdit($id)
    {
        $data = $this->Accounts_model->get_by_id($id);
        echo json_encode($data);
    }

    public function accountDelete($id)
    {
        $foto = $this->db->get_where('accounts', array('accounts_id' => $id));
        if ($foto->num_rows() > 0) {
            $hasil = $foto->row();
            $nama_foto = $hasil->ai_image;
            if (file_exists($file = './assets/images/profile/' . $nama_foto)) {
                unlink($file);
            }
            $this->Accounts_model->delete_by_id($id);
        }
        echo json_encode(array("status" => TRUE));
    }
}
