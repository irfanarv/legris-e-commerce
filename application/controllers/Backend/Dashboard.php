<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Dashboard extends CI_Controller
{
	public $data = array();
    public $userid;
	public $role;
	
	function __construct()
	{ 
		parent::__construct();
		$this->data['isadmin'] = $this->session->userdata('logged_admin');
		$this->data['isSuperAdmin'] = $this->session->userdata('logged_super_admin');
        $this->userid = $this->session->userdata('logged_id');
        $this->data['accType'] = $this->session->userdata('accountType');
        $this->role = $this->session->userdata('role');
        $this->data['role'] = $this->role;
        $this->data['userloggedin'] = $this->session->userdata('logged_admin');
        $this->load->model('VisitorsM');
	}

	function index()
	{

		if ($this->validadmin()) {
            $this->data['visitor']        = $this->VisitorsM->statistik_pengujung();
            $this->data['monthvisit']     = $this->VisitorsM->visitor_month();
            $this->data['lastmonthvisit'] = $this->VisitorsM->visitor_last();
            $this->data['totalvisits'] = $this->VisitorsM->total_visits();
            $this->data['productTotal']   = $this->VisitorsM->totalProduct();
            $this->data['categoryTotal']   = $this->VisitorsM->totalCategory();
            $this->data['orderin']   = $this->VisitorsM->totalOrderNew();
            $this->data['memberTotal']   = $this->VisitorsM->totalMember();
            $this->data['pagetitle']= 'Dashboard | Le Gris Home Furniture';
			$this->backend->display('backend/modules/dashboardV', $this->data); 
		} else {
            $this->data['pagetitle'] = 'Login | Le Gris Home Furniture';
            $this->load->view('backend/auth/loginV', $this->data); 
        }
	}

	function login() {

        $username = $this->input->post('email');
        $password = $this->input->post('password');
        if ($this->input->is_ajax_request()) {

            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'required');

            if ($this->form_validation->run() == FALSE) { 

                $result = array("status" => false, "msg" => validation_errors(), "url" => "");
            } else {

                $login = $this->Accounts_model->login_admin($username, $password);
                if ($login) {
                    $prevurl = $this->session->userdata('prevURL');
                    if (!empty($prevurl)) {
                        $url = $prevurl;
                    } else {
                        $url = base_url() . 'dashboard';
                    }

                    $result = array("status" => true, "msg" => "", "url" => $url);
                } else {
                    $result = array("status" => false, "msg" => "Invalid Login Credentials", "url" => "");

                }

            }
            echo json_encode($result);

        }
    }

	

	function validadmin() {

        if (!empty($this->data['isadmin'])) {
            return true;
        } else {
            return false;
        }
	}

	function logout() {
        $lastlogin = $this->session->userdata('logged_time');
        $updatelogin = array('accounts_last_login' => $lastlogin);
        $this->db->where('accounts_id', $this->userid);
        $this->db->update('accounts', $updatelogin);
        $this->session->sess_destroy();
        redirect('dashboard');
    }
	
}
