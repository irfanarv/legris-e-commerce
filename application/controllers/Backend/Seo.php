<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Seo extends CI_Controller
{
    public $data = array();
    public $userid;
    public $role;

    function __construct()
    {
        parent::__construct();
        $this->data['isadmin'] = $this->session->userdata('logged_admin');
    }

    function index()
    {
        if ($this->data['isadmin']) {
            $this->data['pagetitle'] = 'Settings | Le Gris Home Furniture';
            $this->data['seodata'] = $this->FrontM->getSeo();
            $this->backend->display('backend/modules/seoV', $this->data);
        } else {
            $this->data['pagetitle'] = 'Login | Le Gris Home Furniture';
            $this->load->view('backend/auth/loginV', $this->data);
        }
    }

    function update(){
        $name               = $this->input->post('name', true);
        $title              = $this->input->post('title', true);
        $desc               = $this->input->post('desc', true);
        $keyword            = $this->input->post('keyword', true);
        $pesan              = $this->input->post('pesan', true);
        $ig_title           = $this->input->post('ig_title', true);
        $ig_uri             = $this->input->post('ig_uri', true);
        $address            = $this->input->post('address', true);
        $phone              = $this->input->post('phone', true);
        $email              = $this->input->post('email', true);
        $wholesale          = $this->input->post('wholesale', true);
        $maps               = $this->input->post('maps', true);
        $data = [
            'name'      => $name,
            'title'     => $title,
            'desc'      => $desc,
            'keyword'   => $keyword,
            'pesan'     => $pesan,
            'ig_title'  => $ig_title,
            'ig_uri'    => $ig_uri,
            'address'   => $address,
            'phone'     => $phone,
            'email'     => $email,
            'wholesale' => $wholesale,
            'maps'      => $maps,
        ];

        $this->FrontM->updateSeo(array('id' => 1), $data);
        echo $this->session->set_flashdata('msg','sukses');
        redirect('dashboard/settings');
        
    }

}
