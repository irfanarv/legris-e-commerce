<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Index extends CI_Controller
{
	public $data = array();
    public $userid;
	public $role;
	
	function __construct()
	{ 
		parent::__construct();
		$this->data['isadmin'] = $this->session->userdata('logged_admin');
	}

// view
	function index()
	{
		if ($this->data['isadmin']) {
            $this->data['pagetitle'] = 'News | Legris Home Furniture';
			$this->backend->display('backend/modules/news/indexV', $this->data); 
		} else {
            $this->data['pagetitle'] = 'Login | Legris Home Furniture';
            $this->load->view('backend/auth/loginV', $this->data); 
        }
	}

    public function List()
    {
        $list = $this->NewsM->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $rl) {
            if($rl->post_status == 1){
                $status = "<span class='badge badge-success'>Publish</span>";
            }elseif($rl->post_status == 0) {
                $status = "<span class='badge badge-danger'>Unpublish</span>";
            }
            $no++;
            $row = array();
            $row[] = "
            <td class='align-middle'>
            <img src='../assets/images/news/$rl->post_img'  class='rounded mr-3' style='height:60px;width:60px;'/>
            <p class='m-0 d-inline-block align-middle font-16'>
            $rl->post_title
            </p>
            </td>
            ";
            $row[] = $rl->cat_name;     
            $row[] = $rl->ai_first_name.''.$rl->ai_last_name;            
            $row[] = $status;
            $row[] = date('d M Y - H:i', strtotime($rl->post_created_at))." "."WIB";
            
            
            $row[] = '
            <td class="table-action">
                <a data-toggle="tooltip" data-placement="top" title="Edit" href="'.base_url('dashboard/edit-news/').$rl->post_id.'" class="btn btn-icon btn-outline-success"><i class="feather icon-edit"></i></a>
                
                <a href="javascript:void(0)" onclick="deletePost('."'".$rl->post_id."'".')" class="btn btn-icon btn-outline-danger"><i class="feather icon-trash-2"></i></a>
            </td>
            ';
        
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->CategoryNewsM->count_all(),
                        "recordsFiltered" => $this->CategoryNewsM->count_filtered(),
                        "data" => $data,
                );
        echo json_encode($output);
    }

    // add page

    function add()
	{
		if ($this->data['isadmin']) {
            $this->data['pagetitle'] = 'Add News | Legris Home Furniture';
            $this->data['category'] = $this->CategoryNewsM->getData();
			$this->backend->display('backend/modules/news/addNewsV', $this->data); 
		} else {
            $this->data['pagetitle'] = 'Login | Legris Home Furniture';
            $this->load->view('backend/auth/loginV', $this->data); 
        }
	}

    public function edit($id)
    {
        if ($this->data['isadmin']) {
            $this->data['category'] = $this->CategoryNewsM->getData();
            $this->data['newsdata'] = $this->NewsM->get_by_id($id);
            $this->data['pagetitle'] = 'Edit News | Le Gris Home Furniture';
            $this->backend->display('backend/modules/news/editNews', $this->data);
        } else {
            $this->data['pagetitle'] = 'Login | Le Gris Home Furniture';
            $this->load->view('backend/auth/loginV', $this->data);
        }
    } 

    public function editCategory($id)
    {
        $data = $this->CategoryNewsM->get_by_id($id);
        echo json_encode($data);
    }
// end view

// function
    public function save() 
	{		

        $config['upload_path'] = './assets/images/news/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 8000;
        $config['encrypt_name'] = TRUE;
        $this->upload->initialize($config);

        if(!empty($_FILES['image']['name'])){
            if ($this->upload->do_upload('image')){
                $img = $this->upload->data();
                $image			= $img['file_name'];
                $title			= $this->input->post('title',true); 
                $status			= $this->input->post('status',true); 
                $slug_1         =strip_tags($title);
                $filter         =str_replace("?", "", $slug_1);
                $filter2        =str_replace("$", "", $filter);
                $pra_slug       =$filter2;
                $slug           =strtolower(str_replace(" ", "-", $pra_slug));
                $details		= $this->input->post('content',true); 
                $category		= $this->input->post('category',true);
                $meta_key		= $this->input->post('meta_key',true); 
                $meta_desc		= $this->input->post('meta_desc',true);
                $user_id        = $this->data['isadmin'];
                $data = [
                    'post_title'            => $title,
                    'post_slug'             => $slug,
                    'post_desc'             => $details,
                    'post_category'         => $category,
                    'post_img'              => $image,
                    'post_status'           => $status,
                    'post_meta_keywords'    => $meta_key,
                    'post_meta_desc'        => $meta_desc,
                    'user_id'               => $user_id     
                    
                ];
                
                $result= $this->NewsM->save($data);
                echo json_encode($result);
            }else{
                echo json_encode('Gagal'); 
            }
        }
			
	}

    public function update()
    {
        $config['upload_path'] = './assets/images/news/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 8000;
        $config['encrypt_name'] = TRUE;
        $this->upload->initialize($config);

        if(!empty($_FILES['image']['name'])){
            if ($this->upload->do_upload('image')){
                $img                        = $this->upload->data();
                $image                      = $img['file_name'];
                $title			= $this->input->post('title',true); 
                $status			= $this->input->post('status',true); 
                $slug_1         =strip_tags($title);
                $filter         =str_replace("?", "", $slug_1);
                $filter2        =str_replace("$", "", $filter);
                $pra_slug       =$filter2;
                $slug           =strtolower(str_replace(" ", "-", $pra_slug));
                $details		= $this->input->post('content',true); 
                $category		= $this->input->post('category',true);
                $meta_key		= $this->input->post('meta_key',true); 
                $meta_desc		= $this->input->post('meta_desc',true);
                $user_id        = $this->data['isadmin'];
                $gambar=$this->input->post('gambar');
                $path='./assets/images/news/'.$gambar;
                unlink($path);
                $data = [
                    'post_title'            => $title,
                    'post_slug'             => $slug,
                    'post_desc'             => $details,
                    'post_category'         => $category,
                    'post_img'              => $image,
                    'post_status'           => $status,
                    'post_meta_keywords'    => $meta_key,
                    'post_meta_desc'        => $meta_desc,
                    'user_id'               => $user_id   

                ];
                
                $result= $this->NewsM->update(array('post_id' => $this->input->post('id')), $data);
                echo json_encode($result);
            }else{
                echo json_encode('Gagal'); 
            }
        }else{
            $title			    = $this->input->post('title',true); 
                $status			= $this->input->post('status',true); 
                $slug_1         =strip_tags($title);
                $filter         =str_replace("?", "", $slug_1);
                $filter2        =str_replace("$", "", $filter);
                $pra_slug       =$filter2;
                $slug           =strtolower(str_replace(" ", "-", $pra_slug));
                $details		= $this->input->post('content',true); 
                $category		= $this->input->post('category',true);
                $meta_key		= $this->input->post('meta_key',true); 
                $meta_desc		= $this->input->post('meta_desc',true);
                $user_id        = $this->data['isadmin'];
                $data = [
                    'post_title'            => $title,
                    'post_slug'             => $slug,
                    'post_desc'             => $details,
                    'post_category'         => $category,
                    'post_status'           => $status,
                    'post_meta_keywords'    => $meta_key,
                    'post_meta_desc'        => $meta_desc,
                    'user_id'               => $user_id     
                    
                ];
            
            $result=  $this->NewsM->update(array('post_id' => $this->input->post('id')), $data);
            echo json_encode($result);
        }

    }

    public function categoryEdit()
    {
        $name			= $this->input->post('name',true); 
        $status			= $this->input->post('status',true); 
        $slug_1=strip_tags($name);
        $filter=str_replace("?", "", $slug_1);
        $filter2=str_replace("$", "", $filter);
        $pra_slug=$filter2;
        $slug=strtolower(str_replace(" ", "-", $pra_slug));

        $data = [
            'cat_name' => $name,
            'cat_slug' => $slug,
            'cat_status' => $status
            
        ];
        
        $this->CategoryNewsM->update(array('cat_id' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE));

    } 
    
    public function categoryDelete($id)
    {
        $this->CategoryNewsM->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }

    public function delete($id)
    {
        
        $thumb = $this->db->get_where('news', array('post_id' => $id));
        if ($thumb->num_rows() > 0) {
            $hasil = $thumb->row();
            $nama_foto = $hasil->post_img;
            if (file_exists($file = './assets/images/news/' . $nama_foto)) {
                unlink($file);
            }
            $this->NewsM->delete_by_id($id);
        }
        echo json_encode(array("status" => TRUE));
    }


// end function
	
}
