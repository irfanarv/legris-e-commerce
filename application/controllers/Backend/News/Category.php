<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Category extends CI_Controller
{
	public $data = array();
    public $userid;
	public $role;
	
	function __construct()
	{ 
		parent::__construct();
		$this->data['isadmin'] = $this->session->userdata('logged_admin');
	}
// view
	function index()
	{
		if ($this->data['isadmin']) {
            $this->data['pagetitle'] = 'Category News | Legris Home';
			$this->backend->display('backend/modules/news/categoryV', $this->data); 
		} else {
            $this->data['pagetitle'] = 'Login | Legris Home';
            $this->load->view('backend/auth/loginV', $this->data); 
        }
	}

    public function categoryList()
    {
        $list = $this->CategoryNewsM->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $rl) {
            if($rl->cat_status == 1){
                $status = "<span class='badge badge-success'>Active</span>";
            }elseif($rl->cat_status == 0) {
                $status = "<span class='badge badge-danger'>Deactive</span>";
            }
            $no++;
            $row = array();
            $row[] = $rl->cat_name;            
            $row[] = $status;

            $row[] = date('d M Y - H:i', strtotime($rl->cat_date))." "."WIB";
            
            
            $row[] = '
            <td class="table-action">
                <a href="javascript:void(0)" onclick="categoryEdit('."'".$rl->cat_id."'".')" class="btn btn-icon btn-outline-success"><i class="feather icon-edit"></i></a>
                <a href="javascript:void(0)" onclick="deleteCategory('."'".$rl->cat_id."'".')" class="btn btn-icon btn-outline-danger"><i class="feather icon-trash-2"></i></a>
            </td>
            ';
        
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->CategoryNewsM->count_all(),
                        "recordsFiltered" => $this->CategoryNewsM->count_filtered(),
                        "data" => $data,
                );
        echo json_encode($output);
    }

    public function editCategory($id)
    {
        $data = $this->CategoryNewsM->get_by_id($id);
        echo json_encode($data);
    }
// end view
 
// function
    public function categoryAdd() 
	{		
        $name			= $this->input->post('name',true); 
        $status			= $this->input->post('status',true); 
        $slug_1         =strip_tags($name);
        $filter         =str_replace("?", "", $slug_1);
        $filter2        =str_replace("$", "", $filter);
        $pra_slug       =$filter2;
        $slug           =strtolower(str_replace(" ", "-", $pra_slug));

        $data = [
            'cat_name' => $name,
            'cat_slug' => $slug,
            'cat_status' => $status
            
        ];
        $result= $this->CategoryNewsM->save($data);
        echo json_encode($result);
			
	}

    public function categoryEdit()
    {
        $name			= $this->input->post('name',true); 
        $status			= $this->input->post('status',true); 
        $slug_1=strip_tags($name);
        $filter=str_replace("?", "", $slug_1);
        $filter2=str_replace("$", "", $filter);
        $pra_slug=$filter2;
        $slug=strtolower(str_replace(" ", "-", $pra_slug));

        $data = [
            'cat_name' => $name,
            'cat_slug' => $slug,
            'cat_status' => $status
            
        ];
        
        $this->CategoryNewsM->update(array('cat_id' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE));

    } 
    
    public function categoryDelete($id)
    {
        $this->CategoryNewsM->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }


// end function
	
}
