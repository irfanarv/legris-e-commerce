<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->helper('string');
		$this->VisitorsM->count_visitor(); 
	}
	
	// login logout
	function attemp()
    {

        $username = $this->input->post('email');
        $password = $this->input->post('password');
        if ($this->input->is_ajax_request()) {

            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'required');

            if ($this->form_validation->run() == FALSE) {
                $errors = validation_errors();
				echo json_encode(['error'=>$errors]);
            } else {
                $data = $this->Accounts_model->checkEmail($username);
                if($data['accounts_email'] == '')
                {
                    echo json_encode(['error'=>'Email not found']);
                }else{
                    if(sha1($password, $data['accounts_password']))
                    {
                        $this->session->set_userdata('id', $data['accounts_id']);
                        $this->session->set_userdata('name', $data['ai_first_name']);
						$this->session->set_userdata('mail', $data['accounts_email']);
						$this->session->set_userdata('image', $data['ai_image']);
                        echo json_encode(['status'=> true, 'Success']);
                    }
                    else
                    {
                        echo json_encode(['error'=>'Wrong Password']);
                    }
                }
            }
            
        }
    }

	public function logout()
	{
	 $this->session->unset_userdata('mail');
	 $this->session->unset_userdata('name');
	 redirect();
	}

	public function registerCustomer()
	{
		$this->load->library('form_validation'); 
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[accounts.accounts_email]', array('required' => 'Email Required', 'is_unique' => 'Oops Email already registered', 'valid_email' => 'Invalid Email'));
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');
		$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[password]');
		if ($this->form_validation->run() == FALSE) {
			$errors = validation_errors();
			echo json_encode(['error' => $errors]);
		} else {
			$email			= $this->input->post('email', true);
			$password		= $this->input->post('password', true);
			$full_name 		= $this->input->post('full_name');

			$data = [
				'accounts_email'	=> $email,
				'ai_first_name'		=> $full_name,
				'accounts_password' => password_hash($password, PASSWORD_BCRYPT),
				'accounts_type' 	=> "customers",
				'accounts_is_admin'	=> 0,
				'accounts_status'	=> "yes"
			];
			$this->session->set_userdata('mail', $email);
			$this->session->set_userdata('name', $full_name);
			$this->Accounts_model->CustomerRegister($data);
			echo json_encode(['success' => 'Record added successfully.']);
		}
	}

	public function accountCustomer(){
		$menus  		= $this->FrontM->getMenu();
		$title  		= "Le Gris Home Furniture | My Account";
		$bankLogo 		= $this->FrontM->getBankLogo();
		$mail 			=$this->session->userdata('mail');
		$countWish		= $this->FrontM->countWish($mail);
		$alamat 		= $this->FrontM->getAddress($mail);
		$orderlist 		= $this->FrontM->getOrder($mail); 
		$setting 		= $this->FrontM->get_site_data()->row_array(); 
		$data 			= array(
			'menus' 		=> $menus,
			'pagetitle' 	=> $title,
			'bankLogo'		=> $bankLogo,
			'countWish'		=> $countWish,
			'alamat'		=> $alamat,
			'order'			=> $orderlist,
			// settings
			'pesan'			=> $setting['pesan'],
			'meta_name'		=> $setting['name'],
			'meta_title'	=> $setting['title'],
			'meta_desc'		=> $setting['desc'],
			'keyword'		=> $setting['keyword'],
			'ig_title'		=> $setting['ig_title'],
			'ig_uri'		=> $setting['ig_uri'],
			'address'		=> $setting['address'],
			'phone'			=> $setting['phone'],
			'email'			=> $setting['email'],
			'wholesale'		=> $setting['wholesale'],
			'maps'			=> $setting['maps'],
		);
		if ($this->session->userdata('mail')) {
			$this->frontend->display('frontend/modules/dashboard', $data);
		}else{
			echo $this->session->set_flashdata('msg', 'login');
			redirect();
		}
	}

	public function wishlist()
	{
		$mail 				= $this->session->userdata('mail');
		$menus  			= $this->FrontM->getMenu();
		$wishList  			= $this->FrontM->getWishlist($mail);
        $title  			= "Le Gris Home Furniture | wishlist";
		$bankLogo 			= $this->FrontM->getBankLogo();
		$setting 			= $this->FrontM->get_site_data()->row_array(); 
		$countWish			= $this->FrontM->countWish($mail);
		$data 				= array(
			'menus' 		    => $menus,
			'pagetitle'         => $title,
			'bankLogo'			=> $bankLogo,
			'wishList'			=> $wishList,
			'countWish'			=> $countWish,
			// settings
			'pesan'			=> $setting['pesan'],
			'meta_name'		=> $setting['name'],
			'meta_title'	=> $setting['title'],
			'meta_desc'		=> $setting['desc'],
			'keyword'		=> $setting['keyword'],
			'ig_title'		=> $setting['ig_title'],
			'ig_uri'		=> $setting['ig_uri'],
			'address'		=> $setting['address'],
			'phone'			=> $setting['phone'],
			'email'			=> $setting['email'],
			'wholesale'		=> $setting['wholesale'],
			'maps'			=> $setting['maps'],
		);
		if ($this->session->userdata('mail')) {
			$this->frontend->display('frontend/modules/wishlist', $data);
		}else{
			echo $this->session->set_flashdata('msg', 'login');
			redirect();
		}
	}

	public function updateAddress(){
			$mail 			= $this->session->userdata('mail');
			$address		= $this->input->post('address'); 
            $phone			= $this->input->post('phone',true); 
            $city			= $this->input->post('city',true); 
            $state			= $this->input->post('state',true); 
            $zip			= $this->input->post('zip',true); 
            $data = [
                'ai_address' => $address,
                'ai_phone' => $phone,
                'ai_city' => $city,
                'ai_state' => $state,
                'ai_zip' => $zip
                
            ];
            
            $this->FrontM->AddressUpdate(array('accounts_email' => $mail), $data); 
			echo $this->session->set_flashdata('msg','sukses');
			redirect('my-account');
	}


}
