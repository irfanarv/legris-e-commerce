<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Toc extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->VisitorsM->count_visitor(); 
	}

	public function index()
	{
		$menus  		= $this->FrontM->getMenu();
		$title  		= "Le Gris Home Furniture | Term and Condition";
		$bankLogo 		= $this->FrontM->getBankLogo();
		$mail 			= $this->session->userdata('mail');
		$countWish		= $this->FrontM->countWish($mail);
		$clientLogo  	= $this->FrontM->getClientLogo();
		$content	  	= $this->FrontM->getToc();
		$setting 		= $this->FrontM->get_site_data()->row_array(); 
		
		$data 			= array(
			'menus' 		=> $menus,
			'pagetitle' 	=> $title,
			'bankLogo'		=> $bankLogo,
			'countWish'		=> $countWish,
			'clientLogo'	=> $clientLogo,
			'content'		=> $content,
			'pesan'			=> $setting['pesan'],
			'meta_name'		=> $setting['name'],
			'meta_title'	=> $setting['title'],
			'meta_desc'		=> $setting['desc'],
			'keyword'		=> $setting['keyword'],
			'ig_title'		=> $setting['ig_title'],
			'ig_uri'		=> $setting['ig_uri'],
			'address'		=> $setting['address'],
			'phone'			=> $setting['phone'],
			'email'			=> $setting['email'],
			'wholesale'		=> $setting['wholesale'],
			'maps'			=> $setting['maps'],
		);
		$this->frontend->display('frontend/modules/tocV', $data);
	}

	
}
