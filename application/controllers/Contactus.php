<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Contactus extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->VisitorsM->count_visitor();
	}

	public function email(){
        $this->load->view('template/email/invoice');
    }

	public function index()
	{
		$menus  		= $this->FrontM->getMenu();
		$title  		= "Le Gris Home Furniture | Contact - Us";
		$bankLogo 		= $this->FrontM->getBankLogo();
		$mail 			= $this->session->userdata('mail');
		$countWish		= $this->FrontM->countWish($mail);
		$setting 		= $this->FrontM->get_site_data()->row_array(); 
		$data 			= array( 
			'menus' 		=> $menus,
			'pagetitle' 	=> $title,
			'bankLogo'		=> $bankLogo,
			'countWish'		=> $countWish,
			'pesan'			=> $setting['pesan'],
			'meta_name'		=> $setting['name'],
			'meta_title'	=> $setting['title'],
			'meta_desc'		=> $setting['desc'],
			'keyword'		=> $setting['keyword'],
			'ig_title'		=> $setting['ig_title'],
			'ig_uri'		=> $setting['ig_uri'],
			'address'		=> $setting['address'],
			'phone'			=> $setting['phone'],
			'email'			=> $setting['email'],
			'wholesale'		=> $setting['wholesale'],
			'maps'			=> $setting['maps'],
		);
		$this->frontend->display('frontend/modules/contact', $data);
	}

	public function send()
	{
			$setting 		= $this->FrontM->get_site_data()->row_array(); 
			$this->load->library('email');
            $name = $this->input->post("fname");
            $email = $this->input->post("email");
            $pno = $this->input->post("telp");
			$subject = $this->input->post("subject");
            $message = $this->input->post("message");
			$tujuan['email_tujuan'] = $setting['email'];
            $config['protocol']    	= 'smtp';
            $config['smtp_host']    = 'mail.legrishome.com';
            $config['smtp_port']    = '587';
            $config['smtp_timeout'] = '7';
            $config['smtp_user']    = 'support@legrishome.com';
            $config['smtp_pass']    = 'EQJvEB1PpnTA';
            $config['charset']    = 'utf-8'; 
            
            $config['mailtype'] = 'text'; 
            $config['validation'] = FALSE;

            $this->email->initialize($config);
			$this->email->set_newline("\r\n");
            $this->email->from($email,$name, $pno);
            $this->email->to($tujuan); 
            $this->email->subject($subject);

           $this->email->message($message);  
            $send = $this->email->send();
            if($send) {
                echo json_encode("send");
            } else {
                $error = $this->email->print_debugger(array('headers'));
                echo json_encode($error);
            }

		
	}
}
