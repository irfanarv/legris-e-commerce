<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mission extends CI_Controller
{

	private $ctrl = "mission";

	/**== Construct ==**/
	function __construct()
	{
		parent::__construct();


		// Load model
		$this->load->model('accounts_model', '', TRUE);
		$this->load->model('mission_hits_model', '', TRUE);
		$this->load->model('spin_wheel_prize_model', '', TRUE);
	}


	/**== Index Page ==**/
	public function index()
	{
		authenticatePage('USER');

		// Get data
		$uid =  $this->session->userdata('id');

		// Set data view
		$data['content_view'] = $this->ctrl . '/index.php';
		$data['page_title'] = "Misi";
		$data['profile'] = $this->accounts_model->get($uid);

		// Load view
		$this->load->view(FRONTEND_LAYOUT, $data);
	}


	/**== Index Page ==**/
	public function page($mission_url)
	{
		// Set data view
		$data['content_view'] = 'frontend/' . $this->ctrl . '/mission.php';
		$data['page_title'] = "Misi";
		$data['mission_url'] = base_url('m/' . $mission_url);

		// Load view
		$this->load->view($data['content_view'], $data);
	}


	/**== Mission 1 ==**/
	public function one()
	{
		authenticatePage('USER');

		// Get data
		$uid =  $this->session->userdata('id');
		$profile = $this->accounts_model->get($uid);
		$mission_point = $this->mission_hits_model->getPoint(1, $profile->id);
		
		// Set data view
		$data['content_view'] = 'frontend/' . $this->ctrl . '/mission-1.php';
		$data['profile'] = $this->accounts_model->get($uid);
		$data['mission_point'] = $mission_point;

		// Load view
		$this->load->view($data['content_view'], $data);
	}


	/**== Mission 2 ==**/
	public function two()
	{
		authenticatePage('USER');

		// Get data
		$uid =  $this->session->userdata('id');
		$profile = $this->accounts_model->get($uid);
		$prize = $this->spin_wheel_prize_model->get_list();
		$avaliable = $this->spin_wheel_prize_model->get_avaliable_list();
		$av = array();
		foreach ($avaliable as $v) {
			$av[]  = $v->sort_no;
		}
		$avaliable =  $av[array_rand($av)];
		$mission_point = $this->mission_hits_model->getPoint(2, $profile->id);

		// Set data view
		$data['content_view'] = 'frontend/' . $this->ctrl . '/mission-2.php';
		$data['profile'] = $this->accounts_model->get($uid);
		$data['ctrl'] = $this->ctrl;
		$data['play_total'] = 1;
		$data['prize'] = $prize;
		$data['avaliable'] = $avaliable;
		$data['mission_point'] = $mission_point;

		// Load view
		$this->load->view($data['content_view'], $data);
	}


	/**== Mission 3 ==**/
	public function three()
	{
		authenticatePage('USER');

		// Get data
		$uid =  $this->session->userdata('id');
		$profile = $this->accounts_model->get($uid);
		$mission_point = $this->mission_hits_model->getPoint(3, $profile->id);

		// Set data view
		$data['content_view'] = 'frontend/' . $this->ctrl . '/mission-3.php';
		$data['profile'] = $this->accounts_model->get($uid);
		$data['mission_point'] = $mission_point;

		// Load view
		$this->load->view($data['content_view'], $data);
	}


	/**== Mission 4 ==**/
	public function four()
	{
		authenticatePage('USER');

		// Get data
		$uid =  $this->session->userdata('id');
		$profile = $this->accounts_model->get($uid);
		$mission_point = $this->mission_hits_model->getPoint(4, $profile->id);

		// Set data view
		$data['content_view'] = 'frontend/' . $this->ctrl . '/mission-4.php';
		$data['profile'] = $this->accounts_model->get($uid);
		$data['mission_point'] = $mission_point;

		// Load view
		$this->load->view($data['content_view'], $data);
	}


	/**== Mission 5 ==**/
	public function five()
	{
		authenticatePage('USER');

		// Get data
		$uid =  $this->session->userdata('id');
		$profile = $this->accounts_model->get($uid);
		$mission_point = $this->mission_hits_model->getPoint(5, $profile->id);

		// Set data view
		$data['content_view'] = 'frontend/' . $this->ctrl . '/mission-5.php';
		$data['profile'] = $this->accounts_model->get($uid);
		$data['mission_point'] = $mission_point;

		// Load view
		$this->load->view($data['content_view'], $data);
	}



	/**== Hit Mission ==**/
	public function hit($m_code)
	{

		$this->load->library('user_agent');

		// Get data
		$m_code = explode("-", $m_code);
		$mission = $m_code[0];
		$ref_code = $m_code[1];
		$profile = $this->accounts_model->getByRefCode($ref_code);
		// print_r($m_code[0]);die;

		if ($mission == 1) {
			$redirect = "https://www.instagram.com/ar/526465668637522";
			$data['mission'] = 1;
			$data['point'] = 0;
		} elseif ($mission == 3) {
			$redirect = "https://funbid.id";
			$misi = $data['mission'] = 3;
			$data['point'] = 10;
			$this->SendNotif($misi);
		} elseif ($mission == 4) {
			$redirect = "https://www.youtube.com/channel/UC0DCovQ6Gztbx2YoGu1lvGA";
			$misi = $data['mission'] = 4;
			$data['point'] = 10;
			$this->SendNotif($misi);
		} elseif ($mission == 5) {
			$redirect = "https://www.linkedin.com/feed/update/urn:li:activity:6831862412765990912";
			$misi =  $data['mission'] = 5;
			$data['point'] = 10;
			$this->SendNotif($misi);
		}

		// Set data
		$data['account_id'] = $profile->id;
		$data['ip_address'] = @$this->input->ip_address();
		$data['datetime'] = date("Y-m-d H:i:s");
		$data['agent'] = $this->agent->agent_string();

		// Save data
		$this->mission_hits_model->save($data);
		return redirect($redirect);
	}


	/**== Index Page ==**/
	public function submitSpin()
	{
		authenticatePage('USER');

		$this->load->library('user_agent');

		// Get data
		$uid =  $this->session->userdata('id');
		$profile = $this->accounts_model->get($uid);

		$code = $this->input->post('code');
		$code_exp = explode("-", $code);
		// print_r($code_exp);

		// Set data
		$misi = $data['mission'] = 2;
		$data['point'] = $code_exp[1];
		$data['account_id'] = $profile->id;
		$data['ip_address'] = @$this->input->ip_address();
		$data['datetime'] = date("Y-m-d H:i:s");
		$data['agent'] = $this->agent->agent_string();
		$this->SendNotif($misi);
		// Save data
		$this->mission_hits_model->save($data);

		redirect(base_url("mission?m=2"));
	}

	public function SendNotif($misi)
	{
		// Get data
		$uid =  $this->session->userdata('id');
		$profile = $this->accounts_model->get($uid);
		$ip_address = @$this->input->ip_address();

		$getData = $this->mission_hits_model->checkPoint($misi, $profile->id, $ip_address);
		if ($getData == NULL){
			$title = '#TangguhUntukMaju';
			if($misi == 2){
				$notif = 'Selamat kamu berhasil mendapatkan point';
			}
			$notif = 'Selamat kamu berhasil mendapatkan 10 point';
			$getToken 	= $this->accounts_model->getTokenDevice($profile->id);
			foreach ($getToken as $key => $value) {
				"{$value}";
				$accessToken	= 'AAAAIYWlEDI:APA91bG6DE2uKqARoyt04S7ikfuXROTNox_FDijKMZ1YVf4xAR8l7RdW-zIFL4R6zWypzLcW0KyXZ3GUyyqcn5Bzq2Ov6NKdYz0RFAhjC1DZGSTq5XlQKtAEeYoIgbNXRTqQw2PzI1RB';
				$post_data = '
				{
					"to" : "' . $value . '",
					"data" : {
					"body" : "' . $notif . '",
					"title" : "' . $title . '",
					"message" : "' . $notif . '",
					},
					"notification" : {
						"body" : "' . $notif . '",
						"title" : "' . $title . '",
						"message" : "' . $notif . '",
						"icon": "https://webly.id/assets/img/favicon-3.png",
						"click_action": "https://webly.id/merdeka/mission",
						"sound" : "default"
						}
				}';

				$crl = curl_init();

				$headr = array();
				$headr[] = 'Content-type: application/json';
				$headr[] = 'Authorization: key=' . $accessToken;
				curl_setopt($crl, CURLOPT_SSL_VERIFYPEER, false);

				curl_setopt($crl, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
				curl_setopt($crl, CURLOPT_HTTPHEADER, $headr);
				curl_setopt($crl, CURLOPT_POST, true);
				curl_setopt($crl, CURLOPT_POSTFIELDS, $post_data);
				curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);

				$rest = curl_exec($crl);
			}
		}
	}



}
