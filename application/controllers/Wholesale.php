<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Wholesale extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->VisitorsM->count_visitor(); 
	}

	function index()
	{
		$menus  		= $this->FrontM->getMenu();
		$title  		= "Le Gris Home Furniture | Wholesale";
		$bankLogo 		= $this->FrontM->getBankLogo();
		$mail 			= $this->session->userdata('mail');
		$countWish		= $this->FrontM->countWish($mail);
		$setting 		= $this->FrontM->get_site_data()->row_array(); 
		$data 			= array(
			'menus' 		=> $menus,
			'pagetitle' 	=> $title,
			'bankLogo'		=> $bankLogo,
			'countWish'		=> $countWish,
			'pesan'			=> $setting['pesan'],
			'meta_name'		=> $setting['name'],
			'meta_title'	=> $setting['title'],
			'meta_desc'		=> $setting['desc'],
			'keyword'		=> $setting['keyword'],
			'ig_title'		=> $setting['ig_title'],
			'ig_uri'		=> $setting['ig_uri'],
			'address'		=> $setting['address'],
			'phone'			=> $setting['phone'],
			'email'			=> $setting['email'],
			'wholesale'		=> $setting['wholesale'],
			'maps'			=> $setting['maps'],
		);
		if ($this->session->userdata('access-catalouge')) {
			$this->frontend->display('frontend/modules/catalougeV', $data);
		}else{
			echo $this->session->set_flashdata('msg', 'access-catalouge');
			redirect();
		}
		
	}

	function openCatalouge()
    {
		$setting 		= $this->FrontM->get_site_data()->row_array(); 
        $password = $this->input->post('password');
        if ($this->input->is_ajax_request()) {
            $this->form_validation->set_rules('password', 'Password', 'required');

            if ($this->form_validation->run() == FALSE) {
                $errors = validation_errors();
				echo json_encode(['error'=>$errors]);
            } else {

                    if(($password == $setting['wholesale']))
                    {
                        $this->session->set_userdata('access-catalouge', '12345');
                        echo json_encode(['status'=> true, 'Success']);
                    }
                    else
                    {
                        echo json_encode(['error'=>'Wrong Password']);
                    }
                
            }
            
        }
    }
}
