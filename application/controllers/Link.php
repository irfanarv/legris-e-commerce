<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Link extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->VisitorsM->count_visitor(); 
	}

	public function index()
	{
		$setting = $this->FrontM->get_site_data()->row_array(); 
		$content	  	= $this->FrontM->getLink();
		$title  		= "Legris Link | Le Gris Home Furniture";
		$data 			= array(
			'content'		=> $content,
			'pagetitle' 	=> $title,
			'pesan'			=> $setting['pesan'],
			'meta_name'		=> $setting['name'],
			'meta_title'	=> $setting['title'],
			'meta_desc'		=> $setting['desc'],
			'keyword'		=> $setting['keyword'],
			'ig_title'		=> $setting['ig_title'],
			'ig_uri'		=> $setting['ig_uri'],
			'address'		=> $setting['address'],
			'phone'			=> $setting['phone'],
			'email'			=> $setting['email'],
			'wholesale'		=> $setting['wholesale'],
			'maps'			=> $setting['maps'],
		);
		$this->load->view('frontend/modules/linkV', $data);
	}

	
}
