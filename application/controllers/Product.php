<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Product extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->VisitorsM->count_visitor(); 
	}

	public function index()
	{
		$menus  		= $this->FrontM->getMenu();
        $fetchProduct 	= $this->FrontM->getAllProduct();
        $fetchCategory 	= $this->FrontM->getCategory();
		$title  		= "Le Gris Home Furniture | Products";
		$bankLogo 		= $this->FrontM->getBankLogo();
		$mail 			=$this->session->userdata('mail');
		$countWish		= $this->FrontM->countWish($mail);
		$setting 		= $this->FrontM->get_site_data()->row_array(); 
		$data 			= array(
			'menus' 		=> $menus,
			'pagetitle' 	=> $title,
            'product'       => $fetchProduct,
            'category'       => $fetchCategory,
			'bankLogo'		=> $bankLogo,
			'countWish'		=> $countWish,
			'pesan'			=> $setting['pesan'],
			'meta_name'		=> $setting['name'],
			'meta_title'	=> $setting['title'],
			'meta_desc'		=> $setting['desc'],
			'keyword'		=> $setting['keyword'],
			'ig_title'		=> $setting['ig_title'],
			'ig_uri'		=> $setting['ig_uri'],
			'address'		=> $setting['address'],
			'phone'			=> $setting['phone'],
			'email'			=> $setting['email'],
			'wholesale'		=> $setting['wholesale'],
			'maps'			=> $setting['maps'],
		);
		$this->frontend->display('frontend/modules/product-list', $data);
	}

    public function detail($slug)
	{
		$menus  		= $this->FrontM->getMenu();
        $fetchProduct 	= $this->FrontM->detailProduct($slug);
        $fetchGallery 	= $this->FrontM->galleryProduct($slug);
		$title  		= "Le Gris Home Furniture | Detail Product";
		$bankLogo 		= $this->FrontM->getBankLogo();
		$mail 			=$this->session->userdata('mail');
		$countWish		= $this->FrontM->countWish($mail);
		$setting 		= $this->FrontM->get_site_data()->row_array(); 
		$data 			= array(
			'menus' 		    => $menus,
			'pagetitle'         => $title,
            'productDetail'     => $fetchProduct,
            'productGallery'    => $fetchGallery,
			'bankLogo'		=> $bankLogo,
			'countWish'		=> $countWish,
			'pesan'			=> $setting['pesan'],
			'meta_name'		=> $setting['name'],
			'meta_title'	=> $setting['title'],
			'meta_desc'		=> $setting['desc'],
			'keyword'		=> $setting['keyword'],
			'ig_title'		=> $setting['ig_title'],
			'ig_uri'		=> $setting['ig_uri'],
			'address'		=> $setting['address'],
			'phone'			=> $setting['phone'],
			'email'			=> $setting['email'],
			'wholesale'		=> $setting['wholesale'],
			'maps'			=> $setting['maps'],
		);
		$this->frontend->display('frontend/modules/product-detail', $data);
	}

	// type-product
	public function categoryProduct($slug)
	{
		$menus  			= $this->FrontM->getMenu();
        $fetchByCategory	= $this->FrontM->getProductbyCategory($slug);
		$fetchCategory 		= $this->FrontM->getCategory();
		$getCategory 		= $this->FrontM->getCategoryBySlug($slug);
		$title  			= "Le Gris Home Furniture | Product by Category";
		$bankLogo 			= $this->FrontM->getBankLogo();
		$mail 			=$this->session->userdata('mail');
		$countWish		= $this->FrontM->countWish($mail);
		$setting 		= $this->FrontM->get_site_data()->row_array(); 
		$data 				= array(
			'menus' 		    => $menus,
			'pagetitle'         => $title,
            'productCategory'   => $fetchByCategory,
			'category'       	=> $fetchCategory,
			'fortitle'       	=> $getCategory,
			'bankLogo'		=> $bankLogo,
			'countWish'		=> $countWish,
			'pesan'			=> $setting['pesan'],
			'meta_name'		=> $setting['name'],
			'meta_title'	=> $setting['title'],
			'meta_desc'		=> $setting['desc'],
			'keyword'		=> $setting['keyword'],
			'ig_title'		=> $setting['ig_title'],
			'ig_uri'		=> $setting['ig_uri'],
			'address'		=> $setting['address'],
			'phone'			=> $setting['phone'],
			'email'			=> $setting['email'],
			'wholesale'		=> $setting['wholesale'],
			'maps'			=> $setting['maps'],
		);
		$this->frontend->display('frontend/modules/product-categories', $data);
	}

}
