<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Blog extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->VisitorsM->count_visitor(); 
	}

	public function index()
	{
		$menus  		= $this->FrontM->getMenu();
		$title  		= "Le Gris Home Furniture | Blog";
		$bankLogo 		= $this->FrontM->getBankLogo();
		$mail 			= $this->session->userdata('mail');
		$countWish		= $this->FrontM->countWish($mail);
		$newsList 		= $this->NewsM->getData();
		$setting 		= $this->FrontM->get_site_data()->row_array(); 
		
		$data 			= array(
			'menus' 		=> $menus,
			'pagetitle' 	=> $title,
			'bankLogo'		=> $bankLogo,
			'countWish'		=> $countWish,
			'news'			=> $newsList,
			'pesan'			=> $setting['pesan'],
			'meta_name'		=> $setting['name'],
			'meta_title'	=> $setting['title'],
			'meta_desc'		=> $setting['desc'],
			'keyword'		=> $setting['keyword'],
			'ig_title'		=> $setting['ig_title'],
			'ig_uri'		=> $setting['ig_uri'],
			'address'		=> $setting['address'],
			'phone'			=> $setting['phone'],
			'email'			=> $setting['email'],
			'wholesale'		=> $setting['wholesale'],
			'maps'			=> $setting['maps'],
		);
		$this->frontend->display('frontend/modules/blog-list', $data);
	}

	public function detail($slug)
	{
		$menus  		= $this->FrontM->getMenu();
		$title  		= "Le Gris Home Furniture | Blog Detail ";
		$bankLogo 		= $this->FrontM->getBankLogo();
		$catBlog 		= $this->NewsM->getCategory();
		$mail 			= $this->session->userdata('mail');
		$newsList 		= $this->NewsM->getDetails($slug); 
		$countWish		= $this->FrontM->countWish($mail);
		$news			= $this->NewsM->getDataHome();
		$setting 		= $this->FrontM->get_site_data()->row_array(); 
		
		$data 			= array(
			'menus' 		=> $menus,
			'pagetitle' 	=> $title,
			'bankLogo'		=> $bankLogo,
			'countWish'		=> $countWish,
			'news'			=> $newsList,
			'catblog'		=> $catBlog,
			'latepost'		=> $news,
			'pesan'			=> $setting['pesan'],
			'meta_name'		=> $setting['name'],
			'meta_title'	=> $setting['title'],
			'meta_desc'		=> $setting['desc'],
			'keyword'		=> $setting['keyword'],
			'ig_title'		=> $setting['ig_title'],
			'ig_uri'		=> $setting['ig_uri'],
			'address'		=> $setting['address'],
			'phone'			=> $setting['phone'],
			'email'			=> $setting['email'],
			'wholesale'		=> $setting['wholesale'],
			'maps'			=> $setting['maps'],
		);
		$this->frontend->display('frontend/modules/blog-detail', $data);
	}

	
}
