<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Checkout extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->VisitorsM->count_visitor(); 
		$this->load->library('telegram/Telegram_lib','telegram/telegram_lib');
	}

	public function index()
	{
		$menus  		= $this->FrontM->getMenu();
		$title  		= "Le Gris Home Furniture | Checkout";
		$bankLogo 		= $this->FrontM->getBankLogo();
		$bankAccount 	= $this->FrontM->bankAccount();
		$mail 			= $this->session->userdata('mail');
		$account		= $this->FrontM->getAddressCheckout($mail)->row_array(); 
		$countWish		= $this->FrontM->countWish($mail);
		$setting 		= $this->FrontM->get_site_data()->row_array(); 
		
		$data 			= array(
			'menus' 		=> $menus,
			'pagetitle' 	=> $title,
			'bankLogo'		=> $bankLogo,
			'countWish'		=> $countWish,
			'bankAccount'	=> $bankAccount,
			'ai_address'	=> $account['ai_address'],
			'full_name'		=> $account['ai_first_name'],
			'accounts_email'=> $account['accounts_email'],
			'ai_phone'		=> $account['ai_phone'],
			'city'			=> $account['ai_city'],
			'state'			=> $account['ai_state'],
			'zip'			=> $account['ai_zip'],

			'pesan'			=> $setting['pesan'],
			'meta_name'		=> $setting['name'],
			'meta_title'	=> $setting['title'],
			'meta_desc'		=> $setting['desc'],
			'keyword'		=> $setting['keyword'],
			'ig_title'		=> $setting['ig_title'],
			'ig_uri'		=> $setting['ig_uri'],
			'address'		=> $setting['address'],
			'phone'			=> $setting['phone'],
			'email'			=> $setting['email'],
			'wholesale'		=> $setting['wholesale'],
			'maps'			=> $setting['maps'],
		);
		$this->frontend->display('frontend/modules/checkout', $data);
	}

	public function process()
	{
		
		if($this->cart->contents() !==[]){
            $invoice = $this->FrontM->get_inv();
                $i = 0;
                $total		= $this->cart->total();

                foreach ($this->cart->contents() as $insert){
                    $id 		= $insert['id'];
                    $q 			= $insert['qty'];
                    $sub_total	= $insert['subtotal'];
                    $rowid 		= $insert['rowid'];
                    $tgl 		= date('Y-m-d');
                    $datestring = '%H:%i';
                    $time 		= time();
                    
                    $data = [
                        'invoice_no' => $invoice,
                        'product_id' => $insert['id'],
                        'product_name' => $insert['name'],
                        'price' => $insert['price'],
                        'jumlah' => $insert['qty'],
                        'sub_total' => $insert['subtotal'],
                    ];
                    $masuk[] = $data;
                    $i++; 
 
                }
                	$insert = $this->db->insert_batch('detail_pembelian', $masuk);
					$full_name       = $this->input->post('full_name', true);
					$email           = $this->input->post('email', true);
					$phone           = $this->input->post('phone', true);
					$address         = $this->input->post('address', true);
					$city            = $this->input->post('city', true);
					$state           = $this->input->post('state', true);
					$zip             = $this->input->post('zip', true);
					$bank_id         = $this->input->post('bank_id', true);
					$data2 = [
                        'no_invoice' => $invoice,
                        'user_name' => $full_name,
                        'user_mail' => $email,
                        'user_phone' => $phone,
                        'user_address' => $address,
                        'user_city' => $city,
						'user_state' => $state,
						'user_zip' => $zip,
						'id_bank' => $bank_id,
						'total' => $total,
                    ];
					$this->FrontM->saveOrder($data2);
					
                
                if($insert){
                    $this->cart->destroy();
					$this->session->set_userdata('validInvoice', $invoice);
					$this->load->library('email');
					// send mail
					$dataemail['email']     = $email;
					$dataemail['name']      = $full_name;
					$dataemail['phone']     = $phone;
					$dataemail ['address']  = $address;
					$dataemail ['city']     = $city;
					$dataemail ['state']    = $state;
					$dataemail ['zip']      = $zip;
					$dataemail['status']  	= "<span style='color:red'>UNPAID</span>";

					$dataemail ['total']        = $total;
					$dataemail ['no_inv']       = $invoice;
					
					$from   = "Legris Home <support@legrishome.com>";
					$to_cc = array('support@legrishome.com', 'stock@legrishome.com', 'sales@legrishome.com','alfita@legrishome.com');
					$_from  = "support@legrishome.com";
					$_me    = "Legris Home Furniture"; 
					$email_body = $this->load->view('template/email/invoice', $dataemail, true);
					$subject = 'Legris Home Furniture | Your Order';

					$tujuan['email_tujuan'] = $dataemail['email'];
					$config['protocol']    	= 'smtp';
					$config['smtp_host']    = 'mail.legrishome.com';
					$config['smtp_port']    = '587';
					$config['smtp_timeout'] = '7';
					$config['smtp_user']    = 'support@legrishome.com';
					$config['smtp_pass']    = 'EQJvEB1PpnTA';
					$config['charset']    = 'utf-8'; 
					
					$config['mailtype'] = 'html'; 
					$config['validation'] = FALSE;

					$this->email->initialize($config);
					$this->email->set_newline("\r\n");
					$this->email->from($_from, $_me);
					$this->email->to($tujuan); 
					$this->email->cc($to_cc);
					$this->email->subject($subject);
					$this->email->message($email_body); 
					$send = $this->email->send();
					// telegram
					$this->session->set_userdata('validInvoice', $invoice);
					$viewinv = "https://legrishome.com/invoice/".$invoice;
$output = '
New order Revices:

Invoice No #'.$invoice.'
Detail : '.$viewinv.'
';
$this->telegram_lib->sendmsg($output);
					if($send) {
						echo json_encode(array("status" => TRUE));
					} else {
						$error = $this->email->print_debugger(array('headers'));
						echo json_encode($error);
					}
					redirect('invoice/'.$invoice);
                }else{
                    echo $this->session->set_flashdata('msg','gagal');
                }
            // ADD TO DATABASE
        }else{
            echo('Transaksi Kosong');
        }
	}

	public function getInvoice($invoice){
		$datainvoice 	= $this->FrontM->getInvoice($invoice); 
		$detailinvoice 	= $this->FrontM->getDetailInv($invoice); 
		$setting 		= $this->FrontM->get_site_data()->row_array(); 
		$title  		= "Invoice#".$invoice;
		$data 			= array(
			'pagetitle' 	=> $title,
			'pesan'			=> $setting['pesan'],
			'meta_name'		=> $setting['name'],
			'meta_title'	=> $setting['title'],
			'meta_desc'		=> $setting['desc'],
			'keyword'		=> $setting['keyword'],
			'invoicedata'	=> $datainvoice,
			'invoicedetail'	=> $detailinvoice
		);
		// if ($this->session->userdata('validInvoice') || $this->session->userdata('mail')) {
			$this->load->view('frontend/modules/invoice', $data);
		// }else{
		// 	redirect();
		// }

	}
}
