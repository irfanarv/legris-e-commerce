<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->VisitorsM->count_visitor(); 
	}

	// home
	public function index() 
	{
		$hotProduct  	= $this->FrontM->hotProduct();
		$categoryHome  	= $this->FrontM->getCategoryHome();
		$clientLogo  	= $this->FrontM->getClientLogo();
		$title  		= "Le Gris Home Furniture";
		$slider 		= $this->FrontM->getSliders();
		$featcat 		= $this->FrontM->getCategoryFeatured();
		// all controllers
		$menus  		= $this->FrontM->getMenu();
		$bankLogo 		= $this->FrontM->getBankLogo();
		$mail 			=$this->session->userdata('mail');
		$countWish		= $this->FrontM->countWish($mail); 
		$newsList 		= $this->NewsM->getDataHome();

		// settings
		$setting = $this->FrontM->get_site_data()->row_array(); 
		$data 			= array(
			'menus' 		=> $menus,
			'pagetitle' 	=> $title,
			'sliders' 		=> $slider,
			'hotProduct'	=> $hotProduct,
			'bankLogo'		=> $bankLogo,
			'categoryHome'	=> $categoryHome,
			'clientLogo'	=> $clientLogo,
			'featcat'		=> $featcat,
			'countWish'		=> $countWish,
			'news'			=> $newsList,
			// settings
			'pesan'			=> $setting['pesan'],
			'meta_name'		=> $setting['name'],
			'meta_title'	=> $setting['title'],
			'meta_desc'		=> $setting['desc'],
			'keyword'		=> $setting['keyword'],
			'ig_title'		=> $setting['ig_title'],
			'ig_uri'		=> $setting['ig_uri'],
			'address'		=> $setting['address'],
			'phone'			=> $setting['phone'],
			'email'			=> $setting['email'],
			'wholesale'		=> $setting['wholesale'],
			'maps'			=> $setting['maps'],
		);
		$this->frontend->display('frontend/modules/home', $data);
	}

	// end home


	// cart
	function AddCart()
	{
		$data = array(
			'id' => $this->input->post('produk_id'),
			'name' => $this->input->post('produk_nama'),
			'price' => $this->input->post('produk_harga'),
			'image' => $this->input->post('produk_image'),
			'qty' => $this->input->post('quantity')
		);

		$this->cart->insert($data);
		echo $this->showCart();
	}
	public function AddWish()
	{

		$data = [
			'id_product' => $this->input->post('produk_id'),
			'user_mail' => $this->input->post('user_mail')
		];

		$result = $this->FrontM->saveWish($data);
		echo json_encode($result);
	}

	function showCart()
	{
		$output = '';
		$output = '<h3>Shopping Cart</h3>';
		$no = 0;
		foreach ($this->cart->contents() as $items) {
			$no++;
			$output .= '
			<ul>
                <li>
					<div class="cart-img">
						<img src="' . base_url() . '/assets/images/product/' . $items['image'] . '" style="width:60px !important">
					</div>
					<div class="cart-title">
						<h4>' . $items['name'] . '</h4>
						<span>' . $items['qty'] . ' x '  . 'Rp ' . number_format($items['subtotal']) . ' </span>
					</div>
                    <div class="cart-delete">
						<button type="button" id="' . $items['rowid'] . '" class="deleteItem btn">x</button>
					</div>
                </li>
			</ul>
            ';
		}
		$output .= '
		<div class="cart-total">
			<h4>Subtotal: <span>' . 'Rp. ' . number_format($this->cart->total()) . '</span></h4>
		</div>
		<div class="checkout-btn btn-hover">
			<a class="theme-color" href="' . base_url() . 'checkout">Checkout</a>
		</div>
        ';
		return $output;
	}
	function loadCart()
	{
		echo $this->showCart();
	}

	function deleteCart()
	{
		$data = array(
			'rowid' => $this->input->post('row_id'),
			'qty' => 0,
		);
		$this->cart->update($data);
		echo $this->showCart();
	}

	function deleteWish()
	{
		
		$id = $this->input->post('wish_id');
		$this->FrontM->delete_by_id($id);
	}
	// end cart
}
