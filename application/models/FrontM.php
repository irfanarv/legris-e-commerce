<?php

class FrontM extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function getMenu()
	{
		$this->db->select("*");
		$this->db->from("type_product");
		$this->db->where('status_type', '1');
		$q = $this->db->get();

		$final = array();
		if ($q->num_rows() > 0) {
			foreach ($q->result() as $row) {

				$this->db->select("*");
				$this->db->from("category");
				$this->db->where("type_id", $row->id_type);
				$this->db->where('status', '1');
				$q = $this->db->get();
				if ($q->num_rows() > 0) {
					$row->category = $q->result();
				}
				array_push($final, $row);
			}
		}
		return $final;
	}
	// home
	function hotProduct()
	{
		$this->db->select("*");
		$this->db->from("product");
		$this->db->join('category', 'category.id=product.product_cat');
		$this->db->join('type_product', 'type_product.id_type=category.type_id');
		$this->db->where('product_status', '1');
		$this->db->where('product_home', '1');
		$this->db->where('product_qty > ', '0');
		$query = $this->db->get();
		return $query->result();
	}

	function getSliders()
	{
		$this->db->from('slider');
		$this->db->where('status', '1');
		$query = $this->db->get();
		return $query->result();
	}
	// home
	// product
	// fetch product
	function getAllProduct()
	{
		$this->db->from('product');
		$this->db->where('product_status', '1');
		$this->db->where('product_qty > ', '0');
		$query = $this->db->get();
		return $query->result();
	}
	// fetch product
	// category
	function getCategory()
	{
		$this->db->from('category');
		$this->db->where('status', '1');
		$query = $this->db->get();
		return $query->result();
	}
	function getCategoryBySlug($slug)
	{
		$this->db->from('category');
		$this->db->where('status', '1');
		$this->db->where('slug', $slug);
		$query = $this->db->get();
		return $query->result();
	}
	// category
	// detail product
	function detailProduct($slug)
	{
		$this->db->select("*");
		$this->db->from("product");
		$this->db->join('category', 'category.id=product.product_cat');
		$this->db->join('type_product', 'type_product.id_type=category.type_id');
		$this->db->where('product_slug', $slug);
		$this->db->where('product_status', '1');
		$this->db->where('product_qty > ', '0');
		$query = $this->db->get();
		return $query->result();
	}
	// detail product
	// gallery product
	function galleryProduct($slug)
	{
		$this->db->select('gallery.gallery_name', 'gallery.product_id', 'product.product_id', 'product.product_slug');
		$this->db->from("gallery");
		$this->db->join('product', 'product.product_id=gallery.product_id');
		$this->db->where('product_slug', $slug);
		$query = $this->db->get();
		return $query->result();
	}
	// gallery product
	// type product
	function getProductbyCategory($slug)
	{
		$this->db->select("*");
		$this->db->from("product");
		$this->db->join('category', 'category.id=product.product_cat');
		$this->db->where('product_status', '1');
		$this->db->where('status', '1');
		$this->db->where('product_qty > ', '0');
		$this->db->where('slug', $slug);
		$query = $this->db->get();
		return $query->result();
	}
	// type product
	// product

	// home category
	function getCategoryHome()
	{
		$this->db->select("*");
		$this->db->from("category");
		$this->db->where('status', '1');
		$this->db->where('category_home', '1');
		$query = $this->db->get();
		return $query->result();
	}

	function getCategoryFeatured()
	{
		$this->db->select("*");
		$this->db->from("category");
		$this->db->where('status', '1');
		$this->db->where('category_featured', '1');
		$query = $this->db->get();
		return $query->result();
	}
	// bank on footer
	function getBankLogo()
	{
		$this->db->select("bank_image");
		$this->db->from("bank_account");
		$this->db->where('bank_status', '1');
		$query = $this->db->get();
		return $query->result();
	}

	function getClientLogo()
	{
		$this->db->select("*");
		$this->db->from("client");
		$this->db->where('status', '1');
		$query = $this->db->get();
		return $query->result();
	}
	// wishlist
	function getWishlist($email)
	{
		$this->db->select("*");
		$this->db->from("wishlist");
		$this->db->join('product', 'product.product_id=wishlist.id_product');
		$this->db->join('accounts', 'accounts.accounts_email=wishlist.user_mail');
		$this->db->where('accounts.accounts_email', $email);
		$this->db->where('product.product_qty > ', '0');
		$query = $this->db->get();
		return $query->result();
	}

	function countWish($email)
	{
		$this->db->select("*");
		$this->db->from("wishlist");
		$this->db->join('product', 'product.product_id=wishlist.id_product');
		$this->db->join('accounts', 'accounts.accounts_email=wishlist.user_mail');
		$this->db->where('accounts.accounts_email', $email);
		$query = $this->db->get();
		return $query->num_rows();
	}

	// add wishlist
	public function saveWish($data)
	{
		$this->db->insert("wishlist", $data);
		return $this->db->insert_id();
	}

	public function delete_by_id($id)
	{
		$this->db->where('wish_id', $id);
		$this->db->delete("wishlist");
	}

	public function bankAccount()
	{
		$this->db->select("*");
		$this->db->from("bank_account");
		$this->db->where('bank_status', '1');
		$query = $this->db->get();
		return $query->result();
	}

	public function getAddress($email)
	{
		$this->db->select("*");
		$this->db->from("accounts");
		$this->db->where('accounts_email', $email);
		$query = $this->db->get();
		return $query->result();
	}

	public function getAddressCheckout($email)
	{

		$this->db->where('accounts_email', $email);
		$query = $this->db->get('accounts');
		return $query;
	}
	public function AddressUpdate($where, $data)
	{
		$this->db->update("accounts", $data, $where);
		return $this->db->affected_rows();
	}

	public function getAbout()
	{
		$this->db->select("*");
		$this->db->from("page");
		$this->db->where('id_page', 1);
		$query = $this->db->get();
		return $query->result();
	}

	public function getDelivery()
	{
		$this->db->select("*");
		$this->db->from("page");
		$this->db->where('id_page', 2);
		$query = $this->db->get();
		return $query->result();
	}

	public function getPrivacy()
	{
		$this->db->select("*");
		$this->db->from("page");
		$this->db->where('id_page', 3);
		$query = $this->db->get();
		return $query->result();
	}

	public function getToc()
	{
		$this->db->select("*");
		$this->db->from("page");
		$this->db->where('id_page', 4);
		$query = $this->db->get();
		return $query->result();
	}

	public function getLink()
	{
		$this->db->select("*");
		$this->db->from("link");
		$this->db->where('status', 1);
		$query = $this->db->get();
		return $query->result();
	}

	public function getSeo()
	{
		$this->db->select("*");
		$this->db->from("meta");
		$this->db->where('id', 1);
		$query = $this->db->get();
		return $query->result();
	}

	public function updateSeo($where, $data)
	{
		$this->db->update("meta", $data, $where);
		return $this->db->affected_rows();
	}

	function get_site_data(){
		$query = $this->db->get('meta', 1);
		return $query;
	}

	function get_inv()
	{
        $q = $this->db->query("SELECT MAX(RIGHT(no_invoice,4)) AS kd_max FROM orders WHERE DATE(tanggal_transaksi)=CURDATE()");
        $kd = "";
        if($q->num_rows()>0){
            foreach($q->result() as $k){
                $tmp = ((int)$k->kd_max)+1;
                $kd = sprintf("%04s", $tmp);
            }
        }else{
            $kd = "0001";
		}
        date_default_timezone_set('Asia/Jakarta');
        return date('dmy').$kd;
	}
	public function saveOrder($data2)
	{
		$this->db->insert('orders', $data2);
		return $this->db->insert_id();
	}

	function getInvoice($invoice)
	{
		$this->db->select("*");
		$this->db->from("orders");
		$this->db->join('bank_account', 'bank_account.bank_id=orders.id_bank');
		$this->db->where('no_invoice', $invoice);
		$query = $this->db->get();
		return $query->result();
	}
	function getDetailInv($invoice)
	{
		$this->db->select("*");
		$this->db->from("detail_pembelian");
		$this->db->where('invoice_no', $invoice);
		$query = $this->db->get();
		return $query->result();
	}

	function getOrder($email)
	{
		$this->db->select("*");
		$this->db->from("orders");
		$this->db->where('user_mail', $email);
		$query = $this->db->get();
		return $query->result();
	}


}
