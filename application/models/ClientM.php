<?php

class ClientM extends CI_Model{
	var $table = 'client';
	var $column_order = array('id','name','status'); 
	var $column_search = array('id','name','status'); 
	var $order = array('id' => 'desc'); 

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function saveImage($data)
	{
		$this->db->insert('client', $data);
		return $this->db->insert_id();
	}

	public function getData()
	{
		$this->db->from($this->table);
		$query = $this->db->get();
		return $query->result();
	}

	public function delete_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}
	    	
}