<?php

class GalleryM extends CI_Model{
	var $table = 'gallery';
	var $column_order = array('gallery_id','gallery_name','product_id','gallery_token'); 
	var $column_search = array('gallery_id','gallery_name','product_id','gallery_token'); 
	var $order = array('gallery_id' => 'desc'); 

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


	public function getDetail($id)
	{
		$this->db->from($this->table);
		$this->db->where('product_id',$id);
		$query = $this->db->get();
		return $query->result();
	}

	public function delete_by_id($id)
	{
		$this->db->where('gallery_id', $id);
		$this->db->delete($this->table);
	}

	public function delete_by_product($id)
	{
		$this->db->where('product_id', $id);
		$this->db->delete($this->table);
	}
	    	
}