
<?php foreach ($productDetail as $p) { ?>
<div class="breadcrumb-area bg-gray-4 breadcrumb-padding-1">
    <div class="container">
        <div class="breadcrumb-content text-center">
            <h3 data-aos="fade-up" data-aos-delay="200"><?php echo $p->product_name; ?></h3>
            <ul data-aos="fade-up" data-aos-delay="400">
                <li><a href="<?= base_url(); ?>">Home</a></li>
                <li><i class="ti-angle-right"></i></li>
                <li><a href="<?= base_url(); ?>product">Products</a></li>
                <li><i class="ti-angle-right"></i></li>
                <li><?php echo $p->product_name; ?></li>
            </ul>
        </div>
    </div>
    <div class="breadcrumb-img-1" data-aos="fade-right" data-aos-delay="200">
        <img src="<?= base_url(); ?>assets/frontend/images/banner/breadcrumb-1.png" alt="">
    </div>
    <div class="breadcrumb-img-2" data-aos="fade-left" data-aos-delay="200">
        <img src="<?= base_url(); ?>assets/frontend/images/banner/breadcrumb-2.png" alt="">
    </div>
</div>
<div class="product-details-area pb-100 pt-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="product-details-img-wrap product-details-vertical-wrap" data-aos="fade-up" data-aos-delay="200">
                    <div class="product-details-small-img-wrap">
                        <div class="swiper-container product-details-small-img-slider-1 pd-small-img-style">
                            <div class="swiper-wrapper">
                                <?php foreach ($productGallery as $pg) { ?>
                                <div class="swiper-slide">
                                    <div class="product-details-small-img">
                                        <img src="<?php echo base_url('assets/images/gallery/' . $pg->gallery_name); ?>">
                                    </div>
                                </div>
                                <?php }?>
                            </div>
                        </div>
                        <div class="pd-prev pd-nav-style"> <i class="ti-angle-up"></i></div>
                        <div class="pd-next pd-nav-style"> <i class="ti-angle-down"></i></div>
                    </div>
                    <div class="swiper-container product-details-big-img-slider-1 pd-big-img-style">
                        <div class="swiper-wrapper">
                            <?php foreach ($productGallery as $pg) { ?>
                                <div class="swiper-slide">
                                    <div class="easyzoom-style">
                                        <div class="easyzoom easyzoom--overlay">
                                            <a href="<?php echo base_url('assets/images/gallery/' . $pg->gallery_name); ?>">
                                            <img src="<?php echo base_url('assets/images/gallery/' . $pg->gallery_name); ?>">    
                                            </a>
                                        </div>
                                        <a class="easyzoom-pop-up img-popup" href="<?php echo base_url('assets/images/gallery/' . $pg->gallery_name); ?>">
                                            <i class="pe-7s-search"></i>
                                        </a>
                                    </div>
                                </div>
                            <?php }?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="product-details-content" data-aos="fade-up" data-aos-delay="400">
                    <h2><?php echo $p->product_name; ?></h2>
                    <div class="product-details-price">
                        <!-- <span class="old-price">$25.89 </span> -->
                        <span class="new-price"><?php echo 'Rp. ' . number_format($p->product_price, 0, '', '.') . ',-'; ?> </span>
                    </div>
                    <!-- <div class="product-details-review">
                        <div class="product-rating">
                            <i class=" ti-star"></i>
                            <i class=" ti-star"></i>
                            <i class=" ti-star"></i>
                            <i class=" ti-star"></i>
                            <i class=" ti-star"></i>
                        </div>
                        <span>( 1 Customer Review )</span>
                    </div> -->
                    <!-- <div class="product-color product-color-active product-details-color">
                        <span>Color :</span>
                        <ul>
                            <li><a title="Pink" class="pink" href="product-details.html#">pink</a></li>
                            <li><a title="Yellow" class="active yellow" href="product-details.html#">yellow</a></li>
                            <li><a title="Purple" class="purple" href="product-details.html#">purple</a></li>
                        </ul>
                    </div> -->
                    <div class="product-details-action-wrap">
                        <div class="product-quality">
                        <input name="quantity" id="<?php echo $p->product_id; ?>" value="1" class="quantity form-control">
                        </div>
                        <div class="single-product-cart btn-hover">
                            <a href="javascript:void(0)" class="add_cart" data-produkid="<?php echo $p->product_id; ?>" data-produknama="<?php echo $p->product_name; ?>" data-produkharga="<?php echo $p->product_price; ?>" data-produkimage="<?php echo $p->product_image; ?>"">Add to cart</a>
                        </div>
                        
                        <div class=" single-product-wishlist">
                                                <?php if ($this->session->userdata('mail')) { ?>
                                                    <a class="add_wish" href="#" data-produkid="<?php echo $p->product_id; ?>" data-usermail="<?php echo $this->session->userdata('mail'); ?>" title="Wishlist"><i class="pe-7s-like"></i></a>

                                                <?php } else { ?>
                                                    <a href="<?= base_url('my-account') ?>" class="pro" title="Wishlist"><i class="pe-7s-like"></i></a>
                                                <?php } ?>
                                                </div>
                        
                    </div>
                    <div class="product-details-meta">
                        <ul>
                            <li><span class="title">Type:</span><a href="<?= base_url($p->slug_type); ?>"> <?php echo $p->name_type; ?></a></li>
                            <li><span class="title">Category:</span>
                                <ul>
                                    <li><a href="<?= base_url($p->slug_type . '/' . $p->slug); ?>"><?php echo $p->name; ?></a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="sharethis-inline-share-buttons"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="description-review-area pb-85">
    <div class="container">
        <div class="description-review-topbar nav" data-aos="fade-up" data-aos-delay="200">
            <a class="active" data-bs-toggle="tab" href="product-details.html#des-details1"> Description </a>
            <!-- <a data-bs-toggle="tab" href="product-details.html#des-details2" class=""> Information </a> -->
        </div>
        <div class="tab-content">
            <div id="des-details1" class="tab-pane active">
                <div class="product-description-content text-center">
                    <p data-aos="fade-up" data-aos-delay="200">
                    <?php echo $p->product_desc; ?>
                    </p>
                    
                </div>
            </div>
            <!-- <div id="des-details2" class="tab-pane">
                <div class="specification-wrap table-responsive">
                    <table>
                        <tbody>
                            <tr>
                                <td class="width1">Brands</td>
                                <td>Airi, Brand, Draven, Skudmart, Yena</td>
                            </tr>
                            <tr>
                                <td class="width1">Color</td>
                                <td>Blue, Gray, Pink</td>
                            </tr>
                            <tr>
                                <td class="width1">Size</td>
                                <td>L, M, S, XL, XXL</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div> -->
            
        </div>
    </div>
</div>
<?php } ?>

<!-- <div class="related-product-area pb-95">
    <div class="container">
        <div class="section-title-2 st-border-center text-center mb-75" data-aos="fade-up" data-aos-delay="200">
            <h2>Related Products</h2>
        </div>
        <div class="related-product-active swiper-container">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="product-wrap" data-aos="fade-up" data-aos-delay="200">
                        <div class="product-img img-zoom mb-25">
                            <a href="product-details.html">
                                <img src="assets/images/product/product-1.png" alt="">
                            </a>
                            <div class="product-badge badge-top badge-right badge-pink">
                                <span>-10%</span>
                            </div>
                            <div class="product-action-wrap">
                                <button class="product-action-btn-1" title="Wishlist"><i class="pe-7s-like"></i></button>
                                <button class="product-action-btn-1" title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                    <i class="pe-7s-look"></i>
                                </button>
                            </div>
                            <div class="product-action-2-wrap">
                                <button class="product-action-btn-2" title="Add To Cart"><i class="pe-7s-cart"></i> Add to cart</button>
                            </div>
                        </div>
                        <div class="product-content">
                            <h3><a href="product-details.html">New Modern Sofa Set</a></h3>
                            <div class="product-price">
                                <span class="old-price">$25.89 </span>
                                <span class="new-price">$20.25 </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="product-wrap" data-aos="fade-up" data-aos-delay="400">
                        <div class="product-img img-zoom mb-25">
                            <a href="product-details.html">
                                <img src="assets/images/product/product-2.png" alt="">
                            </a>
                            <div class="product-action-wrap">
                                <button class="product-action-btn-1" title="Wishlist"><i class="pe-7s-like"></i></button>
                                <button class="product-action-btn-1" title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                    <i class="pe-7s-look"></i>
                                </button>
                            </div>
                            <div class="product-action-2-wrap">
                                <button class="product-action-btn-2" title="Add To Cart"><i class="pe-7s-cart"></i> Add to cart</button>
                            </div>
                        </div>
                        <div class="product-content">
                            <h3><a href="product-details.html">New Modern Sofa Set</a></h3>
                            <div class="product-price">
                                <span>$50.25 </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="product-wrap" data-aos="fade-up" data-aos-delay="600">
                        <div class="product-img img-zoom mb-25">
                            <a href="product-details.html">
                                <img src="assets/images/product/product-3.png" alt="">
                            </a>
                            <div class="product-badge badge-top badge-right badge-pink">
                                <span>-10%</span>
                            </div>
                            <div class="product-action-wrap">
                                <button class="product-action-btn-1" title="Wishlist"><i class="pe-7s-like"></i></button>
                                <button class="product-action-btn-1" title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                    <i class="pe-7s-look"></i>
                                </button>
                            </div>
                            <div class="product-action-2-wrap">
                                <button class="product-action-btn-2" title="Add To Cart"><i class="pe-7s-cart"></i> Add to cart</button>
                            </div>
                        </div>
                        <div class="product-content">
                            <h3><a href="product-details.html">Easy Modern Chair</a></h3>
                            <div class="product-price">
                                <span class="old-price">$45.00 </span>
                                <span class="new-price">$40.00 </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="product-wrap" data-aos="fade-up" data-aos-delay="800">
                        <div class="product-img img-zoom mb-25">
                            <a href="product-details.html">
                                <img src="assets/images/product/product-4.png" alt="">
                            </a>
                            <div class="product-action-wrap">
                                <button class="product-action-btn-1" title="Wishlist"><i class="pe-7s-like"></i></button>
                                <button class="product-action-btn-1" title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                    <i class="pe-7s-look"></i>
                                </button>
                            </div>
                            <div class="product-action-2-wrap">
                                <button class="product-action-btn-2" title="Add To Cart"><i class="pe-7s-cart"></i> Add to cart</button>
                            </div>
                        </div>
                        <div class="product-content">
                            <h3><a href="product-details.html">Stylish Swing Chair</a></h3>
                            <div class="product-price">
                                <span>$30.25 </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="product-wrap">
                        <div class="product-img img-zoom mb-25">
                            <a href="product-details.html">
                                <img src="assets/images/product/product-2.png" alt="">
                            </a>
                            <div class="product-badge badge-top badge-right badge-pink">
                                <span>-10%</span>
                            </div>
                            <div class="product-action-wrap">
                                <button class="product-action-btn-1" title="Wishlist"><i class="pe-7s-like"></i></button>
                                <button class="product-action-btn-1" title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                    <i class="pe-7s-look"></i>
                                </button>
                            </div>
                            <div class="product-action-2-wrap">
                                <button class="product-action-btn-2" title="Add To Cart"><i class="pe-7s-cart"></i> Add to cart</button>
                            </div>
                        </div>
                        <div class="product-content">
                            <h3><a href="product-details.html">New Modern Sofa Set</a></h3>
                            <div class="product-price">
                                <span class="old-price">$80.50 </span>
                                <span class="new-price">$75.25 </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->