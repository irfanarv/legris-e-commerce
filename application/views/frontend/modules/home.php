<div class="slider-area">
    <div class="slider-active swiper-container">
        <div class="swiper-wrapper">
            <?php foreach ($sliders as $sl) { ?>
                <div class="swiper-slide">
                    <div class="intro-section slider-height-1 slider-content-center bg-img single-animation-wrap slider-bg-color-3" style="background-image:url('<?php echo base_url('assets/images/sliders/' . $sl->slider_img) ?>')">
                        <div class="container">
                            <div class="row">
                                <div class="col-12 hm2-slider-animation">
                                    <div class="slider-content-2 slider-animated-3">
                                        <h3 class="animated"><?php echo $sl->slider_subtitle; ?></h3>
                                        <h1 class="animated"><?php echo $sl->slider_title; ?></h1>
                                        <?php if (!empty($sl->slider_uri)) {
                                            echo '
                                            <div class="slider-btn-2 btn-hover">
                                                <a href=" ' . $sl->slider_uri . '" class="btn hover-border-radius theme-color animated">
                                                    ' . $sl->slider_btn . '
                                                </a>
                                            </div>
                                            ';
                                        } else {
                                            echo " ";
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="home-slider-prev2 main-slider-nav2"><i class="fa fa-angle-left"></i></div>
            <div class="home-slider-next2 main-slider-nav2"><i class="fa fa-angle-right"></i></div>
        </div>
    </div>
</div>
<!-- <div class="service-area pb-20 mt-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-12 mb-30">
                <div class="service-wrap" data-aos="fade-up" data-aos-delay="200">
                    <div class="service-img">
                        <img src="<?= base_url(); ?>assets/frontend/images/icon-img/car.png" alt="">
                    </div>
                    <div class="service-content">
                        <h3>Free Shipping</h3>
                        <p>Free shipping on all order</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12 mb-30">
                <div class="service-wrap" data-aos="fade-up" data-aos-delay="400">
                    <div class="service-img">
                        <img src="<?= base_url(); ?>assets/frontend/images/icon-img/time.png" alt="">
                    </div>
                    <div class="service-content">
                        <h3>Support 24/7</h3>
                        <p>Support 24 hours a day</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12 mb-30">
                <div class="service-wrap" data-aos="fade-up" data-aos-delay="600">
                    <div class="service-img">
                        <img src="<?= base_url(); ?>assets/frontend/images/icon-img/dollar.png" alt="">
                    </div>
                    <div class="service-content">
                        <h3>Money Return</h3>
                        <p>Back Guarantee Under </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12 mb-30">
                <div class="service-wrap" data-aos="fade-up" data-aos-delay="800">
                    <div class="service-img">
                        <img src="<?= base_url(); ?>assets/frontend/images/icon-img/discount.png" alt="">
                    </div>
                    <div class="service-content">
                        <h3>Order Discount</h3>
                        <p>Onevery order over $150</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->

<div class="product-area padding-10-row-col pb-20 mt-5">
    <div class="container">
        <div class="row grid">
            <div class="grid-sizer"></div>
            <?php foreach ($featcat as $ft) { ?>
            <!-- featured category home -->
            <div class="col-lg-6 col-md-6 col-sm-6 col-12 grid-item">
                <div class="product-wrap product-overly mb-10" data-aos="fade-up" data-aos-delay="200">
                    <div class="product-img img-zoom">
                        <a href="<?= base_url('categories/') . $ft->slug; ?>">
                            <img src="<?php echo base_url('assets/images/category/' . $ft->image) ?>" alt="">
                        </a>

                        <div class="product-content product-content-position">
                            <h3><?= $ft->name ?></h3>
                            <div class="product-price">
                                <span class="new-price"><a href="<?= base_url('categories/') . $ft->slug; ?>"> View Product</a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
            <!-- featured category home -->
            <!-- category home -->
            <?php foreach ($categoryHome as $ch) { ?>
                <div class="col-lg-3 col-md-6 col-sm-6 col-12 grid-item">
                    <div class="product-wrap product-overly mb-10" data-aos="fade-up" data-aos-delay="300">
                        <div class="product-img img-zoom">
                            <a href="<?= base_url('categories/') . $ch->slug; ?>">
                                <img class="img-fluid" src="<?php echo base_url('assets/images/category/' . $ch->image) ?>" alt="<?= $ch->name ?>">
                            </a>

                            <div class="product-content product-content-position">
                                <h3><?= $ch->name ?></h3>
                                <div class="product-price">
                                    <span class="new-price"><a href="<?= base_url('categories/') . $ch->slug; ?>"> View Product</a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <!-- category home -->
        </div>
    </div>
</div>
<div class="product-area pb-60 mb-4">
    <div class="container">
        <div class="section-title-2 border-none text-center mb-5" data-aos="fade-up" data-aos-delay="200">
            <h2>Featured Products</h2>
        </div>
        <div class="row">
            <?php foreach ($hotProduct as $hp) { ?>
                <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                    <div class="product-wrap mb-35">

                        <div class="product-img img-zoom mb-25">
                            <a href="<?= base_url('product/' . $hp->product_slug); ?>">
                                <img src="<?php echo base_url('assets/images/product/' . $hp->product_image) ?>" alt="<?php echo $hp->product_name; ?>">
                            </a>
                            <!-- <div class="product-badge badge-top badge-right badge-pink">
                                    <span>-10%</span>
                                </div> -->
                            <div class="product-action-wrap">
                                <?php if ($this->session->userdata('mail')) { ?>
                                    <button class="add_wish product-action-btn-1" data-produkid="<?php echo $hp->product_id; ?>" data-usermail="<?php echo $this->session->userdata('mail');?>"  title="Wishlist"><i class="pe-7s-like"></i></button>
                                
                                <?php }else{ ?>
                                    <a href="<?= base_url('my-account') ?>" class="product-action-btn-1"   title="Wishlist"><i class="pe-7s-like"></i></a>
                                <?php }?>
                                
                                <button class="product-action-btn-1" title="Quick View" data-bs-toggle="modal" data-bs-target="#<?php echo $hp->product_slug; ?>">
                                    <i class="pe-7s-look"></i>
                                </button>
                            </div>
                            <div class="product-action-2-wrap">
                                <button class="add_cart product-action-btn-2" title="Add To Cart" data-produkid="<?php echo $hp->product_id; ?>" data-produknama="<?php echo $hp->product_name; ?>" data-produkharga="<?php echo $hp->product_price; ?>" data-produkimage="<?php echo $hp->product_image; ?>"><i class="pe-7s-cart"></i> Add to cart</button>
                            </div>
                        </div>
                        <div class="product-content">
                            <h3><a href="href=" <?= base_url('product/' . $hp->product_slug); ?>""><?php echo $hp->product_name; ?></a></h3>
                            <div class="product-price">
                                <!-- <span class="old-price">$25.89 </span> -->
                                <span class="new-price"><?php echo 'Rp. ' . number_format($hp->product_price, 0, '', '.') . ',-'; ?> </span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- modal -->
                <div class="modal fade quickview-modal-style" id="<?php echo $hp->product_slug; ?>" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <a href="javascript:void(0)" class="close" data-bs-dismiss="modal" aria-label="Close"><i class=" ti-close "></i></a>
                            </div>
                            <div class="modal-body">
                                <div class="row gx-0">
                                    <div class="col-lg-5 col-md-5 col-12">
                                        <div class="modal-img-wrap">
                                            <img src="<?php echo base_url('assets/images/product/' . $hp->product_image) ?>" alt="<?php echo $hp->product_name; ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-7 col-md-7 col-12">
                                        <div class="product-details-content quickview-content">
                                            <h2><?php echo $hp->product_name; ?></h2>
                                            <div class="product-details-price">
                                                <!-- <span class="old-price">$25.89 </span> -->
                                                <span class="new-price"><?php echo 'Rp. ' . number_format($hp->product_price, 0, '', '.') . ',-'; ?> </span>
                                            </div>
                                            <!-- <div class="product-details-review">
                                                    <div class="product-rating">
                                                        <i class=" ti-star"></i>
                                                        <i class=" ti-star"></i>
                                                        <i class=" ti-star"></i>
                                                        <i class=" ti-star"></i>
                                                        <i class=" ti-star"></i>
                                                    </div>
                                                    <span>( 1 Customer Review )</span>
                                                </div> -->
                                            <!-- <div class="product-color product-color-active product-details-color">
                                                    <span>Color :</span>
                                                    <ul>
                                                        <li><a title="Pink" class="pink" href="javascript:void(0)">pink</a></li>
                                                        <li><a title="Yellow" class="active yellow" href="javascript:void(0)">yellow</a></li>
                                                        <li><a title="Purple" class="purple" href="javascript:void(0)">purple</a></li>
                                                    </ul>
                                                </div> -->
                                            <?php if (!empty($hp->product_desc)) { ?>
                                                <p><?php $desc = substr($hp->product_desc, 0, 350);
                                                    echo $desc; ?> <a href="<?php echo base_url('product/' . $hp->product_slug) ?>"> ... Read more</a></p>
                                            <?php } ?>

                                            <div class="product-details-action-wrap">
                                                <div class="product-quality">
                                                    <input name="quantity" id="<?php echo $hp->product_id; ?>" value="1" class="quantity form-control">

                                                </div>
                                                <div class="single-product-cart btn-hover">
                                                    <a href="javascript:void(0)" class="add_cart" data-produkid="<?php echo $hp->product_id; ?>" data-produknama="<?php echo $hp->product_name; ?>" data-produkharga="<?php echo $hp->product_price; ?>" data-produkimage="<?php echo $hp->product_image; ?>"">Add to cart</a>
                                                </div>
                                                <div class=" single-product-wishlist">
                                                <?php if ($this->session->userdata('mail')) { ?>
                                                    <a class="add_wish" href="#" data-produkid="<?php echo $hp->product_id; ?>" data-usermail="<?php echo $this->session->userdata('mail'); ?>" title="Wishlist"><i class="pe-7s-like"></i></a>

                                                <?php } else { ?>
                                                    <a href="<?= base_url('my-account') ?>" class="pro" title="Wishlist"><i class="pe-7s-like"></i></a>
                                                <?php } ?>
                                                </div>
                                                
                                            </div>
                                            <div class="product-details-meta">
                                                <ul>
                                                    <li><span class="title">Type:</span><a href="<?= base_url($hp->slug_type); ?>"> <?php echo $hp->name_type; ?></a></li>
                                                    <li><span class="title">Category:</span>
                                                        <ul>
                                                            <li><a href="<?= base_url($hp->slug_type . '/' . $hp->slug); ?>"><?php echo $hp->name; ?></a></li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- endmodal -->
            <?php } ?>
        </div>
    </div>
</div> 
<div class="brand-logo-area pb-95">
    <div class="container">
        <div class="section-title-2 border-none text-center mb-5" data-aos="fade-up" data-aos-delay="200">
            <h2>Our Partner</h2>
        </div>
        <div class="brand-logo-active swiper-container">
            <div class="swiper-wrapper">
            <?php foreach ($clientLogo as $cl) { ?>
                <div class="swiper-slide">
                    <div class="single-brand-logo" data-aos="fade-up" data-aos-delay="200">
                        <a href="#"><img src="<?php echo base_url('assets/images/client/' . $cl->image) ?>"></a>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<div class="blog-area pt-10 pb-100">
    <div class="container">
        <div class="section-title-2 border-none text-center mb-5" data-aos="fade-up" data-aos-delay="200">
            <h2>Recent Blog</h2>
        </div>
        <div class="row">
        <?php foreach ($news as $p) : ?>
            <div class="col-lg-4 col-md-6">
                <div class="blog-wrap mb-50" data-aos="fade-up" data-aos-delay="200">
                    <div class="blog-img-date-wrap mb-25">
                        <div class="blog-img">
                            <a href="<?= base_url('blog/detail/'); ?><?php echo $p->post_slug; ?>"><img src="<?php echo base_url("assets/images/news/"); ?><?php echo $p->post_img; ?>" alt=""></a>
                        </div>
                        <div class="blog-date">
                            <h5><?= date('d', strtotime($p->post_created_at))."";?> <span><?= date('M', strtotime($p->post_created_at))."";?></span></h5>
                        </div>
                    </div>
                    <div class="blog-content">
                        <h3><a href="<?= base_url('blog/detail'); ?>"><?php echo $p->post_title; ?></a></h3>
                        <p><?php echo  substr($p->post_desc,0, 300); ?>...</p>
                        <div class="blog-btn-2 btn-hover">
                            <a class="btn hover-border-radius theme-color" href="<?= base_url('blog/detail/'); ?><?php echo $p->post_slug; ?>">Read More</a>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>