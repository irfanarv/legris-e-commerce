<div class="breadcrumb-area bg-gray-4 breadcrumb-padding-1">
    <div class="container">
        <div class="breadcrumb-content text-center">
            <h2 data-aos="fade-up" data-aos-delay="200">Contact Us</h2>
            <ul data-aos="fade-up" data-aos-delay="400">
                <li><a href="<?= base_url(); ?>">Home</a></li>
                <li><i class="ti-angle-right"></i></li>
                <li>Contact Us</li>
            </ul>
        </div>
    </div>
    <div class="breadcrumb-img-1">
        <img src="<?= base_url(); ?>assets/frontend/images/banner/breadcrumb-1.png" alt="">
    </div>
    <div class="breadcrumb-img-2">
        <img src="<?= base_url(); ?>assets/frontend/images/banner/breadcrumb-2.png" alt="">
    </div>
</div>


<div class="contact-form-area pt-90 pb-100">
    <div class="container">
        <div class="section-title-4 text-center mb-55" data-aos="fade-up" data-aos-delay="200">
            <h2>Ask Us Anything Here</h2>
        </div>
        <div class="contact-form-wrap" data-aos="fade-up" data-aos-delay="200">
            <form class="contact-form-style" id="form_message" action="#" method="post">
                <div class="row">
                    <div class="col-lg-4">
                        <input name="name" id="name" type="text" placeholder="Name*" required>
                        <input name="email" id="email" type="email" placeholder="Email*" required>
                        <input type="number" id="telp" name="telp" id="tel" placeholder="Phone*" required>
                        <input name="subject" id="subject" type="text" placeholder="Subject*" required>
                    </div>
                    <div class="col-lg-8">
                        <textarea name="message" id="message" placeholder="Message"></textarea>
                    </div>
                    <div class="col-lg-12 col-md-12 col-12 contact-us-btn btn-hover">
                        <button class="submit" type="submit">Send Message</button>
                    </div>
                </div>
            </form>
            <p class="form-messege"></p>
        </div>
    </div>
</div>

<div class="map pt-120" data-aos="fade-up" data-aos-delay="200">
    <iframe src="<?= $maps;?>" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
</div>

<div class="modal fade" id="modal_send" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <h2 class="text-center mt-3">Successfully sent message !</h2>
                <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
                <lottie-player src="https://assets1.lottiefiles.com/private_files/lf30_GAiHca.json" background="transparent" speed="1" hover loop autoplay></lottie-player>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_process" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <h2 class="text-center mt-3">Please Wait !</h2>
                <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
                <lottie-player src="https://assets5.lottiefiles.com/private_files/lf30_weidduaf.json"  background="transparent"  speed="1" hover loop  autoplay></lottie-player>
            </div>
        </div>
    </div>
</div>
<script src="<?= base_url(); ?>assets/frontend/js/vendor/jquery-3.6.0.min.js"></script>
<script type="text/javascript">
    $('#form_message').submit(function(e) {
        var url;
        url = "<?php echo site_url('contactus/send') ?>";
        e.preventDefault();
        $.ajax({
            "url": url,
            type: "post",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            beforeSend: function() {
                $('#modal_process').modal('show');
                setTimeout(function() {
                    $('#modal_process').modal('hide');
                }, 1800);
            },
            success: function(data) {
                $('#modal_process').modal('hide');
                $('#form_message')[0].reset();
                $('#modal_send').modal('show');
                setTimeout(function() {
                    $('#modal_send').modal('hide');
                }, 1800);
            }

        });
    });
</script>