<div class="breadcrumb-area bg-gray-4 breadcrumb-padding-1">
    <div class="container">
        <div class="breadcrumb-content text-center">
            <h2 data-aos="fade-up" data-aos-delay="200">Blog</h2>
            <ul data-aos="fade-up" data-aos-delay="400">
                <li><a href="index.html">Home</a></li>
                <li><i class="ti-angle-right"></i></li>
                <li>Blog </li>
            </ul>
        </div>
    </div>
    <div class="breadcrumb-img-1" data-aos="fade-right" data-aos-delay="200">
        <img src="<?= base_url(); ?>assets/frontend/images/banner/breadcrumb-1.png" alt="">
    </div>
    <div class="breadcrumb-img-2" data-aos="fade-left" data-aos-delay="200">
        <img src="<?= base_url(); ?>assets/frontend/images/banner/breadcrumb-2.png" alt="">
    </div>
</div>
<div class="blog-area pt-100 pb-100">
    <div class="container">
        <div class="row">
        <?php foreach ($news as $p) : ?>
            <div class="col-lg-4 col-md-6">
                <div class="blog-wrap mb-50" data-aos="fade-up" data-aos-delay="200">
                    <div class="blog-img-date-wrap mb-25">
                        <div class="blog-img">
                            <a href="<?= base_url('blog/detail/'); ?><?php echo $p->post_slug; ?>"><img src="<?php echo base_url("assets/images/news/"); ?><?php echo $p->post_img; ?>" alt=""></a>
                        </div>
                        <div class="blog-date">
                            <h5><?= date('d', strtotime($p->post_created_at))."";?> <span><?= date('M', strtotime($p->post_created_at))."";?></span></h5>
                        </div>
                    </div>
                    <div class="blog-content">
                        <h3><a href="<?= base_url('blog/detail'); ?>"><?php echo $p->post_title; ?></a></h3>
                        <p><?php echo  substr($p->post_desc,0, 300); ?>...</p>
                        <div class="blog-btn-2 btn-hover">
                            <a class="btn hover-border-radius theme-color" href="<?= base_url('blog/detail/'); ?><?php echo $p->post_slug; ?>">Read More</a>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>