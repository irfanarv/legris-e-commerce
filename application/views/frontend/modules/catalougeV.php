<div class="breadcrumb-area bg-gray-4 breadcrumb-padding-1">
    <div class="container">
        <div class="breadcrumb-content text-center">
            <h2 data-aos="fade-up" data-aos-delay="200">Wholesale</h2>
            <ul data-aos="fade-up" data-aos-delay="400">
                <li><a href="<?= base_url(); ?>">Home</a></li>
                <li><i class="ti-angle-right"></i></li>
                <li>Wholesale</li>
            </ul>
        </div>
    </div>
    <div class="breadcrumb-img-1">
        <img src="<?= base_url(); ?>assets/frontend/images/banner/breadcrumb-1.png" alt="">
    </div>
    <div class="breadcrumb-img-2">
        <img src="<?= base_url(); ?>assets/frontend/images/banner/breadcrumb-2.png" alt="">
    </div>
</div>


<div class="product-area pb-60 mt-5">
    <div class="container">
        <div class="section-title-tab-wrap mb-75">
            <div class="section-title-2" data-aos="fade-up" data-aos-delay="200">
                <h2>Wholesale</h2>
            </div>
            <div class="tab-style-1 nav" data-aos="fade-up" data-aos-delay="400">
                <a class="active" href="#pro-1" data-bs-toggle="tab">Furniture 1 </a>
                <a href="#pro-2" data-bs-toggle="tab" class=""> Furniture 2 </a>
                <a href="#pro-3" data-bs-toggle="tab" class=""> Home Decore </a>
            </div>
        </div>
        <div class="tab-content jump">
            <div id="pro-1" class="tab-pane active">
                <div class="row">
                    <div class="col-md-12">
                    <iframe src="<?= base_url()?>/assets/catalouge/furniture-1.pdf#toolbar=0&navpanes=1&scrollbar=0" width="100%" height="800px"></iframe>
                    </div>
                </div>
            </div>
            <div id="pro-2" class="tab-pane">
                <div class="row">
                    <div class="col-md-12">
                    <iframe src="<?= base_url()?>/assets/catalouge/furniture-2.pdf#toolbar=0&navpanes=1&scrollbar=0" width="100%" height="800px"></iframe>
                    </div>
                </div>
            </div>
            <div id="pro-3" class="tab-pane">
                <div class="row">
                    <div class="col-md-12">
                    <iframe src="<?= base_url()?>/assets/catalouge/home-decore.pdf#toolbar=0&navpanes=1&scrollbar=0" width="100%" height="800px"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>