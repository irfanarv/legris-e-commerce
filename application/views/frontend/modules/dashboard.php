<?php foreach ($alamat as $ad) { ?>
<div class="breadcrumb-area bg-gray-4 breadcrumb-padding-1">
    <div class="container">
        <div class="breadcrumb-content text-center">
            <h2>My Account </h2>
            <ul>
                <li><a href="index.html">Home</a></li>
                <li><i class="ti-angle-right"></i></li>
                <li>My Account </li>
            </ul>
        </div>
    </div>
    <div class="breadcrumb-img-1" data-aos="fade-right" data-aos-delay="200">
        <img src="<?= base_url(); ?>assets/frontend/images/banner/breadcrumb-1.png" alt="">
    </div>
    <div class="breadcrumb-img-2" data-aos="fade-left" data-aos-delay="200">
        <img src="<?= base_url(); ?>assets/frontend/images/banner/breadcrumb-2.png" alt="">
    </div>
</div>
<!-- my account wrapper start -->
<div class="my-account-wrapper pb-100 pt-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!-- My Account Page Start -->
                <div class="myaccount-page-wrapper">
                    <!-- My Account Tab Menu Start -->
                    <div class="row">
                        <div class="col-lg-3 col-md-4">
                            <div class="myaccount-tab-menu nav" role="tablist">
                                <a href="#dashboad" class="active" data-bs-toggle="tab">Dashboard</a>
                                <a href="#orders" data-bs-toggle="tab">Orders</a>
                                <a href="#account-address" data-bs-toggle="tab">Address</a>
                                <!-- <a href="#account-info" data-bs-toggle="tab">Account Details</a> -->
                                <a href="<?= base_url('logout') ?>">Logout</a>
                            </div>
                        </div>
                        <!-- My Account Tab Menu End -->
                        <!-- My Account Tab Content Start -->
                        <div class="col-lg-9 col-md-8">
                            <div class="tab-content" id="myaccountContent">
                                <!-- Single Tab Content Start -->
                                <div class="tab-pane fade show active" id="dashboad" role="tabpanel">
                                    <div class="myaccount-content">
                                        <h3>Dashboard</h3>
                                        <div class="welcome">
                                            <p>Hello, <strong><?php echo $this->session->userdata('name'); ?> </strong></p>
                                        </div>

                                        <p class="mb-0">From your account dashboard. you can easily check & view your recent orders, manage your billing addresses and edit your password and account details.</p>
                                    </div>
                                </div>
                                <!-- Single Tab Content End -->
                                <!-- Single Tab Content Start -->
                                <div class="tab-pane fade" id="orders" role="tabpanel">
                                    <div class="myaccount-content">
                                        <h3>Orders</h3>
                                        <div class="myaccount-table table-responsive text-center">
                                            <table class="table table-bordered">
                                                <thead class="thead-light">
                                                    <tr>
                                                        <th>No Invoice</th>
                                                        <th>Order Date</th>
                                                        <th>Status</th>
                                                        <th>Total</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php foreach ($order as $od) { ?>
                                                    <tr>
                                                        <td>#<?= $od->no_invoice;?></td>
                                                        <td><?= date('d F Y', strtotime($od->tanggal_transaksi))."";?></td>
                                                        
                                                        <?php if ($od->status_order == "Menunggu"){?>
                                                        <td> <span>UNPAID</span> </td>
                                                        <?php } elseif($od->status_order == "Selesai") { ?>
                                                        <td> <span>PAID</span> </td>
                                                        <?php } elseif($od->status_order == "Di Tolak") { ?>
                                                        <td> <span>REJECT</span> </td>
                                                        <?php } ?>
                                                        <td><?= 'Rp. ' . number_format( $od->total, 0 , '' , '.' ) . ',-'; ; ?></td>
                                                        <td><a href="<?= base_url('invoice/'.$od->no_invoice.'')?>" target="_blank" class="check-btn sqr-btn ">View Invoice</a></td>
                                                    </tr>
                                                    <?php }?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="account-info" role="tabpanel">
                                    <div class="myaccount-content">
                                        <h3>Account Details</h3>
                                        <div class="account-details-form">
                                            <form action="#">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <div class="single-input-item">
                                                            <label for="display-name" class="required">Full Name</label>
                                                            <input type="text" name="full_name" id="full_name" value="<?= $ad->ai_first_name; ?>" />
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="single-input-item">
                                                            <label for="email" class="required">Email Addres </label>
                                                            <input type="email" name="email" value="<?= $this->session->userdata('mail'); ?>" id="email" readonly />
                                                            <span>Can't change email address</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="single-input-item">
                                                            <label for="new-pwd" class="required">New Password</label>
                                                            <input type="password" id="new-pwd" name="password" />
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="single-input-item">
                                                            <label for="confirm-pwd" class="required">Confirm Password</label>
                                                            <input type="password" id="confirm-pwd" name="confirm_password" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="single-input-item btn-hover">
                                                    <button class="check-btn sqr-btn">Save Changes</button>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="account-address" role="tabpanel">
                                    <div class="myaccount-content">
                                        <div class="account-details-form">
                                            <form action="<?php echo base_url() . 'Auth/updateAddress' ?>" method="post" id="myForm" enctype="multipart/form-data">
                                                <h3>Address</h3>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="single-input-item">
                                                            <label for="display-name" class="required">Address <abbr class="required" title="required">*</abbr></label>
                                                            <textarea placeholder="House number and street name" name="address" id="address" required><?= $ad->ai_address; ?></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="single-input-item">
                                                            <label for="email" class="required">Phone <abbr class="required" title="required">*</abbr></label>
                                                            <input type="number" name="phone" placeholder="Phone Number" id="phone" value="<?= $ad->ai_phone; ?>" />
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="single-input-item">
                                                            <label>City <abbr class="required" title="required">*</abbr></label>
                                                            <input type="text" name="city" id="city" placeholder="City" value="<?= $ad->ai_city; ?>" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="single-input-item">
                                                            <label>State <abbr class="required" title="required">*</abbr></label>
                                                            <input type="text" name="state" id="state" placeholder="State" value="<?= $ad->ai_state; ?>" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="single-input-item">
                                                            <label>Postcode / ZIP <abbr class="required" title="required">*</abbr></label>
                                                            <input type="text" name="zip" id="zip" placeholder="Postcode / ZIP" value="<?= $ad->ai_zip; ?>" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="single-input-item btn-hover">
                                                    <button class="check-btn sqr-btn">Save Changes</button>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div> <!-- My Account Tab Content End -->
                    </div>
                </div> <!-- My Account Page End -->
            </div>
        </div>
    </div>
</div>
<?php };?>
<div class="modal fade" id="modaladdress" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <h2 class="text-center mt-3"> Update Success</h2>
                <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
                <lottie-player src="https://assets5.lottiefiles.com/packages/lf20_tAtUrg.json"  background="transparent"  speed="1" hover loop  autoplay></lottie-player>
            </div>
        </div>
    </div>
</div>

<?php if ($this->session->flashdata('msg') == 'sukses') : ?>
    <script>
        $(window).on('load', function() {
            $('#modaladdress').modal('show');
        });
    </script>
<?php else : ?>
<?php endif; ?>