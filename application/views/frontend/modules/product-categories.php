<?php foreach ($fortitle as $ft) { ?>
<div class="breadcrumb-area bg-gray-4 breadcrumb-padding-1">
    <div class="container">
        <div class="breadcrumb-content text-center">
            <h2 data-aos="fade-up" data-aos-delay="200">Products</h2>
            <h3 data-aos="fade-up" data-aos-delay="300"><?php echo $ft->name; ?></h3>
            <ul data-aos="fade-up" data-aos-delay="400">
                <li><a href="<?= base_url(); ?>">Home</a></li>
                <li><i class="ti-angle-right"></i></li>
                <li><a href="<?= base_url('product'); ?>">Product</a></li>
                <li><i class="ti-angle-right"></i></li>
                <li><?php echo $ft->name; ?></li>
            </ul>
        </div>
    </div>
    <div class="breadcrumb-img-1" data-aos="fade-right" data-aos-delay="200">
        <img src="<?= base_url(); ?>assets/frontend/images/banner/breadcrumb-1.png" alt="">
    </div>
    <div class="breadcrumb-img-2" data-aos="fade-left" data-aos-delay="200">
        <img src="<?= base_url(); ?>assets/frontend/images/banner/breadcrumb-2.png" alt="">
    </div>
</div>
<div class="shop-area shop-page-responsive pt-100 pb-100">
    <div class="container">
        <div class="row flex-row-reverse">
            <div class="col-lg-12">
                <div class="shop-topbar-wrapper mb-40">
                    <div class="shop-topbar-left" data-aos="fade-up" data-aos-delay="200">
                        <div class="showing-item">
                            <!-- <span>Showing 1–1 of 1 results</span><br> -->
                            <span>Filter By Categories <?php echo $ft->name; ?></span>
                        </div>
                    </div>
                    <div class="shop-topbar-right" data-aos="fade-up" data-aos-delay="400">

                        <div class="shop-view-mode nav">
                            <a class="active" href="#grid-view" data-bs-toggle="tab"><i class=" ti-layout-grid3 "></i> </a>
                            <a href="#list-view" data-bs-toggle="tab" class=""><i class=" ti-view-list-alt "></i></a>
                        </div>
                    </div>
                </div>
                <div class="shop-bottom-area">
                    <div class="tab-content jump">
                        <div id="grid-view" class="tab-pane active">
                            <div class="row">
                                <?php foreach ($productCategory as $p) { ?>
                                    <!-- modal -->
                                    <div class="modal fade quickview-modal-style" id="<?php echo $p->product_slug; ?>" tabindex="-1" role="dialog">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <a href="javascript:void(0)" class="close" data-bs-dismiss="modal" aria-label="Close"><i class=" ti-close "></i></a>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row gx-0">
                                                        <div class="col-lg-5 col-md-5 col-12">
                                                            <div class="modal-img-wrap">
                                                                <img src="<?php echo base_url('assets/images/product/' . $p->product_image) ?>" alt="<?php echo $p->product_name; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-7 col-md-7 col-12">
                                                            <div class="product-details-content quickview-content">
                                                                <h2><?php echo $p->product_name; ?></h2>
                                                                <div class="product-details-price">
                                                                    <!-- <span class="old-price">$25.89 </span> -->
                                                                    <span class="new-price"><?php echo 'Rp. ' . number_format($p->product_price, 0, '', '.') . ',-'; ?> </span>
                                                                </div>
                                                                <?php if (!empty($p->product_desc)) { ?>
                                                                    <p><?php $desc = substr($p->product_desc, 0, 350);
                                                                        echo $desc; ?> <a href="<?php echo base_url('product/' . $p->product_slug) ?>"> ... Read more</a></p>
                                                                <?php } ?>

                                                                <div class="product-details-action-wrap">
                                                                    <div class="product-quality">
                                                                        <input name="quantity" id="<?php echo $p->product_id; ?>" value="1" class="quantity form-control">

                                                                    </div>
                                                                    <div class="single-product-cart btn-hover">
                                                                        <a href="javascript:void(0)" class="add_cart" data-produkid="<?php echo $p->product_id; ?>" data-produknama="<?php echo $p->product_name; ?>" data-produkharga="<?php echo $p->product_price; ?>" data-produkimage="<?php echo $p->product_image; ?>"">Add to cart</a>
                                                                </div>
                                                                <div class=" single-product-wishlist">
                                                                            <a title="Wishlist" href="javascript:void(0)"><i class="pe-7s-like"></i></a>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- endmodal -->
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                                        <div class="product-wrap mb-35" data-aos="fade-up" data-aos-delay="200">
                                            <div class="product-img img-zoom mb-25">
                                                <a href="<?php echo base_url('product/' . $p->product_slug) ?>">
                                                    <img src="<?php echo base_url('assets/images/product/' . $p->product_image) ?>" alt="<?php echo $p->product_name; ?>">
                                                </a>
                                                <!-- <div class="product-badge badge-top badge-right badge-pink">
                                                    <span>-10%</span>
                                                </div> -->
                                                <div class="product-action-wrap">
                                                    <button class="product-action-btn-1" title="Wishlist"><i class="pe-7s-like"></i></button>
                                                    <button class="product-action-btn-1" title="Quick View" data-bs-toggle="modal" data-bs-target="#<?php echo $p->product_slug; ?>">
                                                        <i class="pe-7s-look"></i>
                                                    </button>
                                                </div>
                                                <div class="product-action-2-wrap">
                                                    <button class="add_cart product-action-btn-2" title="Add To Cart" data-produkid="<?php echo $p->product_id; ?>" data-produknama="<?php echo $p->product_name; ?>" data-produkharga="<?php echo $p->product_price; ?>" data-produkimage="<?php echo $p->product_image; ?>"><i class="pe-7s-cart"></i> Add to cart</button>
                                                </div>
                                            </div>
                                            <div class="product-content">
                                                <h3><a href="<?= base_url(); ?>"><?php echo $p->product_name; ?></a></h3>
                                                <div class="product-price">
                                                    <!-- <span class="old-price">$25.89 </span> -->
                                                    <span class="new-price"><?php echo 'Rp. ' . number_format($p->product_price, 0, '', '.') . ',-'; ?> </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                <?php } ?>
                            </div>
                            <!-- <div class="pagination-style-1" data-aos="fade-up" data-aos-delay="200">
                                <ul>
                                    <li><a class="active" href="shop-sidebar.html#">1</a></li>
                                    <li><a href="shop-sidebar.html#">2</a></li>
                                    <li><a href="shop-sidebar.html#">3</a></li>
                                    <li><a class="next" href="shop-sidebar.html#"><i class=" ti-angle-double-right "></i></a></li>
                                </ul>
                            </div> -->
                        </div>
                        <div id="list-view" class="tab-pane ">
                            <?php foreach ($productCategory as $p) { ?>
                                <div class="shop-list-wrap mb-30">
                                    <div class="row">
                                        <div class="col-lg-4 col-sm-5">
                                            <div class="product-list-img">
                                                <a href="<?php echo base_url('product/' . $p->product_slug) ?>">
                                                    <img src="<?php echo base_url('assets/images/product/' . $p->product_image) ?>" alt="<?php echo $p->product_name; ?>">
                                                </a>
                                                <!-- <div class="product-list-badge badge-right badge-pink">
                                                    <span>-20%</span>
                                                </div> -->
                                                <div class="product-list-quickview">
                                                    <button class="product-action-btn-2" title="Quick View" data-bs-toggle="modal" data-bs-target="#<?php echo $p->product_slug; ?>">
                                                        <i class="pe-7s-look"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-8 col-sm-7">
                                            <div class="shop-list-content">
                                                <h3><a href="<?php echo base_url('product/' . $p->product_slug) ?>"><?php echo $p->product_name; ?></a></h3>
                                                <div class="product-price">
                                                    <!-- <span class="old-price">$70.89 </span> -->
                                                    <span class="new-price"><?php echo 'Rp. ' . number_format($p->product_price, 0, '', '.') . ',-'; ?> </span>
                                                </div>
                                                <!-- <div class="product-list-rating">
                                                    <i class=" ti-star"></i>
                                                    <i class=" ti-star"></i>
                                                    <i class=" ti-star"></i>
                                                    <i class=" ti-star"></i>
                                                    <i class=" ti-star"></i>
                                                </div> -->
                                                <?php if (!empty($p->product_desc)) { ?>
                                                    <p class=""><?php $desc = substr($p->product_desc, 0, 120);
                                                                echo $desc; ?> <a href="#"> ... Read more</a></p>
                                                <?php } ?>
                                                <div class="product-list-action mt-4">
                                                    <button class="add_cart product-action-btn-3" title="Add to cart" data-produkid="<?php echo $p->product_id; ?>" data-produknama="<?php echo $p->product_name; ?>" data-produkharga="<?php echo $p->product_price; ?>" data-produkimage="<?php echo $p->product_image; ?>"><i class="pe-7s-cart"></i></button>
                                                    <button class="product-action-btn-3" title="Wishlist"><i class="pe-7s-like"></i></button>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- modal -->
                                <div class="modal fade quickview-modal-style" id="<?php echo $p->product_slug; ?>" tabindex="-1" role="dialog">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <a href="javascript:void(0)" class="close" data-bs-dismiss="modal" aria-label="Close"><i class=" ti-close "></i></a>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row gx-0">
                                                    <div class="col-lg-5 col-md-5 col-12">
                                                        <div class="modal-img-wrap">
                                                            <img src="<?php echo base_url('assets/images/product/' . $p->product_image) ?>" alt="<?php echo $p->product_name; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-7 col-md-7 col-12">
                                                        <div class="product-details-content quickview-content">
                                                            <h2><?php echo $p->product_name; ?></h2>
                                                            <div class="product-details-price">
                                                                <!-- <span class="old-price">$25.89 </span> -->
                                                                <span class="new-price"><?php echo 'Rp. ' . number_format($p->product_price, 0, '', '.') . ',-'; ?> </span>
                                                            </div>
                                                            <?php if (!empty($p->product_desc)) { ?>
                                                                <p><?php $desc = substr($p->product_desc, 0, 350);
                                                                    echo $desc; ?> <a href="#"> ... Read more</a></p>
                                                            <?php } ?>

                                                            <div class="product-details-action-wrap">
                                                                <div class="product-quality">
                                                                    <input name="quantity" id="<?php echo $p->product_id; ?>" value="1" class="quantity form-control">

                                                                </div>
                                                                <div class="single-product-cart btn-hover">
                                                                    <a href="javascript:void(0)" class="add_cart" data-produkid="<?php echo $p->product_id; ?>" data-produknama="<?php echo $p->product_name; ?>" data-produkharga="<?php echo $p->product_price; ?>" data-produkimage="<?php echo $p->product_image; ?>"">Add to cart</a>
                                                                </div>
                                                                <div class=" single-product-wishlist">
                                                                        <a title="Wishlist" href="javascript:void(0)"><i class="pe-7s-like"></i></a>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- endmodal -->
                            <?php } ?>

                            <!-- <div class="pagination-style-1">
                                <ul>
                                    <li><a class="active" href="shop-sidebar.html#">1</a></li>
                                    <li><a href="shop-sidebar.html#">2</a></li>
                                    <li><a href="shop-sidebar.html#">3</a></li>
                                    <li><a class="next" href="shop-sidebar.html#"><i class=" ti-angle-double-right "></i></a></li>
                                </ul>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="col-lg-3">
                <div class="sidebar-wrapper">
                    <div class="sidebar-widget mb-40" data-aos="fade-up" data-aos-delay="200">
                        <div class="search-wrap-2">
                            <form class="search-2-form" action="#">
                                <input placeholder="Search*" type="text">
                                <button class="button-search"><i class=" ti-search "></i></button>
                            </form>
                        </div>
                    </div>

                    <div class="sidebar-widget sidebar-widget-border mb-40 pb-35" data-aos="fade-up" data-aos-delay="200">
                        <div class="sidebar-widget-title mb-25">
                            <h3>Categories</h3>
                        </div>
                        <div class="sidebar-list-style">
                            <ul>
                            <?php foreach ($category as $ct) { ?>
                                <li><a href="<?= base_url('categories/'.$ct->slug); ?>"><?php echo $ct->name;?> <span>1</span></a></li>
                            <?php }?>
                            </ul>
                        </div>
                    </div>

                </div>
            </div> -->
        </div>
    </div>
</div>
<?php }?>