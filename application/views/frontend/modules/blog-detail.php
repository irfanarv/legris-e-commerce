
<div class="breadcrumb-area bg-gray-4 breadcrumb-padding-1">
    <div class="container">
        <div class="breadcrumb-content text-center">
            <h2 data-aos="fade-up" data-aos-delay="200">Blog</h2>
            <ul data-aos="fade-up" data-aos-delay="400">
                <li><a href="index.html">Home</a></li>
                <li><i class="ti-angle-right"></i></li>
                <li>Blog </li>
            </ul>
        </div>
    </div>
    <div class="breadcrumb-img-1" data-aos="fade-right" data-aos-delay="200">
        <img src="<?= base_url(); ?>assets/frontend/images/banner/breadcrumb-1.png" alt="">
    </div>
    <div class="breadcrumb-img-2" data-aos="fade-left" data-aos-delay="200">
        <img src="<?= base_url(); ?>assets/frontend/images/banner/breadcrumb-2.png" alt="">
    </div>
</div>

<div class="blog-details-area pt-100 pb-100">
    <div class="container">
        <div class="row">
            
            <div class="col-lg-8">
                <?php foreach ($news as $p) : ?>
    
                <div class="blog-details-wrapper">
                    <div class="blog-details-img-date-wrap mb-35" data-aos="fade-up" data-aos-delay="200">
                        <div class="blog-details-img">
                            <img src="<?php echo base_url("assets/"); ?>images/news/<?php echo $p->post_img; ?>" alt="">
                        </div>
                        <div class="blog-details-date">
                            <h5><?= date('d', strtotime($p->post_created_at))."";?> <span><?= date('M', strtotime($p->post_created_at))."";?></span></h5>
                        </div>
                    </div>
                    <div class="blog-meta-2" data-aos="fade-up" data-aos-delay="200">
                        <ul>
                            <li>By:<a href="#"> <?= $p->ai_first_name;?></a></li>
                        </ul>
                    </div>
                    <h1 data-aos="fade-up" data-aos-delay="200"><?php echo $p->post_title; ?></h1>
                    <p data-aos="fade-up" data-aos-delay="200"><?php echo $p->post_desc; ?></p>
                    
                   
                </div>

                <?php endforeach; ?>
            </div>
            <div class="col-lg-4">
                <div class="sidebar-wrapper blog-sidebar-mt">
                    
                    <div class="sidebar-widget mb-40" data-aos="fade-up" data-aos-delay="200">
                        <div class="sidebar-widget-title-2 mb-25">
                            <h3>Categories</h3>
                        </div>
                        <div class="sidebar-list-style-2">
                            <ul>
                                <?php foreach ($catblog as $ct) : ?>
                                <li><a href="<?= base_url('blog/')?>"><?= $ct->cat_name;?></a></li>
                                <?php endforeach;?>
                            </ul>
                        </div>
                    </div>
                    <div class="sidebar-widget mb-40" data-aos="fade-up" data-aos-delay="200">
                        <div class="sidebar-widget-title-2 mb-30">
                            <h3>Latest Post</h3>
                        </div>
                        <div class="latest-post-wrap">
                            <?php foreach ($latepost as $lp) : ?>
                                <div class="single-latest-post">
                                    <div class="latest-post-img">
                                        <a href="<?= base_url('blog/detail/'); ?><?php echo $lp->post_slug; ?>"><img src="<?php echo base_url("assets/"); ?>images/news/<?php echo $lp->post_img; ?>"></a>
                                    </div>
                                    <div class="latest-post-content">
                                        <span><?= date('d M Y ', strtotime($lp->post_created_at))."";?></span>
                                        <h4><a href="<?= base_url('blog/detail/'); ?><?php echo $lp->post_slug; ?>">Lorem ipsum dolor sit am conse ctetur adipis</a></h4>
                                        <div class="latest-post-btn">
                                            <a href="<?= base_url('blog/detail/'); ?><?php echo $lp->post_slug; ?>">Continue Reading...</a>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach;?>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>