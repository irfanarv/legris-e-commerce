<div class="breadcrumb-area bg-gray-4 breadcrumb-padding-1">
    <div class="container">
        <div class="breadcrumb-content text-center">
            <h2 data-aos="fade-up" data-aos-delay="200">Wishlist</h2>
            <ul data-aos="fade-up" data-aos-delay="400">
                <li><a href="<?= base_url(); ?>">Home</a></li>
                <li><i class="ti-angle-right"></i></li>
                <li>Wishlist</li>
            </ul>
        </div>
    </div>
    <div class="breadcrumb-img-1">
        <img src="<?= base_url(); ?>assets/frontend/images/banner/breadcrumb-1.png" alt="">
    </div>
    <div class="breadcrumb-img-2">
        <img src="<?= base_url(); ?>assets/frontend/images/banner/breadcrumb-2.png" alt="">
    </div>
</div>

<div class="wishlist-area pb-100 pt-100">
    <div class="container">
        <div class="row">
            <div class="col-12">
            <?php if(!$wishList == ''){?>
                <div class="wishlist-table-content">
                    <div class="table-content table-responsive">
                        <table>
                            <thead>
                                <tr>
                                    <th class="width-remove"></th>
                                    <th class="width-thumbnail"></th>
                                    <th class="width-name">Product</th>
                                    <th class="width-price">  Price </th>
                                    <th class="width-wishlist-cart"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($wishList as $cl) { ?>
                                    <tr id="wish-list">
                                        <td class="product-remove"><button type="button" id="<?php echo $cl->wish_id; ?>" class="wishDelete btn">x</button></td>
                                        <td class="product-thumbnail">
                                            <a href="<?php echo $cl->product_slug; ?>"><img src="<?php echo base_url('assets/images/product/' . $cl->product_image) ?>" alt="<?php echo $cl->product_name; ?>"></a>
                                        </td>
                                        <td class="product-name">
                                            <h5><a href="<?php echo $cl->product_slug; ?>">Stylish Swing Chair</a></h5>
                                        </td>
                                        <td class="product-wishlist-price"><span class="amount"><?php echo 'Rp. ' . number_format($cl->product_price, 0, '', '.') . ',-'; ?></span></td>
                                        <td class="wishlist-cart btn-hover"><a href="javascript:void(0)" class="add_cart" data-produkid="<?php echo $cl->product_id; ?>" data-produknama="<?php echo $cl->product_name; ?>" data-produkharga="<?php echo $cl->product_price; ?>" data-produkimage="<?php echo $cl->product_image; ?>"">Add to cart</a></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            <?php } else { ?>
            <h3 class="text-center">Wishlist is empty !</h3>
            <?php } ?>
            </div>
        </div>
    </div>
</div>