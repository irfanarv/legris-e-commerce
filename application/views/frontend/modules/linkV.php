<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $pagetitle; ?></title>
    <meta name="robots" content="noindex, follow" />
    <meta name="description" content="<?= $meta_desc; ?>">
    <meta name="keywords" content="<?= $keyword; ?>"/>
    <meta property="og:title" content="<?= $meta_title; ?>" />
    <meta property="og:url" content="<?= current_url(); ?>" />
    <meta property="og:site_name" content="<?= $meta_name; ?>" />
    <meta property="og:image" content="<?= base_url(); ?>assets/images/logo.png" />
    <meta property="og:description" content="<?= $keyword; ?>"/>
    <link rel="icon" href="<?= base_url(); ?>assets/images/favicon.png" sizes="32x32" />
    <link rel="icon" href="<?= base_url(); ?>assets/images/favicon.png" sizes="192x192" />
    <link rel="apple-touch-icon" href="<?= base_url(); ?>assets/images/favicon.png" />
    <meta name="msapplication-TileImage" content="<?= base_url(); ?>assets/images/favicon.png" />
    <style>
        body {
            background: #C3B8AA;
            color: #fff;
            font-family: 'Open Sans', sans-serif;
            font-size: 15px;
            letter-spacing: 2px;
        }

        /* img {
            border-radius: 80%;
        } */

        .profile-picture {
            display: block;
            margin-left: auto;
            margin-right: auto;
            margin-top: 10%;
            height: auto;
            max-width: 100px;
        }

        .profile-name {
            text-align: center;
            padding: 30px;
        }

        .links {
            text-align: center;
            margin-top: 20px;
            padding: 20px;
            border: 1px solid white;
            border-width: 2px;
            width: 290px;
            display: block;
            margin-left: auto;
            margin-right: auto;
            border-radius: 40px;
            text-transform: uppercase;
        }

        a {
            text-decoration: none;
            color: white;
            transition: color 1s;
        }

        .bottom-text {
            text-align: center;
            margin-top: 40px;
            font-size: 20px;
            font-weight: bold;
        }

        a:hover {
            color: #000;
            background: #fff;
        }
    </style>
</head>

<body>

    <!-- Profile picture-->
    <img src="<?= base_url(); ?>assets/images/logo.png" alt="profile picture" class="profile-picture">

    <!-- Profile name-->
    <div class="profile-name">Legris Home Furniture</div>

    <!-- Links-->
    <?php foreach ($content as $p) : ?>
    <a href="<?= $p->uri;?>" target="_blank" class="links"><?= $p->name;?></a>
    <?php endforeach;?>
    <div class="bottom-text">Copyright ©<?php echo date('Y');?></div>

</body>

</html>