<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title><?php echo $pagetitle; ?></title>
	<meta name="robots" content="noindex, follow" />
	<meta name="description" content="<?= $meta_desc; ?>">
	<meta name="keywords" content="<?= $keyword; ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta property="og:title" content="<?= $meta_title; ?>" />
	<meta property="og:url" content="<?= current_url(); ?>" />
	<meta property="og:site_name" content="<?= $meta_name; ?>" />
	<meta property="og:image" content="<?= base_url(); ?>assets/images/logo.png" />
	<meta property="og:description" content="<?= $keyword; ?>" />
	<link rel="icon" href="<?= base_url(); ?>assets/images/favicon.png" sizes="32x32" />
	<link rel="icon" href="<?= base_url(); ?>assets/images/favicon.png" sizes="192x192" />
	<link rel="apple-touch-icon" href="<?= base_url(); ?>assets/images/favicon.png" />
	<meta name="msapplication-TileImage" content="<?= base_url(); ?>assets/images/favicon.png" />
	<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900' type='text/css'>
	<link rel="stylesheet" href="<?= base_url(); ?>assets/frontend/css/vendor/bootstrap.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/frontend/css/vendor/font-awesome.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/frontend/css/invoice.css" />
</head>

<body>
<?php foreach ($invoicedata as $i) { ?>
	<!-- Container -->
	<div class="container-fluid invoice-container">
		<!-- Header -->
		<header>
			<div class="row align-items-center">
				<div class="col-sm-7 text-left text-sm-left mb-3 mb-sm-0">
					<img src="<?= base_url(); ?>assets/images/logo.png" style="width: 70px;" class="img-fluid">
				</div>
				<div class="col-sm-5 text-right d-flex justify-content-end text-sm-right">
					<h4 class="text-7 mb-0">Invoice
					<br>
					<?php if($i->status_order == 'Menunggu'){?>
						<span class="text-danger">UNPAID</span>
					<?php } elseif($i->status_order == 'Selesai') { ?>
						<span class="text-success">PAID</span>
					<?php } elseif($i->status_order == 'Di Tolak') { ?>
					<span class="text-danger">REJECT</span>
					<?php } ?>
					</h4>
				</div>
			</div>
			<hr>
		</header>

		<!-- Main Content -->
		<main>
			<div class="row">
				<div class="col-sm-6"><strong>Order Date: </strong><?= date('d F Y', strtotime($i->tanggal_transaksi))."";?></div>
				<div class="col-sm-6 text-sm-right d-flex justify-content-end">
					<strong>Invoice No: </strong>#<?= $i->no_invoice?> 
				</div>

			</div>
			<hr>
			<div class="row">
				<div class="col-sm-6 text-align-right d-flex justify-content-end text-sm-right order-sm-1"> 
					
					<address class="text-right">
					<strong>Pay To:</strong><br>
						Account Name: <?= $i->bank_name?> <br />
						Account Number: <?= $i->bank_no?> <br />
					</address>
				</div>
				<div class="col-sm-6 order-sm-0"> <strong>Invoice To:</strong>
					<address>
						<?= $i->user_name?><br />
						<?= $i->user_mail?><br />
						<?= $i->user_phone?><br />
						<?= $i->user_address?><br />
						<?= $i->user_city?>, <?= $i->user_state?> <?= $i->user_zip?>
					</address>
				</div>
			</div>

			<div class="card">
				<div class="card-body p-0">
					<div class="table-responsive">
						<table class="table mb-0">
							<thead class="card-header">
								<tr>
									<td class="col-3 border-0"><strong>Product</strong></td>
									<td class="col-2 text-center border-0"><strong>Price</strong></td>
									<td class="col-1 text-center border-0"><strong>QTY</strong></td>
									<td class="col-2 text-right border-0"><strong>Amount</strong></td>
								</tr>
							</thead>
							<tbody>
							<?php foreach ($invoicedetail as $id) { ?>
								<tr>
									<td class="col-3 border-0"><?= $id->product_name; ?></td>
									<td class="col-2 text-center border-0"><?= 'Rp. ' . number_format( $id->price, 0 , '' , '.' ) . ',-'; ; ?></td>
									<td class="col-1 text-center border-0"><?= $id->jumlah; ?></td>
									<td class="col-2 text-right border-0"><?= 'Rp. ' . number_format( $id->sub_total, 0 , '' , '.' ) . ',-'; ; ?></td>
								</tr>
								<?php }?>
							</tbody>
							<tfoot class="card-footer">
								<!-- <tr>
									<td colspan="4" class="text-right"><strong>Sub Total:</strong></td>
									<td class="text-right">$2150.00</td>
								</tr>
								<tr>
									<td colspan="4" class="text-right"><strong>Tax:</strong></td>
									<td class="text-right">$215.00</td>
								</tr> -->
								<tr>
									<td colspan="3" class="text-right"><strong>Total:</strong></td>
									<td class="text-right"><?= 'Rp. ' . number_format( $i->total, 0 , '' , '.' ) . ',-'; ; ?></td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</main>
		<!-- Footer -->
		<footer class="text-center mt-4">
			<p class="text-1"><strong>NOTE :</strong> Attach proof of payment in live chat.</p>
			<div class="btn-group btn-group-sm d-print-none"> <a href="javascript:window.print()" class="btn btn-light border text-black-50 shadow-none"><i class="fa fa-print"></i> Print</a>
			<!-- <a href="" class="btn btn-light border text-black-50 shadow-none"><i class="fa fa-download"></i> Download</a>  -->
			</div>
		</footer>
	</div>

<?php }?>
</body>


</html>