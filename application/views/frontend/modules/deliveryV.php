<div class="breadcrumb-area bg-gray-4 breadcrumb-padding-1">
    <div class="container">
        <div class="breadcrumb-content text-center">
            <h2 data-aos="fade-up" data-aos-delay="200">Delivery <br> Information</h2>
            <ul data-aos="fade-up" data-aos-delay="400">
                <li><a href="<?= base_url(); ?>">Home</a></li>
                <li><i class="ti-angle-right"></i></li>
                <li>Delivery Information</li>
            </ul>
        </div>
    </div>
    <div class="breadcrumb-img-1">
        <img src="<?= base_url(); ?>assets/frontend/images/banner/breadcrumb-1.png" alt="">
    </div>
    <div class="breadcrumb-img-2">
        <img src="<?= base_url(); ?>assets/frontend/images/banner/breadcrumb-2.png" alt="">
    </div>
</div>
<?php foreach ($content as $p) : ?>
<div class="about-us-area pt-100 pb-100">
    <div class="container">
        <div class="row align-items-center flex-row-reverse">
            <div class="col-lg-6">
                <div class="about-content text-center">
                    <h2 data-aos="fade-up" data-aos-delay="200"><?= $p->title_page?></h2>
                    <p data-aos="fade-up" data-aos-delay="400"><?= $p->desc_page?></p>
                    
                </div>
            </div>
            <div class="col-lg-6">
                <div class="about-img" data-aos="fade-up" data-aos-delay="400">
                    <img src="<?php echo base_url("assets/images/page/"); ?><?php echo $p->image; ?>" alt="">
                </div>
            </div>
        </div>
    </div>
</div>
<?php endforeach; ?>
<div class="brand-logo-area pb-95">
    <div class="container">
        <div class="section-title-2 border-none text-center mb-5" data-aos="fade-up" data-aos-delay="200">
            <h2>Our Partner</h2>
        </div>
        <div class="brand-logo-active swiper-container">
            <div class="swiper-wrapper">
            <?php foreach ($clientLogo as $cl) { ?>
                <div class="swiper-slide">
                    <div class="single-brand-logo" data-aos="fade-up" data-aos-delay="200">
                        <a href="#"><img src="<?php echo base_url('assets/images/client/' . $cl->image) ?>"></a>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>