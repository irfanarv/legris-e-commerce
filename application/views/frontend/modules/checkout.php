<div class="breadcrumb-area bg-gray-4 breadcrumb-padding-1">
    <div class="container">
        <div class="breadcrumb-content text-center">
            <h2>Checkout </h2>
            <ul>
                <li><a href="<?= base_url(); ?>">Home</a></li>
                <li><i class="ti-angle-right"></i></li>
                <li>Checkout </li>
            </ul>
        </div>
    </div>
    <div class="breadcrumb-img-1">
        <img src="<?= base_url(); ?>assets/frontend/images/banner/breadcrumb-1.png" alt="">
    </div>
    <div class="breadcrumb-img-2">
        <img src="<?= base_url(); ?>assets/frontend/images/banner/breadcrumb-2.png" alt="">
    </div>
</div>

<div class="checkout-main-area pb-100 pt-100">
    <div class="container">
        <?php if (!$this->session->userdata('mail')) { ?>
            <div class="customer-zone mb-20">
                <p class="cart-page-title">Returning customer? <a class="checkout-click1" href="checkout.html#">Click here to login</a></p>
                <div class="checkout-login-info">
                    <p>If you have shopped with us before, please enter your details in the boxes below. If you are a new customer, please proceed to the Billing & Shipping section.</p>
                    <form method="POST" class="form-signincheckout form-horizontal" role="form" onsubmit="return false;">
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <div class="sin-checkout-login">
                                    <label> Email Address <span>*</span></label>
                                    <input type="email" id="email" name="email" placeholder="Email">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="sin-checkout-login">
                                    <label>Passwords <span>*</span></label>
                                    <input type="password" id="password" name="password" placeholder="Password">
                                </div>
                            </div>
                        </div>
                        <div class="button-remember-wrap">
                            <button id="btnLog" class="button" type="submit">Login</button>
                        </div>
                    </form>
                </div>
            </div>

        <?php } else { ?>
        <?php } ?>

        <!-- <div class="customer-zone mb-20">
            <p class="cart-page-title">Have a coupon? <a class="checkout-click3" href="javascript:void(0)">Click here to enter your code</a></p>
            <div class="checkout-login-info3">
                <form action="#">
                    <input type="text" placeholder="Coupon code">
                    <input type="submit" value="Apply Coupon">
                </form>
            </div>
        </div> -->
        <div class="checkout-wrap pt-10">
        <form action="<?php echo base_url() . 'checkout/process' ?>" method="post" id="myForm" enctype="multipart/form-data">
            <form action="javascript:void(0)" method="post" id="form" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-lg-7">
                        <div class="billing-info-wrap">
                            <h3>Billing Details</h3>
                            <div class="row">
                                <div class="col-lg-12 col-md-12">
                                    <div class="billing-info mb-20">
                                        <label>Full Name <abbr class="required" title="required">*</abbr></label>
                                        <input type="text" name="full_name" id="full_name" placeholder="Full Name" value="<?= $full_name; ?>" required>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="billing-info mb-20">
                                        <label>Email <abbr class="required" title="required">*</abbr></label>
                                        <input type="email" name="email" id="email" placeholder="Email" value="<?= $accounts_email; ?>" required>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="billing-info mb-20">
                                        <label>Phone <abbr class="required" title="required">*</abbr></label>
                                        <input type="number" name="phone" id="phone" placeholder="Phone Number" value="<?= $ai_phone; ?>" required>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12">
                                    <div class="billing-info mb-20">
                                        <label>Address <abbr class="required" title="required">*</abbr></label>
                                        <textarea placeholder="House number and street name" name="address" id="address" required><?= $ai_address; ?></textarea>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-4">
                                    <div class="billing-info mb-20">
                                        <label>City <abbr class="required" title="required">*</abbr></label>
                                        <input type="text" name="city" id="city" placeholder="City" value="<?= $city; ?>" required>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="billing-info mb-20">
                                        <label>State <abbr class="required" title="required">*</abbr></label>
                                        <input type="text" name="state" id="state" placeholder="State" value="<?= $state; ?>" required>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="billing-info mb-20">
                                        <label>Postcode / ZIP <abbr class="required" title="required">*</abbr></label>
                                        <input type="text" name="zip" id="zip" placeholder="Postcode / ZIP" value="<?= $zip; ?>" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="your-order-area">
                            <h3>Your order</h3>

                            <div class="your-order-wrap gray-bg-4">

                                <div class="your-order-info-wrap">
                                    <div class="your-order-info">
                                        <ul>
                                            <li>Product <span>Total</span></li>
                                        </ul>
                                    </div>
                                    <div class="your-order-middle">
                                        <ul>
                                            <?php foreach ($this->cart->contents() as $items) { ?>
                                                <li><?php echo $items['name'] . ' X ' . $items['qty'] . '<span>' . 'Rp. ' . number_format($items['subtotal']) . '</span>' ?></li>
                                            <?php } ?>
                                        </ul>

                                    </div>
                                    <div class="your-order-info order-total">
                                        <ul>
                                            <li>Total <span>Rp. <?php echo number_format($this->cart->total()); ?> </span></li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="payment-method">
                                    <div class="pay-top sin-payment">
                                        <input id="payment_method_1" class="input-radio" type="radio" value="cheque" checked="checked" name="payment_method">
                                        <label for="payment_method_1"> Direct Bank Transfer </label>
                                        <div class="payment-box payment_method_bacs">
                                            <p>Make your payment directly into our bank account. Please use your Order ID as the payment reference.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="billing-info-wrap">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="billing-info mb-20">
                                                <label>Select Account Bank <abbr class="required" title="required">*</abbr></label>
                                                <select id="bank_id" name="bank_id" required>
                                                    <?php foreach ($bankAccount as $bl) { ?>
                                                        <option value="<?= $bl->bank_id; ?>"><?= $bl->bank_name; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="Place-order btn-hover">
                                <a href="javascript:void(0)" onclick="place()">Place Order</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="modal_place" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <h4 class="mt-3 text-center" style=" justify-content:center"> Harga belum termasuk ongkos kirim, pastikan Anda menghubungi Admin melalui fitur live chat untuk memastikan ketersediaan dan ongkos kirim barang.</h4>
                                <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
                                <lottie-player src="https://assets1.lottiefiles.com/packages/lf20_uyfxzh9u.json" background="transparent" speed="1" hover loop autoplay></lottie-player>
                                <div class="Place-order btn-hover mt-3 d-flex justify-content-center">
                                    <button type="submit" class="btn btn-primary">Process Order</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </form>
        </div>
    </div>
</div>

<script src="<?= base_url(); ?>assets/frontend/js/vendor/jquery-3.6.0.min.js"></script>
<script type="text/javascript">
    function place() {
        $('#modal_place').modal('show');
    }

    // $('#form').submit(function(e) {
    //     var url;
    //     url = "<?php echo site_url('checkout/process') ?>";
    //     e.preventDefault();
    //     $.ajax({
    //         "url": url,
    //         type: "post",
    //         data: new FormData(this),
    //         processData: false,
    //         contentType: false,
    //         cache: false,
    //         async: false,
    //         success: function(data) {
                                
    //             $('#form')[0].reset();
    //             $('#modal_place').modal('hide');
                

    //         }

    //     });
    // });
</script>