<nav class="pcoded-navbar menu-light ">
	<div class="navbar-wrapper  ">
		<div class="navbar-content scroll-div ">
			<div class="">
				<div class="main-menu-header">
					<?php if (!$this->session->userdata('image')){ $profile = $this->session->userdata('image'); ?>
						<img class="img-radius" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRoyY5b_1yBKpIHqBpE1IOslAL3VOdtNbuCj0PZXGo6-HGUetscjNK6hbbnLaLJw7Y1aVI&usqp=CAU">
						<!-- <img class="img-radius" src="<?php echo base_url("assets/images/profile/"."$profile"); ?>"> -->
					<?php }else{?>
						<img class="img-radius" src="<?php echo base_url("assets/images/profile/be505fc05352f527b24ad9504a3408f1.png"); ?>">
					<?php } ?>
					<div class="user-details">
						<div id="more-details"><?php echo $this->session->userdata('fullName');  ?></div>
					</div>
				</div>
			</div>

			<ul class="nav pcoded-inner-navbar mt-3">
				<li class="nav-item <?php if($this->uri->segment(2)==""){echo 'active';}?>">
					<a href="<?= base_url('dashboard'); ?>" class="nav-link "><span class="pcoded-micon"><i class="feather icon-home"></i></span><span class="pcoded-mtext">Dashboard</span></a>
				</li>
				<li class="nav-item pcoded-hasmenu <?php if($this->uri->segment(2)=="category-products " || $this->uri->segment(2)=="products" || $this->uri->segment(2)=="new-products" || $this->uri->segment(2)=="edit-products" || $this->uri->segment(2)=="type-products" || $this->uri->segment(2)=="products-gallery"){echo 'active pcoded-trigger';}?>"">
					<a href="#!" class="nav-link "><span class="pcoded-micon"><i class="feather icon-package"></i></span><span class="pcoded-mtext">Product</span></a>
					<ul class="pcoded-submenu">
						<li <?php if($this->uri->segment(2)=="new-products"){echo 'active';}?>><a href="<?= base_url(); ?>dashboard/add-products">Publish new product</a></li>
						<li <?php if($this->uri->segment(2)=="products" || $this->uri->segment(2)=="edit-products" ){echo 'active';}?>><a href="<?= base_url(); ?>dashboard/products">Product List</a></li>
						<li class="<?php if($this->uri->segment(2)=="category-products"){echo 'active';}?>"><a href="<?= base_url(); ?>dashboard/category-products">Category Product</a></li>
						<li class="<?php if($this->uri->segment(2)=="type-products"){echo 'active';}?>"><a href="<?= base_url(); ?>dashboard/type-products">Type Product</a></li>
						<!-- <li><a href="dashboard-crm.html">Rating & Review</a></li> -->
					</ul>
				</li>
				<li class="nav-item <?php if($this->uri->segment(2)=="orders"){echo 'active';}?>">
					<a href="<?= base_url('dashboard/orders'); ?>" class="nav-link "><span class="pcoded-micon"><i class="feather icon-shopping-cart"></i></span><span class="pcoded-mtext">Orders</span></a>
				</li>


				<li class="nav-item pcoded-hasmenu <?php if($this->uri->segment(2)=="category-news" || $this->uri->segment(2)=="news" || $this->uri->segment(2)=="edit-news"){echo 'active pcoded-trigger';}?>"">
					<a href="#!" class="nav-link "><span class="pcoded-micon"><i class="feather icon-book"></i></span><span class="pcoded-mtext">News</span></a>
					<ul class="pcoded-submenu">
						<li class="<?php if($this->uri->segment(2)=="add-news"){echo 'active';}?>"><a href="<?= base_url(); ?>dashboard/add-news">Add News</a></li>
						<li class="<?php if($this->uri->segment(2)=="news" || $this->uri->segment(2)=="edit-news"){echo 'active';}?>"><a href="<?= base_url(); ?>dashboard/news">List News</a></li>
						<li class="<?php if($this->uri->segment(2)=="category-news"){echo 'active';}?>"><a href="<?= base_url(); ?>dashboard/category-news">Category</a></li>
					</ul>
				</li>
				
				<li class="nav-item <?php if($this->uri->segment(2)=="bank-account"){echo 'active';}?>">
					<a href="<?= base_url('dashboard/bank-account'); ?>" class="nav-link "><span class="pcoded-micon"><i class="feather icon-umbrella"></i></span><span class="pcoded-mtext">Bank Accounts</span></a>
				</li>
				<li class="nav-item <?php if($this->uri->segment(2)=="slider"){echo 'active';}?>">
					<a href="<?= base_url('dashboard/slider'); ?>" class="nav-link "><span class="pcoded-micon"><i class="feather icon-image"></i></span><span class="pcoded-mtext">Sliders</span></a>
				</li>


				<li class="nav-item <?php if($this->uri->segment(2)=="our-client"){echo 'active';}?>">
					<a href="<?= base_url('dashboard/our-client'); ?>" class="nav-link "><span class="pcoded-micon"><i class="feather icon-sunrise"></i></span><span class="pcoded-mtext">Our Partner</span></a>
				</li>

				<li class="nav-item pcoded-hasmenu <?php if($this->uri->segment(2)=="admin"){echo 'active pcoded-trigger';}?>"">
					<a href="#" class="nav-link "><span class="pcoded-micon"><i class="feather icon-users"></i></span><span class="pcoded-mtext">User Management</span></a>
					<ul class="pcoded-submenu">
						<li <?php if($this->uri->segment(2)=="admin"){echo 'active';}?>><a href="<?= base_url(); ?>dashboard/account/">Administrator</a></li>
						<li <?php if($this->uri->segment(2)=="member"){echo 'active';}?>><a href="<?= base_url(); ?>dashboard/member">Member</a></li>
					</ul>
				</li>
				<li class="nav-item <?php if($this->uri->segment(2)=="pages"){echo 'active';}?>">
					<a href="<?= base_url('dashboard/pages'); ?>" class="nav-link "><span class="pcoded-micon"><i class="feather icon-copy"></i></span><span class="pcoded-mtext">Page Management</span></a>
				</li>
				<li class="nav-item <?php if($this->uri->segment(2)=="link"){echo 'active';}?>">
					<a href="<?= base_url('dashboard/link'); ?>" class="nav-link "><span class="pcoded-micon"><i class="feather icon-share-2"></i></span><span class="pcoded-mtext">Legris Link</span></a>
				</li>

				<li class="nav-item <?php if($this->uri->segment(2)=="settings"){echo 'active';}?>">
					<a href="<?= base_url('dashboard/settings'); ?>" class="nav-link "><span class="pcoded-micon"><i class="feather icon-settings"></i></span><span class="pcoded-mtext">Settings</span></a>
				</li>


			</ul>
		</div>
	</div>
</nav>