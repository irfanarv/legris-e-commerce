<header class="navbar pcoded-header navbar-expand-lg navbar-light header-blue">


<div class="m-header">
    <a class="mobile-menu" id="mobile-collapse" href="#!"><span></span></a>
    <a href="#!" class="b-brand">
        <!-- <img src="<?= base_url(); ?>assets/backend/images/logo.png" alt="" class="logo"> -->
        <h5 class="" style="color: white;">Le Gris Home</h5>

    </a>
    <a href="#!" class="mob-toggler">
        <i class="feather icon-more-vertical"></i>
    </a>
</div>
<div class="collapse navbar-collapse">
   
    <ul class="navbar-nav ml-auto">
        <li>
            <a href="<?= base_url();?>" target="_new" class="dropdown">
                <i class="feather icon-airplay"></i>
            </a>
        </li>
        <li>
            <div class="dropdown drp-user">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="feather icon-user"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right profile-notification">
                    <div class="pro-head">
                        <!-- <img src="<?= base_url(); ?>assets/backend/images/user/avatar-1.jpg" class="img-radius" alt="User-Profile-Image"> -->
                        <span><?php echo $this->session->userdata('fullName'); ?> </span>
                        <a href="<?= base_url();?>dashboard/logout" class="dud-logout" title="Logout">
                            <i class="feather icon-log-out"></i>
                        </a>
                    </div>
                    <!-- <ul class="pro-body">
                        <li><a href="user-profile.html" class="dropdown-item"><i class="feather icon-user"></i> Profile</a></li>
                        <li><a href="email_inbox.html" class="dropdown-item"><i class="feather icon-mail"></i> My Messages</a></li>
                        <li><a href="auth-signin.html" class="dropdown-item"><i class="feather icon-lock"></i> Lock Screen</a></li>
                    </ul> -->
                </div>
            </div>
        </li>
    </ul>
</div>


</header>