<!DOCTYPE html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?php echo $pagetitle; ?></title>
    <meta name="robots" content="noindex, follow" />
    <meta name="description" content="<?= $meta_desc; ?>">
    <meta name="keywords" content="<?= $keyword; ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:title" content="<?= $pagetitle; ?>" />
    <meta property="og:url" content="<?= current_url(); ?>" />
    <meta property="og:site_name" content="<?= $meta_name; ?>" />
    <meta property="og:image" content="<?= base_url(); ?>assets/images/logo3.jpeg" />
    <meta property="og:description" content="<?= $meta_desc; ?>"/>
    <link rel="icon" href="<?= base_url(); ?>assets/images/favicon.png" sizes="32x32" />
    <link rel="icon" href="<?= base_url(); ?>assets/images/favicon.png" sizes="192x192" />
    <link rel="apple-touch-icon" href="<?= base_url(); ?>assets/images/favicon.png" />
    <meta name="msapplication-TileImage" content="<?= base_url(); ?>assets/images/favicon.png" />
    <link rel="stylesheet" href="<?= base_url(); ?>assets/frontend/css/vendor/bootstrap.min.css" />
    <link rel="stylesheet" href="<?= base_url(); ?>assets/frontend/css/vendor/pe-icon-7-stroke.css" />
    <link rel="stylesheet" href="<?= base_url(); ?>assets/frontend/css/vendor/themify-icons.css" />
    <link rel="stylesheet" href="<?= base_url(); ?>assets/frontend/css/vendor/font-awesome.min.css" />
    <link rel="stylesheet" href="<?= base_url(); ?>assets/frontend/css/plugins/animate.css" />
    <link rel="stylesheet" href="<?= base_url(); ?>assets/frontend/css/plugins/aos.css" />
    <link rel="stylesheet" href="<?= base_url(); ?>assets/frontend/css/plugins/magnific-popup.css" />
    <link rel="stylesheet" href="<?= base_url(); ?>assets/frontend/css/plugins/swiper.min.css" />
    <link rel="stylesheet" href="<?= base_url(); ?>assets/frontend/css/plugins/jquery-ui.css" />
    <link rel="stylesheet" href="<?= base_url(); ?>assets/frontend/css/plugins/nice-select.css" />
    <link rel="stylesheet" href="<?= base_url(); ?>assets/frontend/css/plugins/select2.min.css" />
    <link rel="stylesheet" href="<?= base_url(); ?>assets/frontend/css/plugins/easyzoom.css" />
    <link rel="stylesheet" href="<?= base_url(); ?>assets/frontend/css/plugins/slinky.css" />
    <link rel="stylesheet" href="<?= base_url(); ?>assets/frontend/css/style.css" />
    <style>
        .modal-dialog {
            min-height: calc(100vh - 60px);
            display: flex;
            flex-direction: column;
            justify-content: center;
        }

        @media(max-width: 768px) {
            .modal-dialog {
                min-height: calc(100vh - 20px);
            }
        }
    </style>
<script type="text/javascript" src="https://platform-api.sharethis.com/js/sharethis.js#property=60d9de5ed207a700123b2df6&product=inline-share-buttons" async="async"></script>
</head>

<body>

    <div class="main-wrapper main-wrapper-2">
        <?= $_header; ?>
        <div class="sidebar-cart-active">
            <div class="sidebar-cart-all">
                <a class="cart-close" href="javascript:void(0)"><i class="pe-7s-close"></i></a>
                <div class="cart-content" id="detailCart">
                </div>
            </div>
        </div>

        <?= $_content; ?>
        <?= $_footer; ?>
        <!-- Mobile Menu start -->
        <div class="off-canvas-active">
            <a class="off-canvas-close"><i class=" ti-close "></i></a>
            <div class="off-canvas-wrap">
                <div class="welcome-text off-canvas-margin-padding">
                    <p> <?= $pesan;?></p>
                </div>
                <div class="mobile-menu-wrap off-canvas-margin-padding-2">
                    <div id="mobile-menu" class="slinky-mobile-menu text-left">
                        <ul>
                            <li><a href="<?= base_url(); ?>">HOME</a></li>
                            <li><a href="<?= base_url(); ?>wholesale">WHOLESALE</a></li>
                            <li>
                                <a href="<?= base_url(); ?>product">PRODUCTS</a>
                                <ul>
                                    <?php foreach ($menus as $menu) { ?>
                                        <li>
                                            <a href="#"><?php echo $menu->name_type; ?></a>
                                            <ul>
                                                <?php if (isset($menu->category)) {
                                                    foreach ($menu->category as $child) { ?>
                                                        <li><a href="<?= base_url('categories/' . $child->slug); ?>"><?php echo $child->name; ?></a></li>
                                                <?php }
                                                } ?>
                                            </ul>
                                        </li>

                                    <?php } ?>
                                </ul>
                            </li>

                            <li><a href="<?= base_url(); ?>about-us">ABOUT US</a></li>
                            <li><a href="<?= base_url(); ?>blog">BLOG</a></li>
                            <li><a href="<?= base_url(); ?>contact-us">CONTACT US</a></li>



                        </ul>
                    </div>
                </div>
                <div class="welcome-text">
                    <p> <a href="<?= $ig_uri; ?>" target="_blank"> <i class="ti-instagram"></i> <?= $ig_title; ?></a></p>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modal_success" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <h2 class="text-center mt-3">Product Success added !</h2>
                        <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
                        <lottie-player src="https://assets3.lottiefiles.com/packages/lf20_HT2xcH.json" background="transparent" speed="1" autoplay loop></lottie-player>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal_successwish" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <h2 class="text-center mt-3">Product Success added to wishlist !</h2>
                        <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
                        <lottie-player src="https://assets3.lottiefiles.com/packages/lf20_HT2xcH.json" background="transparent" speed="1" autoplay loop></lottie-player>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal_failedregist" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <h2 class="print-error-msg text-center mt-3"></h2>
                        <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
                        <lottie-player src="https://assets10.lottiefiles.com/packages/lf20_tijmpky4.json" background="transparent" speed="1" hover loop autoplay></lottie-player>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade quickview-modal-style" id="signin" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <a href="javascript:void(0)" class="close" data-bs-dismiss="modal" aria-label="Close"><i class=" ti-close "></i></a>
                    </div>
                    <div class="modal-body">
                        <div class="login-register-area pb-100 pt-95">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-8 col-md-12 offset-lg-2">
                                        <div class="login-register-wrapper">
                                            <div class="login-register-tab-list nav">
                                                <a class="active" data-bs-toggle="tab" href="#lg1">
                                                    <h4> login </h4>
                                                </a>
                                                <a data-bs-toggle="tab" href="#lg2">
                                                    <h4> register </h4>
                                                </a>
                                            </div>
                                            <div class="tab-content">
                                                <div id="lg1" class="tab-pane active">
                                                    <div class="login-form-container">
                                                        <div class="login-register-form">
                                                            <form method="POST" class="form-signin form-horizontal" role="form" onsubmit="return false;">
                                                                <input type="email" id="email" name="email" placeholder="Email">
                                                                <input type="password" id="password" name="password" placeholder="Password">
                                                                <div class="login-toggle-btn">
                                                                    <input type="checkbox">
                                                                    <label>Remember me</label>
                                                                    <a data-bs-toggle="tab" href="#forgot">Forgot Password?</a>
                                                                </div>
                                                                <div class="button-box btn-hover">
                                                                    <button id="btnLog" type="submit">Login</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="lg2" class="tab-pane">
                                                    <div class="login-form-container">
                                                        <div class="login-register-form">
                                                            <form id="submit" enctype="multipart/form-data">
                                                                <input type="text" name="full_name" id="full_name" placeholder="Full Name">
                                                                <input name="email" id="email" placeholder="Email" type="email" required>
                                                                <input type="password" id="password" name="password" placeholder="Password" required>
                                                                <input type="password" id="confirm_password" name="confirm_password" placeholder="Confirm Password" required>
                                                                <div class="button-box btn-hover">
                                                                    <button type="submit" id="btnSave">Register</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="forgot" class="tab-pane">
                                                    <div class="login-form-container">
                                                        <div class="login-register-form">
                                                            <form>
                                                                <input name="email" id="email" placeholder="Email" type="email" required>
                                                                <div class="button-box btn-hover">
                                                                    <button type="submit">Send New Password</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade quickview-modal-style" id="access-catalouge" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <a href="javascript:void(0)" class="close" data-bs-dismiss="modal" aria-label="Close"><i class=" ti-close "></i></a>
                    </div>
                    <div class="modal-body">
                        <div class="login-register-area pb-100 pt-95">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-8 col-md-12 offset-lg-2">
                                        <div class="login-register-wrapper">
                                            
                                            <div class="login-register-tab-list nav">
                                                <a class="active" data-bs-toggle="tab" href="#ac">
                                                    <h4> Wholesale </h4>
                                                </a>
                                            </div>
                                            <div class="tab-content">
                                                <div id="ac" class="tab-pane active">
                                                    <div class="login-form-container">
                                                        <div class="login-register-form">
                                                            <form method="POST" class="form-signin-catalouge form-horizontal" role="form" onsubmit="return false;">
                                                                <input type="password" id="password" name="password" placeholder="Input Password">
                                                                <div class="button-box btn-hover">
                                                                    <button id="btnLog" type="submit">Submit</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- All JS is here -->
    <script src="<?= base_url(); ?>assets/frontend/js/vendor/modernizr-3.11.2.min.js"></script>
    <script src="<?= base_url(); ?>assets/frontend/js/vendor/jquery-3.6.0.min.js"></script>
    <script src="<?= base_url(); ?>assets/frontend/js/vendor/jquery-migrate-3.3.2.min.js"></script>
    <script src="<?= base_url(); ?>assets/frontend/js/vendor/popper.min.js"></script>
    <script src="<?= base_url(); ?>assets/frontend/js/vendor/bootstrap.min.js"></script>
    <script src="<?= base_url(); ?>assets/frontend/js/plugins/wow.js"></script>
    <script src="<?= base_url(); ?>assets/frontend/js/plugins/scrollup.js"></script>
    <script src="<?= base_url(); ?>assets/frontend/js/plugins/aos.js"></script>
    <script src="<?= base_url(); ?>assets/frontend/js/plugins/magnific-popup.js"></script>
    <script src="<?= base_url(); ?>assets/frontend/js/plugins/jquery.syotimer.min.js"></script>
    <script src="<?= base_url(); ?>assets/frontend/js/plugins/swiper.min.js"></script>
    <script src="<?= base_url(); ?>assets/frontend/js/plugins/imagesloaded.pkgd.min.js"></script>
    <script src="<?= base_url(); ?>assets/frontend/js/plugins/isotope.pkgd.min.js"></script>
    <script src="<?= base_url(); ?>assets/frontend/js/plugins/jquery-ui.js"></script>
    <script src="<?= base_url(); ?>assets/frontend/js/plugins/jquery-ui-touch-punch.js"></script>
    <script src="<?= base_url(); ?>assets/frontend/js/plugins/jquery.nice-select.min.js"></script>
    <script src="<?= base_url(); ?>assets/frontend/js/plugins/waypoints.min.js"></script>
    <script src="<?= base_url(); ?>assets/frontend/js/plugins/counterup.min.js"></script>
    <script src="<?= base_url(); ?>assets/frontend/js/plugins/select2.min.js"></script>
    <script src="<?= base_url(); ?>assets/frontend/js/plugins/easyzoom.js"></script>
    <script src="<?= base_url(); ?>assets/frontend/js/plugins/slinky.min.js"></script>
    <script src="<?= base_url(); ?>assets/frontend/js/plugins/ajax-mail.js"></script>
    <!-- Main JS -->
    <script src="<?= base_url(); ?>assets/frontend/js/main.js"></script>
    <!-- cart -->
    <script type="text/javascript">
        $(document).ready(function() {
            $('.add_cart').click(function() {
                var produk_id = $(this).data("produkid");
                var produk_nama = $(this).data("produknama");
                var produk_harga = $(this).data("produkharga");
                var produk_image = $(this).data("produkimage");
                var quantity = $('#' + produk_id).val();
                $.ajax({
                    url: "<?php echo base_url(); ?>Home/AddCart",
                    method: "POST",
                    data: {
                        produk_id: produk_id,
                        produk_nama: produk_nama,
                        produk_harga: produk_harga,
                        produk_image: produk_image,
                        quantity: quantity
                    },
                    success: function(data) {
                        $('#' + produk_id).val('1');
                        $('#detailCart').html(data);
                        $('#modal_success').modal('show');
                        setTimeout(function() {
                            $('#modal_success').modal('hide');
                        }, 1800);
                        $('#cart-counter').load(location.href + " #cart-counter");
                    }
                });
            });


            $('#detailCart').load("<?php echo base_url(); ?>Home/loadCart");

            $(document).on('click', '.deleteItem', function() {
                var row_id = $(this).attr("id");
                $.ajax({
                    url: "<?php echo base_url(); ?>Home/deleteCart",
                    method: "POST",
                    data: {
                        row_id: row_id
                    },
                    success: function(data) {
                        $('#detailCart').html(data);
                        $('#cart-counter').load(location.href + " #cart-counter");
                    }
                });
            });
        });
    </script>
    <!-- register -->
    <script type="text/javascript">
        $(document).ready(function() {
            $('#submit').submit(function(e) {
                e.preventDefault();
                $('#btnSave').attr('disabled', true);
                $.ajax({
                    url: '<?php echo site_url('Auth/registerCustomer') ?>',
                    type: "POST",
                    dataType: "json",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    cache: false,
                    async: false,
                    success: function(data) {
                        if ($.isEmptyObject(data.error)) {
                            var redirect;
                            redirect = "<?php echo site_url('my-account') ?>";
                            location.href = redirect;
                            $('#btnSave').attr('disabled', false);
                        } else {
                            $('#signin').modal('hide');
                            $('#modal_failedregist').modal('show');
                            $(".print-error-msg").html(data.error);
                            $('#btnSave').attr('disabled', false);
                            setTimeout(function() {
                                $('#modal_failedregist').modal('hide');
                            }, 5000);
                        }

                    }


                });
            });
        });
    </script>
    <!-- login -->
    <script type="text/javascript">
        $(function() {
            $(".form-signin").on('submit', function() {
                $.post("<?php echo site_url('Auth/attemp') ?>", $(".form-signin").serialize(), function(response) {
                    var resp = $.parseJSON(response);
                    console.log(resp);
                    if (!resp.status) {
                        $('#signin').modal('hide');
                        $('#modal_failedregist').modal('show');
                        $(".print-error-msg").html(resp.error);
                        $('#btnLog').attr('disabled', false);
                        setTimeout(function() {
                            $('#modal_failedregist').modal('hide');
                        }, 5000);
                    } else {
                        var redirect;
                        redirect = "<?php echo site_url('my-account') ?>";
                        location.href = redirect;
                        $('#btnLog').attr('disabled', false);
                    }
                });
            });

            $(".form-signin-catalouge").on('submit', function() {
                $.post("<?php echo site_url('Wholesale/openCatalouge') ?>", $(".form-signin-catalouge").serialize(), function(response) {
                    var resp = $.parseJSON(response);
                    console.log(resp);
                    if (!resp.status) {
                        $('#access-catalouge').modal('hide');
                        $('#modal_failedregist').modal('show');
                        $(".print-error-msg").html(resp.error);
                        $('#btnLog').attr('disabled', false);
                        setTimeout(function() {
                            $('#modal_failedregist').modal('hide');
                        }, 5000);
                    } else {
                        var redirect;
                        redirect = "<?php echo site_url('wholesale') ?>";
                        location.href = redirect;
                        $('#btnLog').attr('disabled', false);
                    }
                });
            });

            $(".form-signincheckout").on('submit', function() {
                $.post("<?php echo site_url('Auth/attemp') ?>", $(".form-signincheckout").serialize(), function(response) {
                    var resp = $.parseJSON(response);
                    console.log(resp);
                    if (!resp.status) {
                        $('#modal_failedregist').modal('show');
                        $(".print-error-msg").html(resp.error);
                        $('#btnLog').attr('disabled', false);
                        setTimeout(function() {
                            $('#modal_failedregist').modal('hide');
                        }, 5000);
                    } else {
                        var redirect;
                        redirect = "<?php echo site_url('checkout') ?>";
                        location.href = redirect;
                        $('#btnLog').attr('disabled', false);
                    }
                });
            });

            $(".resetbtn").on('click', function() {
                var resetemail = $("#resetemail").val();
                if (resetemail == "") {
                    $('#pleasemodal').modal('show');
                } else {
                    $(".resultreset").html("<div class='matrialprogress'><div class='indeterminate'></div></div>");
                    $.post("<?php echo base_url() . $this->uri->segment(1); ?>/resetpass", $("#passresetfrm").serialize(), function(response) {
                        if ($.trim(response) == '1') {
                            $(".resultreset").html("<div class='alert alert-success'>New Password sent to " + resetemail + ", Kindly check email.</div>");
                        } else {
                            $(".resultreset").html("<div class='alert alert-danger'>Email Not Found</div>");
                        }
                    });
                }
            });

        });
    </script>

    <?php if ($this->session->flashdata('msg') == 'login') : ?>
        <script>
            $(window).on('load', function() {
                $('#signin').modal('show');
            });
        </script>
    <?php else : ?>
    <?php endif; ?>

    <?php if ($this->session->flashdata('msg') == 'access-catalouge') : ?>
        <script>
            $(window).on('load', function() {
                $('#access-catalouge').modal('show');
            });
        </script>
    <?php else : ?>
    <?php endif; ?>
    <!-- add wishlist -->
    <script type="text/javascript">
        $(document).ready(function() {
            $('.add_wish').click(function() {
                var produk_id = $(this).data("produkid");
                var user_mail = $(this).data("usermail");

                $.ajax({
                    url: "<?php echo base_url(); ?>Home/AddWish",
                    method: "POST",
                    data: {
                        produk_id: produk_id,
                        user_mail: user_mail
                    },
                    success: function(data) {

                        $('#modal_successwish').modal('show');
                        setTimeout(function() {
                            $('#modal_successwish').modal('hide');
                        }, 1800);
                        $('#wish-counter').load(location.href + " #wish-counter");
                    }
                });
            });

            $(document).on('click', '.wishDelete', function() {
                var wish_id = $(this).attr("id");
                $.ajax({
                    url: "<?php echo base_url(); ?>Home/deleteWish",
                    method: "POST",
                    data: {
                        wish_id: wish_id
                    },
                    success: function(data) {
                        $('#wish-counter').load(location.href + " #wish-counter");
                        $('#wish-list').load(location.href + " #wish-list");
                    }
                });
            });
        });
    </script>
</body>

</html>