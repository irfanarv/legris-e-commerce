<footer class="footer-area">
    <div class="bg-gray-2">
        <div class="container">
            <div class="footer-top pt-80 pb-35">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                        <div class="footer-widget footer-about mb-40">
                            <div class="footer-logo mb-4">
                                <a href="<?= base_url();?>"><img src="<?= base_url(); ?>assets/images/logo.png" style="width: 80px;" class="img-fluid"></a>
                            </div>
                            
                            <div class="payment-img">
                                <?php foreach ($bankLogo as $bank) { ?>
                                <img class="" style="width: 60px;" src="<?php echo base_url('assets/images/bank/' . $bank->bank_image) ?>">
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                        <div class="footer-widget footer-widget-margin-1 footer-list mb-40">
                            <h3 class="footer-title">Information</h3>
                            <ul>
                                <li><a href="<?= base_url('delivery')?>">Delivery Information</a></li>
                                <li><a href="<?= base_url('privacy')?>">Privacy Policy</a></li>
                                <li><a href="<?= base_url('toc')?>">Terms & Conditions</a></li>
                                <li><a href="<?= base_url('link')?>" target="_blank">Legris Link</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-6 col-sm-6 col-12">
                        <div class="footer-widget footer-list mb-40">
                            <h3 class="footer-title">My Account</h3>
                            <ul>
                                <li><a href="<?= base_url('my-account')?>">My Account</a></li>
                                <li><a href="<?= base_url('my-account#orders')?>">Order History</a></li>
                                <li><a href="<?= base_url('wishlist')?>">Wish List</a></li>
                                
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                        <div class="footer-widget footer-widget-margin-2 footer-address mb-40">
                            <h3 class="footer-title">Get in touch</h3>
                            <ul>
                                <li><span>Address: </span><?= $address; ?></li>
                                <li><span>Telephone:</span> (+62) <?= $phone; ?></li>
                                <li><span>Email: </span><?= $email; ?></li>
                            </ul>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-gray-3">
        <div class="container">
            <div class="footer-bottom copyright text-center bg-gray-3">
                <p>Copyright ©<?php echo date('Y');?> All rights reserved Legris Home Furniture.</p>
            </div>
        </div>
    </div>
</footer>