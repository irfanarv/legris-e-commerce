
<header class="header-area header-responsive-padding">
    <div class="header-top d-none d-lg-block bg-gray">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-6">
                    <div class="welcome-text">
                        <p> <?= $pesan; ?></p>
                    </div>
                </div>
                <div class="col-lg-6 col-6">
                    <div class="language-currency-wrap">
                          <div class="language-wrap">
                                <a href="<?= $ig_uri; ?>" target="_blank"> <i class="ti-instagram" style="font-size: 18px;"></i> <?= $ig_title; ?></a>
                            </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-bottom sticky-bar">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-3 col-md-6 col-6">
                    <div class="logo py-3">
                        <a href="<?= base_url(); ?>">
                            <img src="<?= base_url(); ?>assets/images/logo.png" style="width: 70px;" class="img-fluid">
                        </a>
                    </div>
                </div>
                <div class="col-lg-6 d-none d-lg-block">
                    <div class="main-menu text-center">
                        <nav>
                            <ul>
                                <li><a href="<?= base_url(); ?>">HOME</a></li>

                                <li><a href="<?= base_url(); ?>product">PRODUCTS</a>
                                    <ul class="mega-menu-style mega-menu-mrg-1">
                                        <li>
                                            <ul>
                                                <?php foreach ($menus as $menu) { ?>
                                                    <li>
                                                        <a class="dropdown-title" href="javascript:void(0)"><?php echo $menu->name_type; ?></a>
                                                        <ul style="  list-style: none; display: inline-grid; grid-template-rows: repeat(10, auto); grid-auto-flow: column;">
                                                            <?php if (isset($menu->category)) {
                                                                foreach ($menu->category as $child) { ?>
                                                                    <li><a onmouseover="document.getElementById('imgs').setAttribute('src', '<?php echo base_url('assets/images/category/' . $child->image) ?>');" href="<?= base_url('categories/' . $child->slug); ?>"><?php echo $child->name; ?></a></li>
                                                            <?php }
                                                            } ?>
                                                        </ul>
                                                    </li>
                                                <?php } ?>
                                                <li>
                                                    <img id="imgs" src="<?= base_url(); ?>assets/images/menu.jpeg" style="width:200px !important;">
                                                </li>

                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="<?= base_url(); ?>wholesale">WHOLESALE</a></li>
                                <li><a href="<?= base_url(); ?>about-us">ABOUT US</a></li>
                                <li><a href="<?= base_url(); ?>blog">BLOG</a></li>
                                <li><a href="<?= base_url(); ?>contact-us">CONTACT US</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-6">
                    <div class="header-action-wrap">
                        <div class="language-currency-wrap">
                            <?php if ($this->session->userdata('mail')) { ?>
                                <div class="language-wrap">
                                    <a class="language-active" style="margin-top:-15px; margin-right:10px;" href="#"><?php echo $this->session->userdata('name'); ?> </a>
                                    <div class="language-dropdown">
                                        <ul>
                                            <li><a href="<?= base_url('my-account'); ?>">My Account </a></li>
                                            <li><a href="<?= base_url(); ?>logout">Logout</a></li>
                                        </ul>
                                    </div>
                                </div>
                            <?php } else { ?>
                                <a title="Signin or Signup" data-bs-toggle="modal" data-bs-target="#signin"><i class="pe-7s-user" style="font-size: 24px; margin-right:10px;"></i></a>
                            <?php } ?>
                        </div>
                        <div class="header-action-style header-action-wish">
                            <a class="wish-active" href="<?= base_url('wishlist') ?>"><i class="pe-7s-like"></i>
                                <span class="product-count bg-black">
                                    <div id="wish-counter">
                                        <?php
                                        if (!empty($countWish)) {
                                            echo number_format($countWish);
                                        } else {
                                            print 0;
                                        }
                                        ?>
                                    </div>
                                </span>
                            </a>
                        </div>
                        <div class="header-action-style header-action-cart">
                            <a class="cart-active" href="#"><i class="pe-7s-shopbag"></i>
                                <span class="product-count bg-black">
                                    <div id="cart-counter">
                                        <?php
                                        if (!empty($this->cart->contents())) {
                                            echo number_format($this->cart->total_items());
                                        } else {
                                            print 0;
                                        }
                                        ?>
                                    </div>
                                </span>
                            </a>
                        </div>
                        <div class="header-action-style d-block d-lg-none">
                            <a class="mobile-menu-active-button" href="#"><i class="pe-7s-menu"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>