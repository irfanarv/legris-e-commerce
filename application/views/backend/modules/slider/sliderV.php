<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5 class="m-b-10">Sliders</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="#">Sliders</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <div class="float-right">
                <button class="btn btn-success btn-sm btn-round has-ripple" onclick="addSlider()"><i class="feather icon-plus-circle"></i> New Sliders</button>
            </div>
        </div>
        <div class="card-body">
            <div class="dt-responsive table-responsive">
                <table id="table" class="table mb-0 dataTable no-footer ">
                    <thead>
                        <tr>
                            <th>Slider</th>
                            <th>Subtitle</th>
                            <th>Button Action</th>
                            <th>Status</th>
                            <th>Date</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- modals -->
<div class="modal fade" id="modal-slider" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="#" id="form" enctype="multipart/form-data">
                    <input type="hidden" value="" name="id"/>  
                    <input type="hidden" value="" name="gambar">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="floating-label" for="Name">Title</label>
                                <input type="text" name="title" class="form-control" id="title" placeholder=" " required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="floating-label" for="Name">Subtitle</label>
                                <input type="text" name="sub_title" class="form-control" id="sub_title" placeholder=" " required>
                            </div>
                        </div>
                        
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="floating-label" for="Name">Button Title </label>
                                <input type="text" name="btn_title" class="form-control"placeholder=" "  id="btn_title">
                               
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="floating-label" for="Name">Button Url </label>
                                <input type="text" name="btn_url" class="form-control" placeholder=" " id="btn_url">
                                <small id="passwordHelpBlock" class="form-text text-muted"><code class="highlighter-rouge"><?php echo base_url();?></code></small>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group fill">
                                <label class="floating-label" for="Icon">Sliders</label><br>
                                <img class="img-fluid" id="modal-preview" src="https://via.placeholder.com/350"><br><br>
                                <div class="upload-btn-wrapper">
                                    <button class="btn btn-secondary btn-sm">Upload file</button>
                                    <input id="image" type="file" name="image" accept="image/*" onchange="readURL(this);">
                                </div>
                                <input type="hidden" name="hidden_image" id="hidden_image">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <select id="status" name="status" class="form-control" required>
                                <option value="">Status</option>
                                <option value="1">Active</option>
                                <option value="0">Deactived</option>
                            </select>
                        </div>
                        
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="button" class="btn btn-dange" data-dismiss="modal" aria-label="Close">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var table;
    var save_method;
    $(document).ready(function() {
        table = $('#table').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('dashboard/slider_list') ?>",
                "type": "POST"
            },
            "columnDefs": [{
                "targets": [-1],
                "orderable": false,
            }, ],
        });
    });

    function addSlider() {
        save_method = 'add';
        $('#modal-preview').attr('src', 'https://via.placeholder.com/350');
        $('#form')[0].reset();
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();
        $('#modal-slider').modal('show');
        $('.modal-title').text('Add New Sliders');
    }

    function sliderEdit(id)
    {
        save_method = 'update';
        $('#form')[0].reset();
        $('.form-group').removeClass('has-error'); 
        $('.help-block').empty(); 

        
        $.ajax({
            url : "<?php echo site_url('dashboard/edit_slider')?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
                $('[name="id"]').val(data.slider_id);
                $('[name="title"]').val(data.slider_title);
                $('[name="sub_title"]').val(data.slider_subtitle);
                $('[name="btn_title"]').val(data.slider_btn);
                $('[name="btn_url"]').val(data.slider_uri);
                $('[name="gambar"]').val(data.slider_img);
                $('[name="status"]').val(data.status);
                $('#modal-slider').modal('show');
                $('.modal-title').text('Update Slider'); 
                if (data.slider_img) {
                        $('#modal-preview').attr('src', '../assets/images/sliders' + '/' + data.slider_img);
                        $('#hidden_image').attr('src', '../assets/images/sliders' + '/' + data.slider_img);
                    }

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
    }

    function reload_table() {
        table.ajax.reload();
        $('#modal-preview').attr('src', 'https://via.placeholder.com/150');
    }

    function readURL(input, id) {
        id = id || '#modal-preview';
        if (input.files) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $(id).attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
            $('#modal-preview').removeClass('hidden');

        }
    }

    $('#form').submit(function(e) {
        var url;
        if(save_method == 'add') {
            url = "<?php echo site_url('dashboard/slider_add') ?>";
            notify('Slider added successfully', 'inverse');
        } else {
            url = "<?php echo site_url('dashboard/slider_edit') ?>";
            notify('Slider edit successfully', 'inverse');
        }
        e.preventDefault();
        $.ajax({
            "url": url,
            type: "post",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            success: function(data) {                
                $('#form')[0].reset();
                $('#modal-slider').modal('hide');
                reload_table();

            }

        });
    });

    function deleteSlider(id)
    {

        swal({
            title: "Are you sure ?",
            text: "Delete This Slider",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            cancelButtonText: "Cancel",
            confirmButtonText: "Delete",
            closeOnConfirm: false,
            closeOnCancel: false
        },

        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url : "<?php echo site_url('dashboard/slider_delete')?>/" + id,
                    type: "POST",
                    dataType: "JSON",
                    success:function(result){
                        if(result.status == true){
                            reload_table();
                            swal("Success", "Slider has been deleted.", "success");
                        }else{
                            swal("Cancel", "Slider undelete", "error");
                        }
                    },
                    error: function(err){
                        swal("Error", "Slider delete failed", "error");
                    }
                });
            } else {
                swal("Error", "Slider delete failed", "error");
            }
        });
    }


</script>
