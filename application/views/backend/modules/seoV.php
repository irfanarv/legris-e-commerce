<?php if ($this->session->flashdata('msg') == 'sukses') : ?>
    <script>
        $(window).on('load', function() {
            notify('Settings updated successfully', 'inverse');
        });
    </script>
<?php else : ?>
<?php endif; ?>
<?php foreach ($seodata as $p) : ?>
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Legris Settings</h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="feather icon-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="#">Legris Settings</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-12">
        <form action="<?php echo base_url() . 'dashboard/setting_update' ?>" method="post" enctype="multipart/form-data">
            <div class="card">
                <div class="card-header">
                    <div class="float-right">
                        <button type="submit" class="btn btn-success btn-sm btn-round has-ripple"><i class="feather icon-upload-cloud"></i> Update</button>
                    </div>
                </div>
                <div class="card-body">
                    <h5 class="mb-2">General Settings</h5>
                    <hr>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="floating-label">Header Messege</label>
                                <input type="text" class="form-control" id="pesan" name="pesan" required value="<?php echo $p->pesan; ?>">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="floating-label">Instagram Name</label>
                                <input type="text" class="form-control" id="ig_title" name="ig_title" required value="<?php echo $p->ig_title; ?>">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="floating-label">Instagram Uri</label>
                                <input type="text" class="form-control" id="ig_uri" name="ig_uri" required value="<?php echo $p->ig_uri; ?>">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="floating-label">Address</label>
                                <input type="text" class="form-control" id="address" name="address" required value="<?php echo $p->address; ?>">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="floating-label">Email</label>
                                <input type="email" class="form-control" id="email" name="email" required value="<?php echo $p->email; ?>">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="floating-label">Phone</label>
                                <input type="text" class="form-control" id="phone" name="phone" required value="<?php echo $p->phone; ?>">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="floating-label">Wholesale Key</label>
                                <input type="text" class="form-control" id="wholesale" name="wholesale" required value="<?php echo $p->wholesale; ?>">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="floating-label">Maps</label>
                                <input type="text" class="form-control" id="maps" name="maps" required value="<?php echo $p->maps; ?>">
                            </div>
                        </div>
                    </div>
                    <h5 class="mb-2">Meta Settings</h5>
                    <hr>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="floating-label">Meta Name</label>
                                <input type="text" class="form-control" id="name" name="name" required value="<?php echo $p->name; ?>">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="floating-label">Meta Title</label>
                                <input type="text" class="form-control" id="title" name="title" required value="<?php echo $p->title; ?>">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="floating-label">Meta Description</label>
                                <input type="text" class="form-control" id="desc" name="desc" required value="<?php echo $p->desc; ?>">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="floating-label">Meta Keyword</label>
                                <input type="text" class="form-control" id="keyword" name="keyword" required value="<?php echo $p->keyword; ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
<?php endforeach; ?>