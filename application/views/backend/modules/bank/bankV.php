<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5 class="m-b-10">Bank Account</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="#">Bank Account</a></li>
                </ul>
            </div>
        </div>
    </div> 
</div>

<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <div class="float-right">
                <button class="btn btn-success btn-sm btn-round has-ripple" onclick="addBank()"><i class="feather icon-plus-circle"></i> Add New Bank Account</button>
            </div>
        </div>
        <div class="card-body">
            <div class="dt-responsive table-responsive">
                <table id="table" class="table mb-0 dataTable no-footer ">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Account Number</th>
                            <th>Status</th>
                            <th>Date Update</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- modals -->
<div class="modal fade" id="modal-bank" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="#" id="form" enctype="multipart/form-data">
                    <input type="hidden" value="" name="id"/>  
                    <input type="hidden" value="" name="gambar"/>  
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="floating-label" for="Name">Account Name</label>
                                <input type="text" name="bank_name" class="form-control" id="bank_name" placeholder=" " required>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="floating-label" for="Name">Account Number</label>
                                <input type="text" name="bank_no" class="form-control" id="bank_no" placeholder=" " required>
                            </div>
                        </div>
                        <div class="col-sm-12">
                        
                            <select id="status" name="status" class="form-control" required>
                                <option value="">Status</option>
                                <option value="1">Active</option>
                                <option value="0">Deactived</option>
                            </select>
                        </div>

                        <div class="col-sm-12 mt-5">
                            <div class="form-group fill">
                                <label class="floating-label" for="Icon">Bank Logo</label><br>
                                <img class="img-fluid" id="modal-preview" src="https://via.placeholder.com/150"><br><br>
                                <div class="upload-btn-wrapper">
                                    <button class="btn btn-secondary btn-sm">Upload file</button>
                                    <input id="image" type="file" name="image" accept="image/*" onchange="readURL(this);">
                                </div>
                                <input type="hidden" name="hidden_image" id="hidden_image">
                            </div>
                        </div>
                        
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="button" class="btn btn-dange" data-dismiss="modal" aria-label="Close">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var table;
    var save_method;
    $(document).ready(function() {
        table = $('#table').DataTable({ 
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('dashboard/bank_list') ?>",
                "type": "POST"
            },
            "columnDefs": [{
                "targets": [-1],
                "orderable": false,
            }, ],
        });
    });

    function addBank() {
        save_method = 'add';
        $('#modal-preview').attr('src', 'https://via.placeholder.com/150');
        $('#form')[0].reset();
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();
        $('#modal-bank').modal('show');
        $('.modal-title').text('Add New Bank Account');
    }

    $('#form').submit(function(e) {
        var url;

        if(save_method == 'add') {
            url = "<?php echo site_url('dashboard/bank_add') ?>";
            notify('Account added successfully', 'inverse');
        } else {
            url = "<?php echo site_url('dashboard/bank_edit') ?>";
            notify('Account edit successfully', 'inverse');
        }
        e.preventDefault();
        $.ajax({
            "url": url,
            type: "post",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            success: function(data) {                
                $('#form')[0].reset();
                $('#modal-bank').modal('hide');
                reload_table();

            }

        });
    });

    function reload_table() {
        table.ajax.reload();
        $('#modal-preview').attr('src', 'https://via.placeholder.com/150');
    }

    function bankEdit(id)
    {
        save_method = 'update';
        $('#form')[0].reset();
        $('.form-group').removeClass('has-error'); 
        $('.help-block').empty(); 

        
        $.ajax({
            url : "<?php echo site_url('dashboard/edit_bank')?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
                $('[name="id"]').val(data.bank_id);
                $('[name="bank_name"]').val(data.bank_name);
                $('[name="bank_no"]').val(data.bank_no);
                $('[name="gambar"]').val(data.bank_image);
                $('[name="status"]').val(data.bank_status);
                $('#modal-bank').modal('show');
                $('.modal-title').text('Update Bank Account'); 
                if (data.bank_image) {
                        $('#modal-preview').attr('src', '../assets/images/bank' + '/' + data.bank_image);
                        $('#hidden_image').attr('src', '../assets/images/bank' + '/' + data.bank_image);
                    }

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
    }

    function readURL(input, id) {
        id = id || '#modal-preview';
        if (input.files) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $(id).attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
            $('#modal-preview').removeClass('hidden');

        }
    }

    function deleteBank(id)
    {

        swal({
            title: "Are you sure ?",
            text: "Delete Account Bank",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            cancelButtonText: "Cancel",
            confirmButtonText: "Delete",
            closeOnConfirm: false,
            closeOnCancel: false
        },

        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url : "<?php echo site_url('dashboard/bank_delete')?>/" + id,
                    type: "POST",
                    dataType: "JSON",
                    success:function(result){
                        if(result.status == true){
                            reload_table();
                            swal("Success", "Bank Account has been deleted.", "success");
                        }else{
                            swal("Cancel", "Bank Account undelete", "error");
                        }
                    },
                    error: function(err){
                        swal("Error", "Bank Account delete failed", "error");
                    }
                });
            } else {
                swal("Error", "Bank Account delete failed", "error");
            }
        });
    }

</script>