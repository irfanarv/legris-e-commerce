<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5 class="m-b-10">Page Management</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="#">Page Management</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12">
    <div class="card">

        <div class="card-body">
            <div class="dt-responsive table-responsive">
                <table id="table" class="table mb-0 dataTable no-footer ">
                    <thead>
                        <tr>
                            <th>Page Name</th>
                            <th>Title</th>
                            <th>Last Update</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- modals -->
<div class="modal fade" id="modal-page" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="#" id="form" enctype="multipart/form-data">
                    <input type="hidden" value="" name="id"/>  
                    <input type="hidden" value="" name="gambar">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="floating-label" for="Name">Name</label>
                                <input type="text" name="name" class="form-control" id="name" placeholder=" " required>
                            </div>
                        </div>
                        
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="floating-label" for="Name">Title</label>
                                <input type="text" name="title" class="form-control" id="title" placeholder=" " required>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">
                                <textarea name="desc" id="desc" cols="105" rows="10"></textarea>
                            </div>
                        </div>

                        <div class="col-sm-12 mt-5">
                            <div class="form-group fill">
                                <label class="floating-label" for="Icon">Image</label><br>
                                <img class="img-fluid" id="modal-preview" src="https://via.placeholder.com/150"><br><br>
                                <div class="upload-btn-wrapper">
                                    <button class="btn btn-secondary btn-sm">Upload file</button>
                                    <input id="image" type="file" name="image" accept="image/*" onchange="readURL(this);">
                                </div>
                                <input type="hidden" name="hidden_image" id="hidden_image">
                            </div>
                        </div>
                        
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="button" class="btn btn-dange" data-dismiss="modal" aria-label="Close">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var table;
    var save_method;
    $(document).ready(function() {
        table = $('#table').DataTable({ 
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('dashboard/page_list') ?>",
                "type": "POST"
            },
            "columnDefs": [{
                "targets": [-1],
                "orderable": false,
            }, ],
        });
    });

    function addPage() {
        save_method = 'add';
        $('#modal-preview').attr('src', 'https://via.placeholder.com/150');
        $('#form')[0].reset();
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();
        $('#modal-page').modal('show');
        $('.modal-title').text('Add New Page');
    }

    $('#form').submit(function(e) {
        var url;

        if(save_method == 'add') {
            url = "<?php echo site_url('dashboard/page_add') ?>";
            notify('Page added successfully', 'inverse');
        } else {
            url = "<?php echo site_url('dashboard/edit_page') ?>";
            notify('Page edit successfully', 'inverse');
        }
        e.preventDefault();
        $.ajax({
            "url": url,
            type: "post",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            success: function(data) {                
                $('#form')[0].reset();
                $('#modal-page').modal('hide');
                reload_table();

            }

        });
    });

    function reload_table() {
        table.ajax.reload();
        $('#modal-preview').attr('src', 'https://via.placeholder.com/150');
    }

    function pageEdit(id)
    {
        save_method = 'update';
        $('#form')[0].reset();
        $('.form-group').removeClass('has-error'); 
        $('.help-block').empty(); 

        
        $.ajax({
            url : "<?php echo site_url('dashboard/page_edit')?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
                $('[name="id"]').val(data.id_page);
                $('[name="name"]').val(data.name_page);
                $('[name="gambar"]').val(data.image);
                $('[name="title"]').val(data.title_page);
                $('[name="desc"]').val(data.desc_page);
                $('#modal-page').modal('show');
                $('.modal-title').text('Update Page'); 
                if (data.image) {
                        $('#modal-preview').attr('src', '../assets/images/page' + '/' + data.image);
                        $('#hidden_image').attr('src', '../assets/images/page' + '/' + data.image);
                    }

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
    }

    function readURL(input, id) {
        id = id || '#modal-preview';
        if (input.files) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $(id).attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
            $('#modal-preview').removeClass('hidden');

        }
    }

    function deletePage(id)
    {

        swal({
            title: "Are you sure ?",
            text: "Delete Page",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            cancelButtonText: "Cancel",
            confirmButtonText: "Delete",
            closeOnConfirm: false,
            closeOnCancel: false
        },

        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url : "<?php echo site_url('dashboard/page_delete')?>/" + id,
                    type: "POST",
                    dataType: "JSON",
                    success:function(result){
                        if(result.status == true){
                            reload_table();
                            swal("Success", "Page has been deleted.", "success");
                        }else{
                            swal("Cancel", "Page undelete", "error");
                        }
                    },
                    error: function(err){
                        swal("Error", "Page delete failed", "error");
                    }
                });
            } else {
                swal("Error", "Page delete failed", "error");
            }
        });
    }

</script>