<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5 class="m-b-10">Account Management</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="#">Account Management</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <div class="float-right">
                <button class="btn btn-success btn-sm btn-round has-ripple" onclick="addAccount()"><i class="feather icon-plus-circle"></i> Add New Account</button>
            </div>
        </div>
        <div class="card-body">
        <div class="print-error-msg text-danger text-left"></div>
            <div class="dt-responsive table-responsive">
                <table id="table" class="table mb-0 dataTable no-footer ">
                    <thead>
                        <tr>
                            <th>Account Name</th>
                            <th>Email</th>
                            <th>Status</th>
                            <th>Last Update</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- modals -->
<div class="modal fade" id="modal-account" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="#" id="form" enctype="multipart/form-data">
                    <input type="hidden" value="" name="id"/>  
                    <input type="hidden" value="" name="gambar"/>  
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="floating-label" for="Name">Full Name</label>
                                <input type="text" name="name" class="form-control" id="name" placeholder=" " required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="floating-label" for="Name">Email</label>
                                <input type="email" name="email" class="form-control" id="email" placeholder=" " required>
                            </div>
                        </div>
                        <div class="col-sm-12 mb-5">
                            <label class="floating-label" for="Name">Status</label>
                            <select id="status" name="status" class="form-control" placeholder=" " required>
                                <option>Select</option>
                                <option value="yes">Active</option>
                                <option value="no">Deactived</option>
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="floating-label" for="Name">Password</label>
                                <input type="password" name="password" class="form-control" id="password" placeholder=" " required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="floating-label" for="Name">Confirm Password</label>
                                <input type="password" name="confirm_password" class="form-control" id="confirm_password" placeholder=" " required>
                            </div>
                        </div>

                        <div class="col-sm-12 mt-5">
                            <div class="form-group fill">
                                <label class="floating-label" for="Icon">Avatar</label><br>
                                <img class="img-fluid" id="modal-preview" src="https://via.placeholder.com/150"><br><br>
                                <div class="upload-btn-wrapper">
                                    <button class="btn btn-secondary btn-sm">Upload file</button>
                                    <input id="image" type="file" name="image" accept="image/*" onchange="readURL(this);">
                                </div>
                                <input type="hidden" name="hidden_image" id="hidden_image" >
                            </div>
                        </div>
                        
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="button" class="btn btn-dange" data-dismiss="modal" aria-label="Close">Cancel</button>
                        </div>
                        <div class="col-sm-12 mt-3">
                            
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var table;
    var save_method;
    $(document).ready(function() {
        table = $('#table').DataTable({ 
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('dashboard/account_list') ?>",
                "type": "POST"
            },
            "columnDefs": [{
                "targets": [-1],
                "orderable": false,
            }, ],
        });
    });

    function addAccount() {
        save_method = 'add';
        $('#modal-preview').attr('src', 'https://via.placeholder.com/150');
        $('#form')[0].reset();
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();
        $('#modal-account').modal('show');
        $('.modal-title').text('Add New Administrator Account');
    }

    $('#form').submit(function(e) {
        var url;

        if(save_method == 'add') {
            url = "<?php echo site_url('dashboard/account_add') ?>";
            notify('Account edit successfully', 'inverse');
        } else {
            url = "<?php echo site_url('dashboard/edit_account') ?>";
            notify('Account edit successfully', 'inverse');
        }
        e.preventDefault();
        $.ajax({
            "url": url,
            type: "post",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            success: function(data) { 
                $('#form')[0].reset();
                $('#modal-account').modal('hide');
                reload_table();

            }

        });
    });

    function reload_table() {
        table.ajax.reload();
        $('#modal-preview').attr('src', 'https://via.placeholder.com/150');
    }

    function editAccount(id)
    {
        save_method = 'update';
        $('#form')[0].reset();
        $('.form-group').removeClass('has-error'); 
        $('.help-block').empty(); 

        
        $.ajax({
            url : "<?php echo site_url('dashboard/account_edit')?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
                $('[name="id"]').val(data.accounts_id);
                $('[name="name"]').val(data.ai_first_name);
                $('[name="email"]').val(data.accounts_email);
                $('[name="gambar"]').val(data.ai_image);
                $('[name="status"]').val(data.accounts_status);
                $('#modal-account').modal('show');
                $('.modal-title').text('Update Account'); 
                if (data.ai_image) {
                        $('#modal-preview').attr('src', '../../assets/images/profile' + '/' + data.ai_image);
                        $('#hidden_image').attr('src', '../../assets/images/profile' + '/' + data.ai_image);
                    }

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
    }

    function readURL(input, id) {
        id = id || '#modal-preview';
        if (input.files) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $(id).attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
            $('#modal-preview').removeClass('hidden');

        }
    }

    function deleteBank(id)
    {

        swal({
            title: "Are you sure ?",
            text: "Delete Account",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            cancelButtonText: "Cancel",
            confirmButtonText: "Delete",
            closeOnConfirm: false,
            closeOnCancel: false
        },

        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url : "<?php echo site_url('dashboard/account_delete')?>/" + id,
                    type: "POST",
                    dataType: "JSON",
                    success:function(result){
                        if(result.status == true){
                            reload_table();
                            swal("Success", " Account has been deleted.", "success");
                        }else{
                            swal("Cancel", " Account undelete", "error");
                        }
                    },
                    error: function(err){
                        swal("Error", " Account delete failed", "error");
                    }
                });
            } else {
                swal("Error", " Account delete failed", "error");
            }
        });
    }

</script>