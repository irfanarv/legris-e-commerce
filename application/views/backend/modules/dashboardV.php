<?php
error_reporting(0);
foreach ($visitor as $result) {
    $bulan[] = $result->tgl;
    $value[] = (float) $result->jumlah;
}

?>
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5 class="m-b-10">Hai <?php echo $this->session->userdata('fullName'); ?> ,
                        <?php
                        date_default_timezone_set('Asia/Jakarta');
                        $Hour = date('G');
                        if ($Hour >= 00 && $Hour <= 11) {
                            echo "Selamat Pagi";
                        } else if ($Hour >= 12 && $Hour <= 15) {
                            echo "Selamat Siang";
                        } else if ($Hour >= 16 && $Hour <= 18) {
                            echo "Selamat Sore";
                        } else if ($Hour >= 19 || $Hour <= 23) {
                            echo "Selamat Malam";
                        }
                        ?>
                        👋
                    </h5>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <!-- card top -->
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-8">
                        <h4 class="text-c-yellow"><?php echo number_format($productTotal); ?></h4>
                        <h6 class="text-muted m-b-0">Items Product</h6>
                    </div>
                    <div class="col-4 text-right">
                        <i class="feather icon-box f-28"></i>
                    </div>
                </div>
            </div>
            <div class="card-footer bg-c-yellow">
                <div class="row align-items-center">
                    <div class="col-12">
                        <a href="<?= base_url() ?>dashboard/products" class="text-white m-b-0">Views</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-8">
                        <h4 class="text-c-green"><?php echo number_format($categoryTotal); ?></h4>
                        <h6 class="text-muted m-b-0">Category Product</h6>
                    </div>
                    <div class="col-4 text-right">
                        <i class="feather icon-codepen f-28"></i>
                    </div>
                </div>
            </div>
            <div class="card-footer bg-c-green">
                <div class="row align-items-center">
                    <div class="col-9">
                        <a href="<?= base_url() ?>dashboard/category-products" class="text-white m-b-0">Views</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-8">
                        <h4 class="text-c-blue"><?php echo number_format($memberTotal); ?></h4>
                        <h6 class="text-muted m-b-0">Members</h6>
                    </div>
                    <div class="col-4 text-right">
                        <i class="feather icon-user-check f-28"></i>
                    </div>
                </div>
            </div>
            <div class="card-footer bg-c-blue">
                <div class="row align-items-center">
                    <div class="col-12">
                        <a href="<?= base_url() ?>dashboard/member" class="text-white m-b-0">Views</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-8">
                        <h4 class="text-c-red"><?php echo number_format($orderin); ?></h4>
                        <h6 class="text-muted m-b-0">New Order</h6>
                    </div>
                    <div class="col-4 text-right">
                        <i class="feather icon-shopping-cart f-28"></i>
                    </div>
                </div>
            </div>
            <div class="card-footer bg-c-red">
                <div class="row align-items-center">
                    <div class="col-12">
                        <a href="<?= base_url() ?>dashboard/orders" class="text-white m-b-0">Views</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- card top -->
    <!-- ecommerce -->
    <!-- <div class="col-lg-12 col-md-12">
        <div class="card table-card latest-activity-card">
            <div class="card-header">
                <h5>Latest Order</h5>
                <div class="card-header-right">
                    <div class="btn-group card-option">
                        <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="feather icon-more-horizontal"></i>
                        </button>
                        <ul class="list-unstyled card-option dropdown-menu dropdown-menu-right">
                            <li class="dropdown-item full-card"><a href="#!"><span><i class="feather icon-maximize"></i> maximize</span><span style="display:none"><i class="feather icon-minimize"></i> Restore</span></a></li>
                            <li class="dropdown-item minimize-card"><a href="#!"><span><i class="feather icon-minus"></i> collapse</span><span style="display:none"><i class="feather icon-plus"></i> expand</span></a></li>
                            <li class="dropdown-item reload-card"><a href="#!"><i class="feather icon-refresh-cw"></i> reload</a></li>
                            <li class="dropdown-item close-card"><a href="#!"><i class="feather icon-trash"></i> remove</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="card-body p-0">
                <div class="table-responsive">
                    <table class="table table-hover table-borderless mb-0">
                        <thead>
                            <tr>
                                <th>Customer</th>
                                <th>Order ID</th>
                                <th>Photo</th>
                                <th>Product</th>
                                <th>Qty</th>
                                <th>Date</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>John Deo</td>
                                <td>#814123</td>
                                <td><img src="assets/images/widget/PHONE1.jpg" alt="" class="img-fluid"></td>
                                <td>Moto G5</td>
                                <td>10</td>
                                <td>17-2-2019</td>
                                <td><label class="badge badge-light-warning">Pending</label></td>
                                <td><a href="#!"><i class="icon feather icon-edit f-w-600 f-16 m-r-15 text-c-green"></i></a><a href="#!"><i class="feather icon-trash-2 f-w-600 f-16 text-c-red"></i></a></td>
                            </tr>
                            <tr>
                                <td>Jenny William</td>
                                <td>#684898</td>
                                <td><img src="assets/images/widget/PHONE2.jpg" alt="" class="img-fluid"></td>
                                <td>iPhone 8</td>
                                <td>16</td>
                                <td>20-2-2019</td>
                                <td><label class="badge badge-light-primary">Paid</label></td>
                                <td><a href="#!"><i class="icon feather icon-edit f-w-600 f-16 m-r-15 text-c-green"></i></a><a href="#!"><i class="feather icon-trash-2 f-w-600 f-16 text-c-red"></i></a></td>
                            </tr>
                            <tr>
                                <td>Lori Moore</td>
                                <td>#454898</td>
                                <td><img src="assets/images/widget/PHONE3.jpg" alt="" class="img-fluid"></td>
                                <td>Redmi 4</td>
                                <td>20</td>
                                <td>17-2-2019</td>
                                <td><label class="badge badge-light-success">Success</label></td>
                                <td><a href="#!"><i class="icon feather icon-edit f-w-600 f-16 m-r-15 text-c-green"></i></a><a href="#!"><i class="feather icon-trash-2 f-w-600 f-16 text-c-red"></i></a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div> -->
    <!-- Latest Order end -->
    <!-- order  start -->
    <!-- <div class="col-md-12 col-xl-4">
        <div class="card bg-c-yellow order-card">
            <div class="card-body">
                <h6 class="text-white">Revenue</h6>
                <h2 class="text-white">$42,562</h2>
                <i class="card-icon feather icon-filter"></i>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-xl-4">
        <div class="card bg-c-blue order-card">
            <div class="card-body">
                <h6 class="text-white">Orders Received</h6>
                <h2 class="text-white">486</h2>
                <i class="card-icon feather icon-users"></i>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-xl-4">
        <div class="card bg-c-green order-card">
            <div class="card-body">
                <h6 class="text-white">Total Sales</h6>
                <h2 class="text-white">1641</h2>
                <i class="card-icon feather icon-radio"></i>
            </div>
        </div>
    </div> -->

    <!-- visitors -->
    <div class="col-xl-9 col-md-12">
        <div class="card">
            <div class="card-header">
                <h5>Statistic Of Visitors</h5>
            </div>
            <div class="card-body">
                <canvas id="salesChart"></canvas>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-md-12">
        <div class="card bg-c-blue text-white widget-visitor-card">
            <div class="card-body text-center">
                <h2 class="text-white"><?php echo number_format($monthvisit); ?></h2>
                <h6 class="text-white">Visitors of the month</h6>
                <i class="feather icon-file-text"></i>
            </div>
        </div>
        <div class="card bg-c-yellow text-white widget-visitor-card">
            <div class="card-body text-center">
                <h2 class="text-white"><?php echo number_format($lastmonthvisit); ?></h2>
                <h6 class="text-white">Last month visitor</h6>
                <i class="feather icon-award"></i>
            </div>
        </div>
        <div class="card bg-c-green text-white widget-visitor-card">
            <div class="card-body text-center">
                <h2 class="text-white"><?php echo number_format($totalvisits); ?></h2>
                <h6 class="text-white">Total Visitors</h6>
                <i class="feather icon-trending-up"></i>
            </div>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js" integrity="sha512-ju6u+4bPX50JQmgU97YOGAXmRMrD9as4LE05PdC3qycsGQmjGlfm041azyB1VfCXpkpt1i9gqXCT6XuxhBJtKg==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>

<script>
    $(function() {

        var salesChartCanvas = $("#salesChart").get(0).getContext("2d");

        var salesChart = new Chart(salesChartCanvas);

        var salesChartData = {
            labels: <?php echo json_encode($bulan); ?>,

            datasets: [{
                    label: "Date of visits",
                    backgroundColor: "rgba(0,0,0,.05)",
                    fillColor: "rgb(210, 214, 222)",
                    strokeColor: "rgb(210, 214, 222)",
                    pointColor: "rgb(210, 214, 222)",
                    pointStrokeColor: "#c1c7d1",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgb(220,220,220)",
                    data: <?php echo json_encode($bulan); ?>
                },

                {
                    label: "Visitors",
                    backgroundColor: "rgb(75, 192, 192)",
                    fillColor: "rgb(75, 192, 192)",
                    strokeColor: "rgba(60,141,188,0.8)",
                    pointColor: "#FF6384",
                    pointStrokeColor: "rgba(60,141,188,1)",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(60,141,188,1)",
                    data: <?php echo json_encode($value); ?>
                }
            ]
        };


        var salesChart = new Chart(salesChartCanvas, {
            type: "line",
            data: salesChartData,
            responsive: true,
            maintainAspectRatio: true,

        });

    });
</script>
