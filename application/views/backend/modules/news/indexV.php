<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5 class="m-b-10">List News</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="#">News</a></li>
                    <li class="breadcrumb-item"><a href="#">List News</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <div class="float-right">
                <a href="<?= base_url(); ?>dashboard/add-news" class="btn btn-success btn-sm btn-round has-ripple"><i class="feather icon-plus-circle"></i> Add News</a>
            </div>
        </div>
        <div class="card-body">
            <div class="dt-responsive table-responsive">
                <table id="table" class="table mb-0 dataTable no-footer ">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Category</th>
                            <th>Add By</th>
                            <th>Status</th>
                            <th>Date</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    var table;
    var save_method;
    $(document).ready(function() {
        table = $('#table').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('dashboard/news_list') ?>",
                "type": "POST"
            },
            "columnDefs": [{
                "targets": [-1],
                "orderable": false,
            }, ],
        });
    });

   

    function reload_table() {
        table.ajax.reload();
    }
    function deletePost(id)
    {

        swal({
            title: "Are you sure ?",
            text: "Delete this post ",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            cancelButtonText: "Cancel",
            confirmButtonText: "Delete",
            closeOnConfirm: false,
            closeOnCancel: false
        },

        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url : "<?php echo site_url('dashboard/post_delete')?>/" + id,
                    type: "POST",
                    dataType: "JSON",
                    success:function(result){
                        if(result.status == true){
                            reload_table();
                            swal("Success", "Post has been deleted.", "success");
                        }else{
                            swal("Cancel", "Post undelete", "error");
                        }
                    },
                    error: function(err){
                        swal("Error", "Post delete failed", "error");
                    }
                });
            } else {
                swal("Error", "Post delete failed", "error");
            }
        });
    }

</script>