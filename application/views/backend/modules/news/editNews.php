
<?php foreach ($newsdata as $p) : ?>
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5 class="m-b-10">Edit News <?= $p->post_title;?></h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="#">News</a></li>
                    <li class="breadcrumb-item"><a href="#">Edit News <?= $p->post_title;?></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<?php endforeach;?>
<form action="#" id="form" enctype="multipart/form-data">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <div class="float-left">
                    <ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
                        <!-- <li class="nav-item">
                            <a class="nav-link active text-uppercase" id="news-tab" data-toggle="tab" href="#news" role="tab" aria-controls="news" aria-selected="true">Post News</a>
                        </li> -->
                        <!-- <li class="nav-item">
                            <a class="nav-link text-uppercase" id="translate-tab" data-toggle="tab" href="#translate" role="tab" aria-controls="translate" aria-selected="false">Translate</a>
                        </li> -->
                    </ul>
                </div>
                <div class="float-right">
                    <button type="submit" class="btn btn-success btn-sm btn-round has-ripple"><i class="feather icon-upload-cloud"></i> Update News</button>
                </div>
            </div>
            <div class="card-body">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="news" role="tabpanel" aria-labelledby="news-tab">
                        <div class="row">
                            <div class="col-sm-7">
                                <div class="form-group">
                                    <label class="floating-label">Title</label>
                                    <input type="hidden" value="<?php echo $p->post_id; ?>" name="id" />
                                    <input type="hidden" value="<?php echo $p->post_img; ?>" name="gambar">
                                    <input type="text" id="title" name="title" class="form-control" value="<?= $p->post_title;?>" required>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="form-group"> 
                                <select id="status" name="status" class="form-control" required>
                                    <option value="<?php echo $p->post_status; ?>" <?php  {echo 'selected="selected"'; } ?>>
                                    <?php
                                    if ($p->post_status == "1") {
                                        echo "Active";
                                    }else{
                                        echo "Deactived";
                                    } ?>
                                    </option>
                                    <option value="<?php if ($p->post_status == "1" ){ echo "0"; }else{echo "1"; } ?>"><?php if ($p->post_status == "1" ){ echo "Deactived"; }else{ echo "Active"; } ?></option>
                                </select>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <textarea name="content" id="content" rows="8" cols="80"><?= $p->post_desc;?></textarea>
                                <br>
                            </div>
                            <!-- <div class="col-sm-5">
                                <label class="floating-label">Category</label>
                                <div class="form-group">
                                    <select multiple data-role="tagsinput">
                                        
                                        
                                    </select>
                                </div>
                            </div> -->
                            
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <select id="category" name="category" class="form-control" required>
                                        

                                        <option value="<?php echo $p->cat_id; ?>"  <?php if (in_array($p->cat_id, $category)) { echo "selected"; } ?> ><?php echo $p->cat_name; ?></option>
                                        
                                            <?php foreach ($category as $catd) : ?>
                                                <option value="<?php echo $catd->cat_id; ?>" <?php if (in_array($catd->cat_id, $category)) {echo 'selected="selected"'; } ?>><?php echo $catd->cat_name; ?></option>
                                            <?php endforeach;  ?>
                                        
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="floating-label">Meta Keyword</label>
                                    <input type="text" id="meta_key" name="meta_key" class="form-control" value="<?= $p->post_meta_keywords;?>" required>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="floating-label">Meta Desc</label>
                                    <input type="text" id="meta_desc" name="meta_desc" class="form-control" value="<?= $p->post_meta_desc;?>" required>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group fill">
                                    <label class="floating-label" for="Icon">Thumbnails</label><br>
                                    <?php 
                                        if ($p->post_img == NULL){
                                            echo '<img class="img-fluid" id="modal-preview" src="https://via.placeholder.com/250"><br><br>';
                                        }else{
                                        echo '<img id="modal-preview" src="'.base_url().'assets/images/news/'.$p->post_img.'"  class="rounded mr-3"/><br><br>';
                                        }
                                        ?>
                                    <div class="upload-btn-wrapper">
                                        <button class="btn btn-secondary btn-sm">Upload file</button>
                                        <input id="image" type="file" name="image" accept="image/*" onchange="readURL(this);">
                                    </div>
                                    <input type="hidden" name="hidden_image" id="hidden_image">
                                </div>
                            </div>

                        </div>
                        

                    </div>
                    <!-- <div class="tab-pane fade" id="translate" role="tabpanel" aria-labelledby="translate-tab">
                        <p class="mb-0">Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four
                            loko
                            farm-to-table
                            craft beer twee. Qui photo booth letterpress, commodo enim craft beer mlkshk aliquip jean shorts ullamco ad vinyl cillum PBR. accusamus tattooed echo park.</p>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</form>

<script>
    $(window).on('load', function() {
        CKEDITOR.replace( 'content' );
    
    });
    function readURL(input, id) {
        id = id || '#modal-preview';
        if (input.files) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $(id).attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
            $('#modal-preview').removeClass('hidden');

        }
    } 

    $('#form').submit(function(e) {
        for (instance in CKEDITOR.instances) {
        CKEDITOR.instances[instance].updateElement();
        }
        var formData = new FormData($('#form')[0]);
        var url;
        url = "<?php echo site_url('dashboard/news_update') ?>";
        e.preventDefault();

        $.ajax({
            "url": url,
            type: "post",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            success: function(data) {                
                var redirect;
                redirect = "<?php echo site_url('dashboard/news') ?>";
                location.href = redirect;
                notify('News updated successfully', 'inverse');
            }

        });
    });
</script>
