<style>
    .dropzone {
        border: 2px dashed #0087F7;
    }
</style>
<?php foreach ($dataproduct as $p) : ?>
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        
                        <h5 class="m-b-10">Gallery Product <?php echo $p->product_name; ?></h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="feather icon-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>dashboard/products">Products</a></li>
                        <li class="breadcrumb-item"><a href="#"><?php echo $p->product_name; ?></a></li>
                        <li class="breadcrumb-item"><a href="#">Gallery Product <?php echo $p->product_name; ?></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5>Upload Photo Product</h5>
            </div>
            <div class="card-body">
                <div class="dropzone" id="dropzoneFrom">

                    <div class="dz-message">
                        <h5> Click or Drag and Drop Image here</h5>
                    </div>

                </div>
            </div>
        </div>
        <div class="card"> 

            <div class="card-body gallery-masonry" id="detail_gallery">
                <div class="card-columns">
                    <?php foreach ($datagallery as $p) : ?>
                        <div class="card">
                            <img class="img-fluid card-img-top" src="<?php echo base_url("assets/"); ?>images/gallery/<?php echo $p->gallery_name; ?>">
                            <div class="card-body">
                                <a data-toggle="tooltip" data-placement="top" title="Delete" href="javascript:void(0)" onclick="deletePhoto(<?php echo $p->gallery_id ?>)" class="btn btn-icon btn-outline-danger float-right mb-3"><i class="feather icon-trash-2"></i></a>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>

<?php endforeach; ?>
<script type="text/javascript">
    Dropzone.autoDiscover = false;
    $(document).ready(function() {
        var id = "<?php echo $this->uri->segment('3'); ?>"
        var id_produt = +id;
        var token = Math.random();
        var foto_upload = new Dropzone(".dropzone", {
            url: "<?php echo site_url('dashboard/products_upload') ?>",
            maxFilesize: 5,
            method: "post",
            acceptedFiles: "image/*",
            // autoProcessQueue: false,
            paramName: "userfile",
            dictInvalidFileType: "Type file ini tidak dizinkan",
            addRemoveLinks: true,
        });

        foto_upload.on("sending", function(a, b, c) {
            b.idproduct = id_produt;
            c.append("id", b.idproduct);
            a.token = Math.random();
            c.append("token_foto", a.token)
        });

        // foto_upload.on("removedfile", function(a) {
        //     var token = a.token;
        //     $.ajax({
        //         method: "POST",
        //         data: {
        //             token: token
        //         },
        //         url: "<?php echo site_url('backend/products/Product/removegallery') ?>",
        //         cache: false,
        //         dataType: 'json',
        //         success: function() {
        //             console.log("Foto terhapus");
        //         },
        //         error: function(xhr, ajaxOptions, thrownError) {
        //             console.log(xhr.status);
        //             console.log(thrownError);
        //             console.log(ajaxOptions);

        //         }
        //     });
        // });

        foto_upload.on("complete", function(file) {
            foto_upload.removeFile(file);
            $("#detail_gallery").load(window.location.href + " #detail_gallery");
            notify('Photo Success added', 'inverse');
        });

       
    });

        
    

    function deletePhoto(id) {

        swal({
                title: "Are you sure ?",
                text: "Delete this photo",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                cancelButtonText: "Cancel",
                confirmButtonText: "Delete",
                closeOnConfirm: false,
                closeOnCancel: false
            },

            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: "<?php echo site_url('dashboard/photo_remove') ?>/" + id,
                        type: "POST",
                        dataType: "JSON",
                        success: function(result) {
                            if (result.status == true) {
                                $("#detail_gallery").load(window.location.href + " #detail_gallery");
                                swal("Success", "Photo has been deleted.", "success");
                            } else {
                                swal("Cancel", "Photo undelete", "error");
                                $("#detail_gallery").load(window.location.href + " #detail_gallery");
                            }
                        },
                        error: function(err) {
                            swal("Error", "Photo delete failed", "error");
                        }

                    });
                } else {
                    swal("Error", "Photo delete failed", "error");
                }
            });
    }
</script>
