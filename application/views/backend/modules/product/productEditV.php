<?php foreach ($productdata as $p) : ?>
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Edit Product <?php echo $p->product_name; ?></h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="feather icon-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="#">Product</a></li>
                        <li class="breadcrumb-item"><a href="#">Edit Product <?php echo $p->product_name; ?></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-12">
        <form action="<?php echo base_url() . 'dashboard/products_edit' ?>" method="post" enctype="multipart/form-data">
            <input type="hidden" value="<?php echo $p->product_id; ?>" name="id" />
            <input type="hidden" value="<?php echo $p->product_image; ?>" name="gambar">
            <div class="card">
                <div class="card-header">
                    <div class="float-right">
                        <button type="submit" class="btn btn-success btn-sm btn-round has-ripple"><i class="feather icon-upload-cloud"></i> Save</button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="floating-label">Product Name</label>
                                <input type="text" class="form-control" id="name" name="name" required value="<?php echo $p->product_name; ?>">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <select id="featured" name="featured" class="form-control" required>
                                    <option value="<?php echo $p->product_home; ?>" <?php  {echo 'selected="selected"'; } ?>>
                                    <?php
                                    if ($p->product_home == "1") {
                                        echo "Yes";
                                    }else{
                                        echo "No";
                                    } ?>
                                    </option>
                                    <option value="<?php if ($p->product_home == "1" ){ echo "0"; }else{echo "1"; } ?>"><?php if ($p->product_home == "1" ){ echo "No"; }else{ echo "Yes"; } ?></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <select id="status" name="status" class="form-control" required>
                                    <option value="<?php echo $p->product_status; ?>" <?php  {echo 'selected="selected"'; } ?>>
                                    <?php
                                    if ($p->product_status == "1") {
                                        echo "Active";
                                    }else{
                                        echo "Deactived";
                                    } ?>
                                    </option>
                                    <option value="<?php if ($p->product_status == "1" ){ echo "0"; }else{echo "1"; } ?>"><?php if ($p->product_status == "1" ){ echo "Deactived"; }else{ echo "Active"; } ?></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <label class="floating-label">Description</label>
                            <textarea name="desc" id="desc"><?php echo $p->product_desc;?></textarea>
                            <br>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <select id="category" name="category" class="form-control" required>
                                <option value="<?php if ($p->product_cat == $p->id ){ echo $p->id; }?>"><?php if ($p->product_cat == $p->id ){ echo $p->name; } ?></option>
                                    <?php foreach ($category as $cat) : ?>
                                        <option value="<?php echo $cat->id; ?>" <?php if (in_array($cat->id, $category)) {echo 'selected="selected"'; } ?>><?php echo $cat->name; ?></option>
                                    <?php endforeach;  ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="floating-label">Quantity</label>
                                <input type="number" class="form-control" id="qty" name="qty" value="<?php echo $p->product_qty?>" required>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="floating-label">Price</label>
                                <input type="number" class="form-control" id="price" value="<?php echo $p->product_price?>" name="price" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="floating-label">Meta Title</label>
                                <input type="text" class="form-control" id="metatitle" value="<?php echo $p->seo_title?>" name="metatitle" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="floating-label">Meta Description</label>
                                <input type="text" class="form-control" id="metadesc" value="<?php echo $p->seo_desc?>" name="metadesc" required>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group fill">
                                <label class="floating-label" for="Icon">Thumbnails</label><br>
                                <?php 
                                if ($p->product_image == NULL){
                                    echo '<img class="img-fluid" id="modal-preview" src="https://via.placeholder.com/250"><br><br>';
                                }else{
                                   echo '<img id="modal-preview" src="'.base_url().'assets/images/product/'.$p->product_image.'"  class="rounded mr-3"/><br><br>';
                                }
                                ?>
                                
                                <div class="upload-btn-wrapper">
                                    <button class="btn btn-secondary btn-sm">Upload file</button>
                                    <input id="image" type="file" name="image" accept="image/*" onchange="readURL(this);">
                                </div>
                                <input type="hidden" name="hidden_image" id="hidden_image">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
<?php endforeach; ?>

<script>
    $(window).on('load', function() {
        CKEDITOR.replace( 'desc' );
    
    });
    function readURL(input, id) {
        id = id || '#modal-preview';
        if (input.files) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $(id).attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
            $('#modal-preview').removeClass('hidden');

        }
    }
</script>