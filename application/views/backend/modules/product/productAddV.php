<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5 class="m-b-10">Add New Product</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="#">Product</a></li>
                    <li class="breadcrumb-item"><a href="#">Add New Product</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12">
    <form action="#" id="form" enctype="multipart/form-data">
        <input type="hidden" value="" name="id"/>  
        <input type="hidden" value="" name="gambar">
        <div class="card">
            <div class="card-header">
                <div class="float-right">
                    <button type="submit" class="btn btn-success btn-sm btn-round has-ripple"><i class="feather icon-upload-cloud"></i> Save</button>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="floating-label">Product Name</label>
                            <input type="text" class="form-control" id="name" name="name" required>
                        </div>
                    </div>
                    <!-- <div class="col-sm-4">
                        <div class="form-group">
                            <label class="floating-label">Product SKU</label>
                            <input type="text" class="form-control" id="sku" name="sku">
                        </div>
                    </div> -->
                    <div class="col-sm-4">
                        <div class="form-group">
                            <select id="status" name="status" class="form-control" required>
                                <option value="">Status</option>
                                <option value="1">Active</option>
                                <option value="0">Deactived</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <select id="featured" name="featured" class="form-control" required>
                                <option value="">Set as featured</option>
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <label class="floating-label">Description</label>
                        <textarea name="desc" id="desc"></textarea>
                        <br>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <select id="category" name="category" class="form-control" required>
                                <option value="">Category</option>
                                <?php 
                                    foreach ($category as $p) :
                                ?>
                                <option value="<?php echo $p->id; ?>"  <?php if (in_array($p->id, $category)) { echo 'selected="selected"'; } ?> ><?php echo $p->name; ?></option>
                                <?php endforeach;  ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="floating-label">Quantity</label>
                            <input type="number" class="form-control" id="qty" name="qty" required>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="floating-label">Price</label>
                            <input type="number" class="form-control" id="price" name="price" required>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="floating-label">Meta Title</label>
                            <input type="text" class="form-control" id="metatitle" name="metatitle" required>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="floating-label">Meta Description</label>
                            <input type="text" class="form-control" id="metadesc" name="metadesc" required>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group fill">
                            <label class="floating-label" for="Icon">Thumbnails</label><br>
                            <img class="img-fluid" id="modal-preview" src="https://via.placeholder.com/250"><br><br>
                            <div class="upload-btn-wrapper">
                                <button class="btn btn-secondary btn-sm">Upload file</button>
                                <input id="image" type="file" name="image" accept="image/*" onchange="readURL(this);" required>
                            </div>
                            <input type="hidden" name="hidden_image" id="hidden_image">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">

    $('#form').submit(function(e) {
        var url;
        url = "<?php echo site_url('dashboard/products_add') ?>";
        e.preventDefault();
        for (instance in CKEDITOR.instances) {
        CKEDITOR.instances[instance].updateElement();
        }
        $.ajax({
            "url": url,
            type: "post",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            success: function(data) {
                    if($.isEmptyObject(data.error)){
                        var redirect;
                        redirect = "<?php echo site_url('dashboard/products') ?>";
                        location.href = redirect;
                        notify('Product added successfully', 'inverse');
                    }else{
                        notify('inverse').html(data.error);
                    }

                }
        });
    });

    function readURL(input, id) {
        id = id || '#modal-preview';
        if (input.files) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $(id).attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
            $('#modal-preview').removeClass('hidden');

        }
    }

    $(window).on('load', function() {
        CKEDITOR.replace( 'desc' );
    
    });

</script>