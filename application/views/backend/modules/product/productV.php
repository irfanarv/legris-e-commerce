<style>
    .dropzone {
        border: 2px dashed #0087F7; 
    }
</style>
<?php if ($this->session->flashdata('msg') == 'sukses') : ?>
    <script>
        $(window).on('load', function() {
            notify('Product edit successfully', 'inverse');
        });
    </script>
<?php elseif ($this->session->flashdata('msg') == 'gagal') : ?>
    <script>
        $(window).on('load', function() {
            notify('Product edit failed', 'inverse');
        });
    </script>
<?php else : ?>
<?php endif; ?>
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5 class="m-b-10">Product List</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="#">Product</a></li>
                    <li class="breadcrumb-item"><a href="#">Product List</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <div class="float-right">
                <a href="<?= base_url(); ?>dashboard/add-products" class="btn btn-success btn-sm btn-round has-ripple"><i class="feather icon-plus-circle"></i> New Product</a>
            </div>
        </div>
        <div class="card-body">
            <div class="dt-responsive table-responsive">
                <table id="table" class="table mb-0 dataTable no-footer ">
                    <thead>
                        <tr>
                            <th>Product Name</th>
                            <th>Category</th>
                            <th>Price</th>
                            <th>Qty</th>
                            <th>Featured</th>
                            <th>Status</th>
                            <th>Add By</th>
                            <th>Updated</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<!-- modals -->
<div class="modal fade" id="modal-photo" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button id="newphoto" class="btn btn-primary btn-sm float-right">Add Photo Product</button>
                <button id="tutup" class="btn btn-primary btn-sm float-right" style="display: none;">Hide Form Upload</button>
            </div>
            <div class="modal-body">

                <div class="dropzone" id="dropzoneFrom" style="display:none">

                    <div class="dz-message">
                        <h5> Click or Drag and Drop Image here</h5>
                    </div>

                </div>
                <br><br>
                <div id="preview"></div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var table;
    function reload_table() {
        table.ajax.reload();
    }
    Dropzone.autoDiscover = false;
    $(document).ready(function() {

        $("#newphoto").click(function() {
            $("#dropzoneFrom").show();
            $("#tutup").show();
            $("#newphoto").hide();


        });

        $("#tutup").click(function() {
            $("#dropzoneFrom").hide();
            $("#tutup").hide();
            $('#newphoto').show();
        });

        table = $('#table').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('dashboard/products_list') ?>",
                "type": "POST"
            },
            "columnDefs": [{
                "targets": [-1],
                "orderable": false,
            }, ],
        });
    });

    function uploadProduct(id) {
        $('#modal-photo').modal('show');
        $('.modal-title').text('Gallery Product');
        var id_produt = +id;
        var token = Math.random();
        var foto_upload = new Dropzone(".dropzone", {
            url: "<?php echo site_url('dashboard/products_upload') ?>",
            maxFilesize: 5,
            method: "post",
            acceptedFiles: "image/*",
            // autoProcessQueue: false,
            paramName: "userfile",
            dictInvalidFileType: "Type file ini tidak dizinkan",
            addRemoveLinks: true,
        });

        foto_upload.on("sending", function(a, b, c) {
            b.idproduct = id_produt;
            c.append("id", b.idproduct);
            a.token = Math.random();
            c.append("token_foto", a.token)
        });

        foto_upload.on("removedfile", function(a) {
            var token = a.token;
            $.ajax({
                method: "POST",
                data: {
                    token: token
                },
                url: "<?php echo site_url('dashboard/products_remove') ?>",
                cache: false,
                dataType: 'json',
                success: function() {
                    console.log("Foto terhapus");
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(thrownError);
                    console.log(ajaxOptions);

                }
            });
        });

        // $("#modal-photo").on('hidden.bs.modal', function(e) {
        //     html(foto_upload);

        // });

        foto_upload.on("complete", function(file) {
            foto_upload.removeFile(file);
            $("#dropzoneFrom").fadeOut();
            $("#tutup").fadeOut();
            $('#newphoto').show();
        });
    }

    function deleteProduct(id)
    {

        swal({
            title: "Are you sure ?",
            text: "Delete this Product",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            cancelButtonText: "Cancel",
            confirmButtonText: "Delete",
            closeOnConfirm: false,
            closeOnCancel: false
        },

        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url : "<?php echo site_url('dashboard/products_delete')?>/" + id,
                    type: "POST",
                    dataType: "JSON",
                    success:function(result){
                        if(result.status == true){
                            reload_table();
                            swal("Success", "Product has been deleted.", "success");
                        }else{
                            swal("Cancel", "Product undelete", "error");
                        }
                    },
                    error: function(err){
                        swal("Error", "Cancel", "error");
                    }
                });
            } else {
                swal("Error", "Cancel", "error");
            }
        });
    }

    function voucher() {
        $('#form')[0].reset();
        $('.form-group').removeClass('has-error'); 
        $('.help-block').empty(); 
        $.ajax({
            url : "<?php echo site_url('dashboard/edit_cat')?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
                $('[name="id"]').val(data.id);
                $('[name="name"]').val(data.name);
                $('[name="type"]').val(data.type_id);
                $('[name="gambar"]').val(data.image);
                $('[name="status"]').val(data.status);
                $('[name="home_category"]').val(data.category_home);
                $('[name="category_featured"]').val(data.category_featured);
                $('#modal-category').modal('show');
                $('.modal-title').text('Update Category Product'); 
                if (data.image) {
                        $('#modal-preview').attr('src', '../assets/images/category' + '/' + data.image);
                        $('#hidden_image').attr('src', '../assets/images/category' + '/' + data.image);
                    }

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
    }
</script>