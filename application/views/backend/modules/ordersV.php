<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5 class="m-b-10">Orders</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="#">Orders</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12">
    <div class="card">

        <div class="card-body">
            <div class="dt-responsive table-responsive">
                <table id="table" class="table mb-0 dataTable no-footer ">
                    <thead>
                        <tr>
                            <th>Invoice No</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>City</th>
                            <th>Status</th>
                            <th>Approved By</th>
                            <th>Order Date</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- modals -->
<div class="modal fade" id="modal-type" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="#" id="form" enctype="multipart/form-data">
                    <input type="hidden" value="" name="id"/>  
                    <input type="hidden" name="user_name" value="">
                    <input type="hidden" name="user_mail" value="">
                    <input type="hidden" name="user_phone" value="">
                    <input type="hidden" name="user_address" value="">
                    <input type="hidden" name="user_city" value="">
                    <input type="hidden" name="user_state" value="">
                    <input type="hidden" name="user_zip" value="">
                    <input type="hidden" name="total" value="">
                    <input type="hidden" name="bank_name" value="">
                    <input type="hidden" name="bank_no" value="">   


                    <div class="row">

                        <div class="form-group fill col-sm-12">
                            <label for="Name">Update Status Order</label>
                            <select id="status" name="status" class="form-control" required>
                                <option value="Selesai">Paid</option> 
                                <option value="Di Tolak">Reject</option>
                            </select>
                        </div>
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="button" class="btn btn-dange" data-dismiss="modal" aria-label="Close">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var table;
    var save_method;
    $(document).ready(function() {
        table = $('#table').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('dashboard/order_list') ?>",
                "type": "POST"
            },
            "columnDefs": [{
                "targets": [-1],
                "orderable": false,
            }, ],
        });
    });


    $('#form').submit(function(e) {
        var url;
        url = "<?php echo site_url('dashboard/updateorder') ?>";
        notify('Update Status successfully', 'inverse');
        e.preventDefault();
        $.ajax({
            "url": url,
            type: "post",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            success: function(data) {                
                $('#form')[0].reset();
                $('#modal-type').modal('hide');
                reload_table();

            }

        });
    });

    function reload_table() {
        table.ajax.reload();
    }

    function updateStatus(id)
    {
        $('#form')[0].reset();
        $('.form-group').removeClass('has-error'); 
        $('.help-block').empty(); 

        
        $.ajax({
            url : "<?php echo site_url('dashboard/get_order')?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
                $('[name="id"]').val(data.no_invoice);
                $('[name="status"]').val(data.status_order);

                $('[name="user_mail"]').val(data.user_mail);
                $('[name="user_name"]').val(data.user_name);
                $('[name="user_phone"]').val(data.user_phone);
                $('[name="user_address"]').val(data.user_address);
                $('[name="user_city"]').val(data.user_city);
                $('[name="user_state"]').val(data.user_state);
                $('[name="user_zip"]').val(data.user_zip);
                $('[name="total"]').val(data.total);
                $('[name="bank_name"]').val(data.bank_name);
                $('[name="bank_no"]').val(data.bank_no);

                $('#modal-type').modal('show');
                $('.modal-title').text('Update Status Order'); 
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
    }


</script>