# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.4.14-MariaDB)
# Database: legris
# Generation Time: 2021-06-20 06:05:31 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table accounts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `accounts`;

CREATE TABLE `accounts` (
  `accounts_id` int(11) NOT NULL AUTO_INCREMENT,
  `ai_title` varchar(5) DEFAULT NULL,
  `ai_first_name` varchar(50) DEFAULT NULL,
  `ai_last_name` varchar(50) DEFAULT NULL,
  `accounts_email` varchar(255) NOT NULL,
  `accounts_password` varchar(50) NOT NULL,
  `ai_image` varchar(35) DEFAULT NULL,
  `accounts_type` enum('webadmin','admin','customers','guest') NOT NULL,
  `accounts_is_admin` tinyint(4) NOT NULL DEFAULT 0,
  `accounts_created_at` datetime DEFAULT NULL,
  `accounts_updated_at` datetime DEFAULT NULL,
  `accounts_status` enum('yes','no') NOT NULL DEFAULT 'yes',
  `accounts_verified` tinyint(4) DEFAULT 1,
  `accounts_last_login` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`accounts_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `accounts` WRITE;
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;

INSERT INTO `accounts` (`accounts_id`, `ai_title`, `ai_first_name`, `ai_last_name`, `accounts_email`, `accounts_password`, `ai_image`, `accounts_type`, `accounts_is_admin`, `accounts_created_at`, `accounts_updated_at`, `accounts_status`, `accounts_verified`, `accounts_last_login`)
VALUES
	(1,'','Irfan','Arifin','admin@legris.com','8cb2237d0679ca88db6464eac60da96345513964','','webadmin',1,'1901-02-16 11:41:04','2019-08-08 19:15:14','yes',1,1624169024);

/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `category`;

CREATE TABLE `category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type_id` int(11) DEFAULT NULL,
  `name` varchar(254) DEFAULT NULL,
  `slug` varchar(254) DEFAULT NULL,
  `image` varchar(254) DEFAULT NULL,
  `status` enum('0','1') DEFAULT '1',
  `updatedAt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;

INSERT INTO `category` (`id`, `type_id`, `name`, `slug`, `image`, `status`, `updatedAt`)
VALUES
	(9,3,'Kursi','kursi','8f68f4fa8a06dfb6168d4064e22bd0eb.png','1','2021-06-15 14:06:12'),
	(11,3,'Meja','meja','f55baaa8c96ea61e87c7f9e4645b583a.png','1','2021-06-15 15:33:34');

/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table gallery
# ------------------------------------------------------------

DROP TABLE IF EXISTS `gallery`;

CREATE TABLE `gallery` (
  `gallery_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `gallery_name` varchar(100) DEFAULT NULL,
  `gallery_token` varchar(100) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`gallery_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `gallery` WRITE;
/*!40000 ALTER TABLE `gallery` DISABLE KEYS */;

INSERT INTO `gallery` (`gallery_id`, `gallery_name`, `gallery_token`, `product_id`)
VALUES
	(62,'14c7c0a6337e5e2ceda15c7662025d4d.png','0.6557801701214458',5),
	(63,'5f03062fe7007b1b53a707fa1954bee6.png','0.3488377640130882',5),
	(64,'0c1e7cf10f2e308051535efcebc65140.png','0.8804476409152386',5),
	(65,'194e928e11328ae58245ee9befc47c7a.png','0.168444702036195',5),
	(66,'35df367b769f1d4d60abab2b2f1e3663.png','0.876310010527124',5),
	(67,'2f957a3bababe7b5b4c21b5f748b758e.png','0.06300376142381015',5),
	(68,'c78d65eebc1f6fc54c4229ac7da77b4c.png','0.18203650364978596',5),
	(69,'97a649dfefc90f7f037ceb3bd72cea0c.png','0.01715938476715495',6),
	(70,'df81e464d0f8e3a32920628634402a23.png','0.2624759197987603',6),
	(71,'f512967edb09aec3cd131e62e0a413dd.png','0.2256534688956351',6),
	(72,'cfdac1515758c72edb746942eaf58a19.png','0.043526068511562155',6),
	(73,'0db8ec2e248eb67a8425991865bf6983.png','0.5394886791559577',6),
	(74,'208a7542441e6d6560d7480885ec6b9c.png','0.5546201434009961',6);

/*!40000 ALTER TABLE `gallery` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table product
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product`;

CREATE TABLE `product` (
  `product_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_name` varchar(254) DEFAULT NULL,
  `product_image` varchar(254) DEFAULT NULL,
  `product_cat` int(11) DEFAULT NULL,
  `product_slug` varchar(254) DEFAULT NULL,
  `product_qty` int(11) DEFAULT NULL,
  `product_price` int(11) DEFAULT NULL,
  `product_desc` longtext DEFAULT NULL,
  `product_sku` varchar(254) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `product_status` enum('1','0') DEFAULT '1',
  `product_date` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `seo_title` varchar(254) DEFAULT NULL,
  `seo_desc` varchar(254) DEFAULT NULL,
  `product_home` enum('1','0') DEFAULT '0',
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;

INSERT INTO `product` (`product_id`, `product_name`, `product_image`, `product_cat`, `product_slug`, `product_qty`, `product_price`, `product_desc`, `product_sku`, `user_id`, `product_status`, `product_date`, `seo_title`, `seo_desc`, `product_home`)
VALUES
	(5,'Kursi Minimalis','9ddd00ea2deffab72281069064052321.png',9,'kursi-minimalis',5,2500000,'','SKU001',1,'1','2021-06-19 08:43:18','Kursi minimalis','kursi minimalis ','1'),
	(6,'Meja Dapur','1bb67528a2e6a18d34ec1a791a5d8e08.png',11,'meja-dapur',5,1550000,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse dui est, placerat gravida magna in, egestas vehicula arcu. Nam tempus tincidunt tellus, at blandit ex ultricies nec. Suspendisse facilisis iaculis sapien, quis sodales ante ultrices vel. Nunc tincidunt quis nibh ut bibendum. Cras dictum rhoncus ex, vel luctus urna finibus id. Mauris ut urna sagittis, fringilla odio vitae, pellentesque eros. Donec lacus neque, dictum ac orci et, fringilla tristique erat. Aenean tristique, mauris ac mollis luctus, massa ex pretium nibh, vel luctus magna augue congue neque. Suspendisse suscipit, metus nec semper luctus, arcu nibh vehicula ipsum, id laoreet turpis lacus at nisi. Aliquam erat volutpat.</p>','SKU002',1,'1','2021-06-20 11:44:48','Meja dapur','Meja dapur minimalis','1');

/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table slider
# ------------------------------------------------------------

DROP TABLE IF EXISTS `slider`;

CREATE TABLE `slider` (
  `slider_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slider_title` varchar(254) DEFAULT NULL,
  `slider_subtitle` varchar(254) DEFAULT NULL,
  `slider_uri` varchar(254) DEFAULT NULL,
  `slider_img` varchar(254) DEFAULT NULL,
  `status` enum('0','1') DEFAULT '1',
  `slider_btn` varchar(254) DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`slider_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `slider` WRITE;
/*!40000 ALTER TABLE `slider` DISABLE KEYS */;

INSERT INTO `slider` (`slider_id`, `slider_title`, `slider_subtitle`, `slider_uri`, `slider_img`, `status`, `slider_btn`, `updatedAt`)
VALUES
	(2,'Furniture <br> Collection','Discount all item','furniture','7dc9a44dc2d756fa10ed7a99d0a2ac15.jpg','1','Shop now','2021-06-15 13:49:01');

/*!40000 ALTER TABLE `slider` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table type_product
# ------------------------------------------------------------

DROP TABLE IF EXISTS `type_product`;

CREATE TABLE `type_product` (
  `id_type` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name_type` varchar(254) DEFAULT NULL,
  `slug_type` varchar(254) DEFAULT NULL,
  `status_type` enum('1','0') DEFAULT '1',
  `type_createdAt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `type_product` WRITE;
/*!40000 ALTER TABLE `type_product` DISABLE KEYS */;

INSERT INTO `type_product` (`id_type`, `name_type`, `slug_type`, `status_type`, `type_createdAt`)
VALUES
	(3,'Furniture','furniture','1','2021-06-14 20:52:52'),
	(5,'Accessories','accessories','1','2021-06-12 21:40:51');

/*!40000 ALTER TABLE `type_product` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
